from radai.evaluation.answerkey import build_answerkey, build_example_submission

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", type=str, help="Path to Test HDF5 file", required=True)

    args = parser.parse_args()
    datafn = args.d

    akpath = build_answerkey(radai_h5_path=datafn)

    sfn = build_example_submission(
        radai_h5_path=datafn,
        answerkey_path=akpath,
        detection_scale=5.0,
        identification_scale=12.0,
        false_positive_scale=0.08,
    )
