from radai.evaluation.evaluation import validate_submission, compute_metrics
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", type=str, help="Path to submission csv file", required=True
    )
    parser.add_argument(
        "-a", type=str, help="Path to answer key csv file", required=True
    )
    parser.add_argument("-d", type=str, help="Path to Test HDF5 file")
    parser.add_argument(
        "--skip-validation",
        action="store_true",
        help="Score the submission without validating it first.",
    )

    args = parser.parse_args()
    sfn = args.s
    akfn = args.a
    datafn = args.d

    if args.skip_validation:
        good = True
    else:
        good = validate_submission(answerkey_path=akfn, submission_path=sfn)

    if good:
        mfn = compute_metrics(
            radai_h5_path=datafn,
            answerkey_path=akfn,
            submission_path=sfn,
            max_alarm_time=160.0,
            # was 90, but increasing b/c largest encouter duration is 142s
        )
        print(mfn)
