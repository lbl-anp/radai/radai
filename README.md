# RADAI: Radiological Anomaly Detection And Identification

#### Python tools for performing radiological anomaly detection and identification.

This package implements common algorithms for performing radiological anomaly detection and identification.
Algorithms are built around the base-class `algorithms/algorithm.py` and return outputs structured with the class from `result.py`.
These algorithms can take advantage of pre-built filters `filters/*.py` to perform background aggregation or other operations.
Finally, the `data/*` module will provide an interface for reading, manipulating, and training algorithms with the to-be-released RADAI dataset.

The available algorithms include:
* *Gross-counts k-sigma*
* *Gross-counts SPRT (Sequential Probability Ratio Test)*
* *ROI (Region of Interest)*
* *CEW (Censored Energy Window)*
* *N-SCRAD (Nuisance-rejection Spectral Comparison Ratio Anomaly Detection)*
* *SAD (Spectral Anomaly Detection) (with PCA)*


## Installation

### User

```bash
pip install .
```

### Developer

```bash
pip install -r requirements-dev.txt && pip install -e .
```

### Extras
There are two extra dependencies (fast-histogram and tensorflow) that can be used to extend capabilities.
Install via `pip install -e .[full]` or install these dependencies separately within your environment.

### Testing

Beware... running all tests can take a while.

Testing with verbose output and early stopping:

```bash
pytest -vv -x
```

Testing with plots saved to disk (disabled by default):

```bash
SAVEFIG=True pytest ...
```

Testing can also be performed in parallel:

```bash
pip install pytest-xdist
pytest -vv -x -n 4
```

### Formatting

We use `pre-commit` to enforce `flake8` and `black` formatting as well as a couple of other helpful hooks to make collaborative development easier. Install with:

```bash
pre-commit install
```

and run with:

```bash
pre-commit run --all
```

## Getting started

TODO

## Contributing

If you are interested in contributing or are want to install the package from source, please see the instructions in [`CONTRIBUTING.md`](./CONTRIBUTING.md).

## Reporting issues

When reporting issues with `radai`, please provide a minimum working example to help identify the problem and tag the issue as a `bug`.

## Feature requests

For a feature request, please create an issue and label it as a `new feature`.

## LICENSE

Radiological Anomaly Detection And Identification (RADAI) Copyright (c) 2021,
The Regents of the University of California, through Lawrence Berkeley
National Laboratory and Oak Ridge National Laboratory (subject to receipt
of any required approvals from the U.S. Dept. of Energy). All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

(1) Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

(2) Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

(3) Neither the name of the University of California, Lawrence Berkeley
National Laboratory, Oak Ridge National Laboratory, U.S. Dept. of Energy
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

## COPYRIGHT

Radiological Anomaly Detection And Identification (RADAI) Copyright (c) 2021,
The Regents of the University of California, through Lawrence Berkeley
National Laboratory and Oak Ridge National Laboratory (subject to receipt
of any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative
works, and perform publicly and display publicly, and to permit others to do so.
