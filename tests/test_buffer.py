import pytest
import numpy as np
import radai


@pytest.fixture(scope="function", params=[1, 2, 10])
def _n_time(request):
    yield request.param


@pytest.fixture(scope="function", params=[1, 2, 1000])
def _n_energy(request):
    yield request.param


@pytest.fixture(scope="function", params=[np.uint32, np.int64, np.float32])
def _dtype(request):
    yield request.param


@pytest.fixture(scope="function")
def spectra_buffer_kwargs(_n_time, _n_energy, _dtype):
    return {"n_time": _n_time, "n_energy": _n_energy, "spectra_dtype": _dtype}


def _gen_stl(n_time, n_energy, duration_multiplier=2, dt=1.0) -> tuple:
    t0 = np.random.random() * 100.0 + 1e9
    dt = float(dt)
    for i in range(0, round(n_time * duration_multiplier)):
        spectrum = np.random.randint(0, 100, n_energy)
        yield (
            i,
            spectrum[0] if n_energy == 1 else spectrum,
            t0 + i * dt,
            dt * np.random.uniform(0.8, 1.0),
        )


class TestSpectraBuffer:
    @pytest.mark.parametrize("n_energy_starts_as_none", [False, True])
    def test_basic_operation(self, spectra_buffer_kwargs, n_energy_starts_as_none):
        n_energy = spectra_buffer_kwargs["n_energy"]
        if n_energy_starts_as_none:
            spectra_buffer_kwargs["n_energy"] = None
        buffer = radai.SpectraBuffer(**spectra_buffer_kwargs)
        assert not buffer.is_ready()
        with pytest.raises(ValueError):
            buffer.check_ready()
        for i, spec, ts, lt in _gen_stl(buffer.n_time, n_energy):
            # Check init case for unknown number of energy bins
            if n_energy_starts_as_none and i == 0:
                assert buffer.n_energy is None
                assert buffer.spectra is None
            else:
                assert buffer.n_energy == n_energy
            # Check the counter before updating
            assert i == buffer.counter
            # Add data
            buffer.update(spec, ts, lt)
            # Check the counter after updating
            assert i + 1 == buffer.counter
            # Check counts scalar
            if n_energy == 1:
                assert buffer.spectra[-1] == spec
            # Check spectrum
            else:
                assert np.all(buffer.spectra[-1] == spec)
            # Check that the buffer is ready once there are enough time entries
            if i + 1 >= buffer.n_time:
                assert buffer.is_ready()
            else:
                assert not buffer.is_ready()
                with pytest.raises(ValueError):
                    buffer.check_ready()

    def test_indexing(self, spectra_buffer_kwargs):
        buffer = radai.SpectraBuffer(**spectra_buffer_kwargs)
        s_list, t_list, l_list = [], [], []
        for i, spec, ts, lt in _gen_stl(buffer.n_time, buffer.n_energy):
            s_list.append(spec)
            t_list.append(ts)
            l_list.append(lt)
            buffer.update(spec, ts, lt)
            if buffer.is_ready():
                # Most recent entry
                stl = buffer[-1]
                assert np.allclose(stl[0], s_list[-1])
                assert np.isclose(stl[1], t_list[-1])
                assert np.isclose(stl[2], l_list[-1])
                # All entries
                if buffer.n_energy > 1:
                    stl = buffer[:]
                    assert np.allclose(stl[0], s_list[-buffer.n_time :])
                    assert np.allclose(stl[1], t_list[-buffer.n_time :])
                    assert np.allclose(stl[2], l_list[-buffer.n_time :])

    def test_clear(self, spectra_buffer_kwargs):
        buffer = radai.SpectraBuffer(**spectra_buffer_kwargs)
        for i, spec, ts, lt in _gen_stl(
            buffer.n_time, buffer.n_energy, duration_multiplier=1
        ):
            buffer.update(spec, ts, lt)
        assert buffer.is_ready()
        buffer.clear()
        assert buffer.is_empty()
        assert not buffer.is_ready()
        assert buffer.spectra is None
        assert np.all(np.isnan(buffer.timestamps))
        assert np.all(np.isnan(buffer.live_times))
