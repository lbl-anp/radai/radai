"""Test radai.algorithms on constant spectra."""

import copy
from functools import lru_cache
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import chi2
from radai.algorithms import (
    KSigma,
    SPRT,
    SAD,
    CEW,
    MultiplexedCEW,
    ROI,
    NSCRAD,
    NSCRADAlternate,
    NSCRADExperimental,
    NSCRADNonOverlapping,
)
from radai.spectrum_generator import RandomSpectrumGenerator
from radai.tools import hist_outline
from radai.filters import MovingAverageFilter
import pytest
from common import TEST_OUTPUTS_PATH


ALGORITHMS = [
    "k_sigma",
    "sprt",
    "sad",
    "cew",
    "multiplexedcew",
    "roi",
    "nscrad",
    "nscradalt",
    "nscradexp",
    "nscradnonoverlap",
]

DATASETS = [
    "bkg_constant_src_single",
    "bkg_constant_src_multiple",
    "bkg_complex_src_single",
    "bkg_complex_src_multiple",
]


def plot_rates(measured_spectra, measured_lts, generator):
    """Plot the measured, bkg, and source rates for the generated data."""
    measured_rate = measured_spectra.sum(axis=1) / sum(measured_lts)
    bkg_rates = np.array(generator.history_bkg_rates)
    src_rates = np.array(generator.history_src_rates)
    total_rate = bkg_rates.sum(axis=1) + src_rates.sum(axis=1)
    plt.plot(measured_rate, "k-", lw=2, label="measured total rate")
    plt.plot(bkg_rates.sum(axis=1), "k:", label="mean background rate")
    plt.plot(total_rate, "r-", lw=1.2, alpha=0.7, label="mean total rate")
    for j in range(bkg_rates.shape[1]):
        plt.plot(bkg_rates[:, j], "-", lw=1, alpha=0.7, label=f"background rate {j}")
    for j in range(src_rates.shape[1]):
        plt.plot(src_rates[:, j], "-", lw=1, alpha=0.7, label=f"source rate {j}")
    plt.legend()
    plt.xlabel("Sample")
    plt.ylabel("Counts per second")
    plt.xlim(0, len(total_rate))
    plt.ylim(0)
    plt.tight_layout()


def data_dict_summary(measured_spectra, measured_lts, bin_edges, generator):
    """Convert measured data and generator history to dict of mean values."""
    assert isinstance(generator, RandomSpectrumGenerator)
    bkg_rates = np.array(generator.history_bkg_rates)
    bkg_comps = generator.bkg_spectra
    src_rates = np.array(generator.history_src_rates)
    src_rates_mean = np.zeros((src_rates.shape[1],))
    for j in range(src_rates.shape[1]):
        rates_j = src_rates[:, j]
        src_rates_mean[j] = rates_j[rates_j > 0].mean()
    src_comps = generator.src_spectra
    data = {}
    data["train_bkg_comps_mean"] = bkg_rates.mean(axis=0)[:, None] * bkg_comps
    data["train_bkg_total_mean"] = sum(data["train_bkg_comps_mean"])
    data["train_src_comps_mean"] = src_rates_mean[:, None] * src_comps
    data["bin_edges"] = bin_edges
    return data


def plot_bkg_src_spectra(data):
    """Plot the mean background and source spectra."""
    assert isinstance(data, dict)
    assert "bin_edges" in data

    if "train_bkg_total_mean" in data:
        x2, y2 = hist_outline((data["train_bkg_total_mean"], data["bin_edges"]))
        plt.plot(x2, y2, "k-", lw=2, label="mean background")

    if "train_bkg_comps_mean" in data:
        for j in range(data["train_bkg_comps_mean"].shape[0]):
            x2, y2 = hist_outline((data["train_bkg_comps_mean"][j], data["bin_edges"]))
            plt.plot(x2, y2, "-", lw=1, alpha=0.7, label=f"background comp {j}")

    if "train_src_comps_mean" in data:
        for j in range(data["train_src_comps_mean"].shape[0]):
            x2, y2 = hist_outline((data["train_src_comps_mean"][j], data["bin_edges"]))
            plt.plot(x2, y2, "-", lw=2, alpha=0.7, label=f"source {j}")


@lru_cache
def generate_dataset(dset_name, seed=20):
    """Generate training and testing data for the specified dataset."""
    if dset_name not in DATASETS:
        raise Exception(f"Unknown dataset: {dset_name}")
    prefix = f"test_all_algorithms__{dset_name}_"
    np.random.seed(seed)

    required_keys = [
        "bin_edges",
        "train_spectra",
        "train_labels",
        "train_tss",
        "train_lts",
        "test_spectra",
        "test_labels",
        "test_tss",
        "test_lts",
    ]
    data = {key: [] for key in required_keys}

    bin_edges = np.linspace(0, 3000, num=101)
    n_bins = len(bin_edges) - 1

    # create background spectra
    if dset_name.startswith("bkg_constant"):
        spec_bkg = [np.exp(np.linspace(np.log(1.0), np.log(0.01), num=n_bins))]
        bkg_rates = [1500.0]
        bkg_rates_min = None
        bkg_rates_max = None
        bkg_rates_var = None
    elif dset_name.startswith("bkg_complex"):
        bkg0 = np.exp(np.linspace(np.log(1.0), np.log(0.01), num=n_bins))
        bkg1 = np.ones_like(bkg0)
        spec_bkg = [bkg0, bkg1]
        bkg_rates = [800.0, 400.0]
        bkg_rates_min = [400.0, 200.0]
        bkg_rates_max = [1800.0, 800.0]
        bkg_rates_var = [[60.0, -30.0], [-30.0, 60.0]]

    # create source spectra
    if dset_name.endswith("src_single"):
        spec_src = np.zeros_like(spec_bkg[0])
        spec_src[20:50] = 1.0
        spec_src = [spec_src]
        src_rates = [300.0]
        src_rates_var = [2500.0]
    elif dset_name.endswith("src_multiple"):
        src0 = np.zeros(len(bin_edges) - 1)
        src1 = np.zeros_like(src0)
        src2 = np.zeros_like(src0)
        src0[10:30] = 1
        src1[20:40] = 1
        src2[70:90] = 1
        spec_src = [src0, src1, src2]
        src_rates = [300.0, 300.0, 300.0]
        src_rates_var = [2500.0, 2500.0, 2500.0]

    # generate the datasets
    gen_train = RandomSpectrumGenerator(
        spec_bkg,
        bkg_rates=bkg_rates,
        bkg_rates_min=bkg_rates_min,
        bkg_rates_max=bkg_rates_max,
        bkg_rates_var=bkg_rates_var,
        src_spectra=spec_src,
        src_rates=src_rates,
        src_rates_var=src_rates_var,
        int_time=1.0,
        batch_size=5000 * (1 + len(spec_src)),
        n_bkg=5000,
        n_src=5000,
        inject_mode="cycle",
    )
    gen_test = copy.deepcopy(gen_train)
    gen_test.batch_size = 10000
    gen_test.n_bkg = 100
    gen_test.n_src = 5
    gen_test.inject_mode = "interleave"

    train_spectra, train_labels, train_tss, train_lts = gen_train.next()
    test_spectra, test_labels, test_tss, test_lts = gen_test.next()

    data["bin_edges"] = bin_edges
    data["spec_bkg"] = [spec_bkg]
    data["spec_src"] = [spec_src]
    data["train_spectra"] = train_spectra
    data["train_labels"] = train_labels
    data["train_tss"] = train_tss
    data["train_lts"] = train_lts
    data["test_spectra"] = test_spectra
    data["test_labels"] = test_labels
    data["test_tss"] = test_tss
    data["test_lts"] = test_lts

    # save background and source information
    data_train = data_dict_summary(train_spectra, train_lts, bin_edges, gen_train)
    data_test = data_dict_summary(test_spectra, test_lts, bin_edges, gen_test)
    data = {**data, **data_train}

    # plot the training count rates
    plt.figure(figsize=(10, 4))
    plot_rates(train_spectra, train_lts, gen_train)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}rates_train.png")
    plt.close()

    # plot the testing count rates
    plt.figure(figsize=(10, 4))
    plot_rates(test_spectra, test_lts, gen_test)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}rates_test.png")
    plt.close()

    # plot the mean training spectra
    plt.figure()
    plot_bkg_src_spectra(data_train)
    plt.legend()
    plt.xlabel("Energy bins (keV)")
    plt.ylabel("Counts per second per bin")
    plt.xlim(bin_edges[0], bin_edges[-1])
    plt.gca().set_yscale("log")
    plt.tight_layout()
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra_train.png")
    plt.close()

    # plot the mean testing spectra
    plt.figure()
    plot_bkg_src_spectra(data_test)
    plt.legend()
    plt.xlabel("Energy bins (keV)")
    plt.ylabel("Counts per second per bin")
    plt.xlim(bin_edges[0], bin_edges[-1])
    plt.gca().set_yscale("log")
    plt.tight_layout()
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra_test.png")
    plt.close()

    # check that all data have been generated
    for key in required_keys:
        assert len(data[key]) > 0, f"Dataset {dset_name} missing {key}"
    return data


def init_algorithm(alg_name, bin_edges):
    """Instantiate the algorithm for use in tests."""
    if alg_name not in ALGORITHMS:
        raise Exception(f"Unknown algorithm name: {alg_name}")

    # create a nuisance spectrum for NSCRAD
    spec_nuisance = np.ones(len(bin_edges) - 1)
    spec_nuisance[50:] += 5.0

    if alg_name == "k_sigma":
        cls = KSigma
        alg = cls(filter={"classname": "EWMAFilter", "lam": 0.01})
    elif alg_name == "sprt":
        cls = SPRT
        alg = cls(MovingAverageFilter(), auto_calculate_thresholds=True)
    elif alg_name == "sad":
        cls = SAD
        alg = cls(bin_edges=bin_edges)
    elif alg_name == "cew":
        cls = CEW
        alg = cls()
    elif alg_name == "multiplexedcew":
        cls = MultiplexedCEW
        alg = cls()
    elif alg_name == "roi":
        cls = ROI
        alg = cls(
            bin_edges=bin_edges,
            src_regions=[[400.0, 1000.0]],
            bkg_regions=[[100.0, 400.0], [1000.0, 2000.0]],
        )
    elif alg_name == "nscrad":
        cls = NSCRAD
        alg = cls(
            bin_edges=bin_edges,
            spectral_windows=[
                [100.0, 300.0],
                [300.0, 500.0],
                [400.0, 1000.0],
                [1200.0, 2000.0],
                [1600.0, 3000.0],
            ],
            nuisance_spectra=[spec_nuisance],
        )
    elif alg_name == "nscradalt":
        cls = NSCRADAlternate
        alg = cls(
            bin_edges=bin_edges,
            rebinning_matrix=np.eye(5, len(bin_edges) - 1),
            nuisance_spectra=[spec_nuisance],
        )
    elif alg_name == "nscradexp":
        cls = NSCRADExperimental
        alg = cls(
            bin_edges=bin_edges,
            spectral_windows=[
                [100.0, 300.0],
                [300.0, 500.0],
                [400.0, 1000.0],
                [1200.0, 2000.0],
                [1600.0, 3000.0],
            ],
            nuisance_spectra=[spec_nuisance],
        )
    elif alg_name == "nscradnonoverlap":
        cls = NSCRADNonOverlapping
        alg = cls(
            bin_edges=bin_edges,
            spectral_windows=[
                [100.0, 300.0],
                [300.0, 500.0],
                [500.0, 1000.0],
                [1000.0, 3000.0],
            ],
            nuisance_spectra=[spec_nuisance],
        )
    else:
        raise NotImplementedError(f"Algorithm {alg_name}")
    return alg, cls


def train_algorithm(dset_name, data, alg_name, alg, quick=False):
    """Train the algorithm with the training data."""
    prefix = f"test_all_algorithms__{dset_name}__{alg_name}__"
    train_info = None

    # extract background spectra for some algorithms
    is_bkg = data["train_labels"] == 0

    if alg_name == "k_sigma":
        pass
    elif alg_name == "sad":
        alg.fit(data["train_spectra"][is_bkg], live_times=data["train_lts"][is_bkg])
        # HACK: set the threshold by hand
        alg.threshold = 10.6
    elif alg_name == "sprt":
        alg.fit(
            data["train_spectra"][is_bkg],
            timestamps=data["train_lts"][is_bkg].cumsum(),
            live_times=data["train_lts"][is_bkg],
        )
    elif alg_name == "cew":
        if quick:
            kwargs = {
                "max_step": 5,
                "popsize": 10,
                "ngen": 5,
            }
        else:
            kwargs = {
                "max_step": 5,
                "popsize": 50,
                "ngen": 60,
            }
        train_info = alg.fit(
            data["train_spectra"],
            data["train_labels"],
            **kwargs,
            verbose=True,
            savefig=TEST_OUTPUTS_PATH / f"{prefix}foms.png",
        )
    elif alg_name == "multiplexedcew":
        train_info = alg.fit(
            data["train_spectra"],
            data["train_labels"],
        )
    elif alg_name == "roi":
        train_info = alg.fit(data["train_spectra"], data["train_labels"])
    elif alg_name == "nscradexp":
        if quick:
            kwargs = {
                "max_step": 5,
                "popsize": 10,
                "ngen": 5,
            }
        else:
            kwargs = {
                "max_step": 10,
                "popsize": 30,
                "ngen": 70,
            }
        train_info = alg.fit(
            data["train_spectra"],
            data["train_labels"],
            n_windows=5,
            **kwargs,
            savefig=TEST_OUTPUTS_PATH / f"{prefix}foms.png",
        )
    elif alg_name.startswith("nscrad"):
        if quick:
            kwargs = {
                "sparsity": 0,
                "eps": 1e-4,
                "max_step": 5,
                "popsize": 10,
                "ngen": 5,
            }
        else:
            kwargs = {
                "sparsity": 1e-4,
                "eps": 1e-7,
                "max_step": 5,
                "popsize": 30,
                "ngen": 50,
            }
            if alg_name == "nscradalt":
                kwargs["ngen"] = 100
        train_info = alg.fit(
            data["train_spectra"],
            data["train_labels"],
            n_windows=5,
            **kwargs,
            savefig=TEST_OUTPUTS_PATH / f"{prefix}foms.png",
        )
    else:
        raise NotImplementedError(f"Algorithm {alg_name}")
    return alg, train_info


def plot_metric_time(ts, metric, thresh):
    """Plot the metric versus time."""
    plt.plot(ts, metric, "r.-", ms=2, lw=0.5, label="alarm metric")
    plt.hlines(thresh, ts.min(), ts.max(), color="b", label="threshold")
    plt.legend()
    plt.xlabel("Time (s)")
    plt.ylabel("Alarm metric")
    plt.tight_layout()


def plot_src_ids_time(ts, src_ids, color="r", label="source ID"):
    """Plot the source IDs versus time."""
    plt.plot(ts, src_ids, ".-", color=color, ms=2, lw=0.5, label=label)
    plt.legend()
    plt.xlabel("Time (s)")
    plt.ylabel("Source ID")
    plt.tight_layout()


def plot_metric_hist(metric, bins, thresh, label, alg, alg_name):
    """Plot a histogram of the metric."""
    h = np.histogram(metric, bins=bins)
    x2, y2 = hist_outline(h)
    plt.plot(x2, y2, "k-", lw=0.5)
    plt.fill_between(x2, y2, color="r", alpha=0.2, label="alarm metric " + label)
    plt.vlines(thresh, 0, y2.max(), color="b", label="threshold")
    if alg_name in ("k_sigma", "cew", "multiplexedcew", "roi", "sprt"):
        x3 = np.linspace(x2.min(), x2.max(), num=1000)
        pdf = np.exp(-x3)
        pdf *= len(metric)
        pdf *= bins[1] - bins[0]
        plt.plot(x3, pdf, "g-", label="expected PDF (bkg only)", alpha=0.7)
    elif alg_name.startswith("nscrad"):
        x3 = np.linspace(x2.min(), x2.max(), num=1000)
        pdf = chi2.pdf(x3, alg.dof)
        pdf *= len(metric)
        pdf *= bins[1] - bins[0]
        plt.plot(x3, pdf, "g-", label="expected PDF (bkg only)", alpha=0.7)
    plt.legend()
    plt.xlabel("Metric value (arb.)")
    plt.ylabel("Counts per bin")
    plt.xlim(x2[0], x2[-1])
    plt.ylim(0.3, y2.max() * 2)
    plt.gca().set_yscale("log")
    plt.tight_layout()


@pytest.mark.parametrize("dset_name", DATASETS)
@pytest.mark.parametrize("alg_name", ALGORITHMS)
def test_algorithm(dset_name, alg_name):
    """Test the algorithms end-to-end on training and testing data."""
    prefix = f"test_all_algorithms__{dset_name}__{alg_name}__"
    data = generate_dataset(dset_name)
    alg = init_algorithm(alg_name, data["bin_edges"])[0]

    # The algorithm should not be ready or trained yet
    if alg_name in ("k_sigma", "nscrad", "nscradexp", "nscradalt", "nscradnonoverlap"):
        assert alg.is_ready()
    else:
        assert not alg.is_ready()

    alg, train_info = train_algorithm(dset_name, data, alg_name, alg)

    # evaluate the algorithm on testing data
    test_labels = data["test_labels"]
    bkg_set = test_labels == 0
    batch_results = alg.analyze_batch(
        data["test_spectra"], data["test_tss"], live_times=data["test_lts"]
    )
    # get highest metric and corresponding source ID for each measurement
    alarm_metrics = []
    src_ids = []
    is_alarm = []
    for results in batch_results:
        results.sort("alarm_metric", reverse=True)
        result = results[0]
        alarm_metrics.append(result.alarm_metric)
        is_alarm.append(result.is_alarm)
        src_ids.append(result.source_id)
    alarm_metrics = np.array(alarm_metrics)
    src_ids = np.array(src_ids)
    is_alarm = np.array(is_alarm)
    ts = data["test_tss"]
    metric_bkg = alarm_metrics[bkg_set]
    metric_src = alarm_metrics[~bkg_set]

    # extract threshold to use in plots below
    thresh = alg.threshold
    if alg_name == "multiplexedcew":
        # pull out the only sub-algorithm for the plots below
        alg = alg.sub_alg[1]
        thresh = alg.threshold
    elif alg_name == "sprt":
        thresh = alg.a

    # plot the results
    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_metric_time(ts, alarm_metrics, thresh)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}metric_time.png")
    plt.close()

    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_src_ids_time(ts, src_ids * is_alarm)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}srcids_time.png")
    plt.close()

    mean = metric_bkg.mean()
    std = metric_bkg.std()
    bins_min = min(mean - 5 * std, metric_bkg.min(), metric_src.min(), thresh)
    if alg_name in (
        "k_sigma",
        "cew",
        "multiplexedcew",
        "roi",
        "nscrad",
        "nscradalt",
        "nscradexp",
        "nscradnonoverlap",
        "sprt",
    ):
        bins_min = 0.0
    bins_max = max(mean + 5 * std, metric_bkg.max(), metric_src.max(), thresh)
    # bins_max = thresh + 5
    bins = np.linspace(bins_min, bins_max, num=201)

    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_metric_hist(metric_bkg, bins, thresh, "(bkg only)", alg, alg_name)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}metric_hist_bkg.png")
    plt.close()

    plt.figure(figsize=(10, 3))
    plt.title(alg_name)
    plot_metric_hist(metric_src, bins, thresh, "(source)", alg, alg_name)
    plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}metric_hist_src.png")
    plt.close()

    # make any special algorithm-specific plots
    plot_algorithm_specific(dset_name, data, alg_name, alg, train_info)


@pytest.mark.parametrize("dset_name", DATASETS)
@pytest.mark.parametrize("alg_name", ALGORITHMS)
def test_algorithm_empty_results(dset_name, alg_name):
    """Test the algorithms end-to-end on training and testing data."""
    data = generate_dataset(dset_name)
    alg = init_algorithm(alg_name, data["bin_edges"])[0]
    kwargs = dict(
        spectrum=np.atleast_1d(data["test_spectra"][0]),
        timestamp=np.atleast_1d(data["test_tss"][0]),
        live_time=np.atleast_1d(data["test_lts"][0]),
    )
    # The algorithm should be ready and return a non-zero results
    if alg_name in ("k_sigma", "nscrad", "nscradexp", "nscradalt", "nscradnonoverlap"):
        assert alg.is_ready()
        results = alg.analyze(**kwargs)
        assert len(results) > 0
    # The algorithm should not be ready or trained yet, run one analysis
    else:
        assert not alg.is_ready()
        results = alg.analyze(**kwargs)
        assert len(results) == 0


def plot_algorithm_specific(dset_name, data, alg_name, alg, train_info):
    """Create any plots specific to the algorithm."""
    prefix = f"test_all_algorithms__{dset_name}__{alg_name}__"
    if alg_name in ["cew", "multiplexedcew"]:
        if alg_name == "multiplexedcew":
            # pull out training info for the sub-algorithm
            train_info = train_info[1]

        # draw special plots for CEW
        plt.figure()
        plot_bkg_src_spectra(data)
        x2, y2 = hist_outline((train_info["s_over_b"], data["bin_edges"]))
        plt.plot(x2, y2, "-", label="S / B")
        plt.legend()
        plt.xlabel("Energy bins (keV)")
        plt.ylabel("Counts per second per bin")
        plt.xlim(x2[0], x2[-1])
        plt.gca().set_yscale("log")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra.png")
        plt.close()

        plt.figure()
        x2, y2 = hist_outline((alg.w_s, data["bin_edges"]))
        plt.plot(x2, y2, "-", label="source weights")
        x2, y2 = hist_outline((alg.w_b, data["bin_edges"]))
        plt.plot(x2, y2, "-", label="background weights")
        plt.legend()
        plt.xlabel("Energy bins (keV)")
        plt.ylabel("Weight value")
        plt.xlim(x2[0], x2[-1])
        plt.gca().set_yscale("symlog")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra_weights.png")
        plt.close()

    if alg_name == "roi":
        # draw special plots for ROI
        plt.figure()
        plot_bkg_src_spectra(data)
        plt.legend()
        plt.xlabel("Energy bins (keV)")
        plt.ylabel("Counts per second per bin")
        plt.xlim(data["bin_edges"][0], data["bin_edges"][-1])
        plt.gca().set_yscale("log")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra.png")
        plt.close()

        plt.figure()
        x2, y2 = hist_outline((alg.w_s, data["bin_edges"]))
        plt.plot(x2, y2, "-", label="source weights")
        x2, y2 = hist_outline((alg.w_b, data["bin_edges"]))
        plt.plot(x2, y2, "-", label="background weights")
        plt.legend()
        plt.xlabel("Energy bins (keV)")
        plt.ylabel("Weight value")
        plt.xlim(x2[0], x2[-1])
        plt.gca().set_yscale("log")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}spectra_weights.png")
        plt.close()

    if alg_name.startswith("nscrad"):
        # draw special plots for N-SCRAD
        plt.figure(figsize=(10, 3))
        plt.plot(train_info["foms"], ".-", ms=2, lw=0.5)
        plt.xlabel("Step during optimization")
        plt.ylabel("Figure of merit")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}foms.png")
        plt.close()

        # draw spectral windows
        plt.figure()
        plot_bkg_src_spectra(data)
        for j in range(alg.nuisance_spectra.shape[0]):
            spec = alg.nuisance_spectra[j]
            spec = spec / spec.sum() * train_info["bkg_spectrum"].sum()
            x2, y2 = hist_outline((spec, data["bin_edges"]))
            plt.plot(x2, y2, "b-", lw=2, label=f"nuisance spectrum {j}")
        R = alg.rebinning_matrix
        for j in range(R.shape[0]):
            spec = data["train_bkg_total_mean"]
            window = spec * R[j]
            window[window > 0] = window[window > 0].mean()
            x2, y2 = hist_outline((window, data["bin_edges"]))
            plt.fill_between(x2, y2, 0, alpha=0.2, label=f"window {j}")
        plt.legend(prop={"size": 7})
        plt.xlabel("Energy bins (keV)")
        plt.ylabel("Counts per second per bin")
        plt.xlim(x2[0], x2[-1])
        plt.gca().set_yscale("log")
        plt.tight_layout()
        plt.savefig(TEST_OUTPUTS_PATH / f"{prefix}windows.png")
        plt.close()


@pytest.mark.parametrize("alg_name", ALGORITHMS)
@pytest.mark.parametrize(
    "mode",
    (
        ".tar",
        ".tgz",
        ".tar.gz",
        ".tar.bz2",
        ".tar.xz",
        None,
        "dir",
        ".yaml",
        ".json",
        ".pkl",
    ),
)
def test_all_algorithms_io(alg_name, mode):
    """Test that to_dict/from_dict and read/write result in identical results."""
    dset_name = "bkg_constant_src_single"
    data = generate_dataset(dset_name)
    alg1, cls = init_algorithm(alg_name, data["bin_edges"])
    alg1 = train_algorithm(dset_name, data, alg_name, alg1, quick=True)[0]

    # instantiate a copy of the algorithm via dictionary or file loading
    if mode is None:
        alg2 = cls.from_dict(alg1.to_dict())
    else:
        dname = TEST_OUTPUTS_PATH / (
            f"test_all_algorithms__{dset_name}_{alg_name}_io_"
            f"{mode.lstrip('.').replace('.', '_')}"
        )
        if mode == "dir":
            fname = dname
        else:
            dname.mkdir(exist_ok=True)
            fname = (dname / "algo").with_suffix(mode)
        alg1.write(fname)
        alg2 = cls.read(fname)

    # evaluate the algorithms on testing data
    batch_results1 = alg1.analyze_batch(
        data["test_spectra"], data["test_tss"], live_times=data["test_lts"]
    )
    batch_results2 = alg2.analyze_batch(
        data["test_spectra"], data["test_tss"], live_times=data["test_lts"]
    )
    alarm_metrics1 = np.array(batch_results1.get_all("alarm_metric"))
    alarm_metrics2 = np.array(batch_results2.get_all("alarm_metric"))
    assert np.allclose(alarm_metrics1, alarm_metrics2)


@pytest.mark.parametrize("alg_name", ALGORITHMS)
def test_all_algorithms_to_dict_from_dict(alg_name):
    """Test that to_dict and from_dict result in identical results."""
    dset_name = "bkg_constant_src_single"
    data = generate_dataset(dset_name)
    alg1, cls = init_algorithm(alg_name, data["bin_edges"])
    alg1 = train_algorithm(dset_name, data, alg_name, alg1, quick=True)[0]

    # instantiate a copy of the algorithm
    alg2 = cls.from_dict(alg1.to_dict())

    # evaluate the algorithms on testing data
    batch_results1 = alg1.analyze_batch(
        data["test_spectra"], data["test_tss"], live_times=data["test_lts"]
    )
    batch_results2 = alg2.analyze_batch(
        data["test_spectra"], data["test_tss"], live_times=data["test_lts"]
    )
    alarm_metrics1 = np.array(batch_results1.get_all("alarm_metric"))
    alarm_metrics2 = np.array(batch_results2.get_all("alarm_metric"))
    assert np.allclose(alarm_metrics1, alarm_metrics2)
