"""Test the train_nmf utility."""

import numpy as np
from radai.nmf import factorize_nmf, train_bkg_sources


def test_train_nmf():
    """Test the training use case of factorize_nmf utility."""
    X_true = np.ones((1000, 30))
    X = np.random.poisson(X_true)
    regularizer = [{"classname": "Sparsity", "coeff": 1.0e-6}]

    # train a 1-component NMF model
    A, V, loss = factorize_nmf(X, 1)
    assert A.shape[1] == V.shape[0] == 1

    # train a 2-component NMF model
    A, V, loss = factorize_nmf(X, 2)
    assert A.shape[1] == V.shape[0] == 2

    # train a 2-component NMF model with sparsity
    A, V, loss = factorize_nmf(X, 2, regularizer=regularizer)
    assert A.shape[1] == V.shape[0] == 2

    # train an additional 1 component
    A2, V2, loss = factorize_nmf(X, 1, A=A, V=V)
    assert A2.shape[1] == V2.shape[0] == 3

    # train an additional 2 components
    A2, V2, loss = factorize_nmf(X, 2, A=A, V=V)
    assert A2.shape[1] == V2.shape[0] == 4

    # train an additional 2 components with sparsity
    A2, V2, loss = factorize_nmf(X, 2, A=A, V=V, regularizer=regularizer)
    assert A2.shape[1] == V2.shape[0] == 4


def test_train_bkg_sources():
    """Test train_bkg_sources."""
    X_true = np.ones((10000, 30))
    X = np.random.poisson(X_true)
    spec_ids = np.random.choice([0, 1, 2], size=X.shape[0])

    V_bkg, V_src = train_bkg_sources(X, spec_ids)
    assert V_bkg.shape == (1, 30)
    assert set(V_src.keys()) == set([1, 2])
    for src_id in [1, 2]:
        assert V_src[src_id].shape == (1, 30)

    V_bkg, V_src = train_bkg_sources(X, spec_ids, n_bkg=2, n_src=1)
    assert V_bkg.shape == (2, 30)
    assert set(V_src.keys()) == set([1, 2])
    for src_id in [1, 2]:
        assert V_src[src_id].shape == (1, 30)

    V_bkg, V_src = train_bkg_sources(X, spec_ids, n_bkg=1, n_src={1: 2, 2: 2})
    assert V_bkg.shape == (1, 30)
    assert set(V_src.keys()) == set([1, 2])
    for src_id in [1, 2]:
        assert V_src[src_id].shape == (2, 30)


def test_factorize_nmf():
    """Test transformation capability of factorize_nmf."""
    V1 = np.ones((1, 30)) * np.linspace(0.5, 1.5, num=30)
    V1 /= V1.sum(axis=1)[:, None]
    V2 = np.ones((1, 30)) * np.linspace(1.5, 0.5, num=30)
    V2 /= V2.sum(axis=1)[:, None]
    V2 = np.vstack([V1, V2])
    A_true = np.ones((10, 2)) * 30
    X_true = A_true @ V2
    X = np.random.poisson(X_true)

    # evaluate with a 1-component NMF model
    A1, Vfit, loss1 = factorize_nmf(X, 0, V=V1)
    assert A1.shape[1] == V1.shape[0] == 1
    assert isinstance(
        loss1, float
    ), "Regression test for https://gitlab.com/lbl-anp/radai/radai/-/merge_requests/63"

    # evaluate with frozen V
    A1, Vfit, loss1 = factorize_nmf(X, 0, V=V1, freeze_V=True)
    assert np.array_equal(Vfit, V1)

    # evaluate with non-frozen V
    A1, Vfit, loss1 = factorize_nmf(X, 0, V=V1, freeze_V=False)
    assert not np.array_equal(Vfit, V1)

    # evaluate with a 2-component NMF model
    A2, _, loss2 = factorize_nmf(X, 0, V=V2)
    assert A2.shape[1] == V2.shape[0] == 2

    # evaluate with mean for weight initialization
    A2, _, loss2 = factorize_nmf(X, 0, A=A_true, V=V2)
    assert A2.shape[1] == V2.shape[0] == 2

    # evaluate single spectrum with gamma prior
    regularizer = [
        {
            "classname": "Gamma",
            "mean": A_true[0:1],
            "cov": np.diag(np.ones(2)).reshape((1, 2, 2)),
            "coeff": 1.0,
        }
    ]
    A2, _, loss2 = factorize_nmf(
        X[0:1],
        0,
        V=V2,
        regularizer=regularizer,
    )
    assert A2.shape[1] == V2.shape[0] == 2
    assert A2.shape[0] == 1

    # evaluate single spectrum with shifted gamma prior
    regularizer[0]["mean"] += 5
    A2prime, _, loss2prime = factorize_nmf(
        X[0:1],
        0,
        V=V2,
        regularizer=regularizer,
    )
    assert np.isclose(A2, A2prime - 5.0, atol=1.0).all()
