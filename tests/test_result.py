import radai
import pytest
from copy import deepcopy
import numpy as np


# ------------------------------------------------------------------------------
# Result
# ------------------------------------------------------------------------------


def gen_result_kwargs(timestamp_offset=0, duration=1, include_optional=False):
    out = dict(
        origin="some_algo",
        source_id=10,
        is_alarm=True,
        alarm_metric=1.0,
        alarm_metric_name="some_metric",
        timestamp_start=1.5e9 + timestamp_offset,
        timestamp_stop=1.5e9 + timestamp_offset + duration,
    )
    if include_optional:
        out["over_threshold"] = False
        out["timestamp_alarm"] = out["timestamp_stop"] - duration * 0.1
        out["misc"] = {"random_value": 7}
    return out


@pytest.fixture(
    scope="function",
    params=[gen_result_kwargs(), gen_result_kwargs(include_optional=True)],
)
def result_kwargs(request):
    yield deepcopy(request.param)


@pytest.fixture(scope="function")
def result(result_kwargs):
    yield radai.Result(**result_kwargs)


class TestResult:
    def test_init(self, result_kwargs):
        result = radai.Result(**result_kwargs)
        assert result.origin == result_kwargs["origin"]
        assert result.source_id == result_kwargs["source_id"]
        assert result.is_alarm == result_kwargs["is_alarm"]
        assert result.alarm_metric == result_kwargs["alarm_metric"]
        assert result.alarm_metric_name == result_kwargs["alarm_metric_name"]
        assert result.timestamp_start == result_kwargs["timestamp_start"]
        assert result.timestamp_stop == result_kwargs["timestamp_stop"]
        if "timestamp_alarm" in result_kwargs:
            assert result.timestamp_alarm == result_kwargs["timestamp_alarm"]
        else:
            assert result.timestamp_alarm == (
                (result.timestamp_stop + result.timestamp_start) / 2.0
            )
        if "over_threshold" in result_kwargs:
            assert result.over_threshold is result_kwargs["over_threshold"]
        else:
            assert result.is_alarm is result.over_threshold

    def test_invalid_timstamp_start(self, result_kwargs):
        result_kwargs["timestamp_start"] = result_kwargs["timestamp_stop"]
        with pytest.raises(ValueError):
            radai.Result(**result_kwargs)

    def test_invalid_timstamp_stop(self, result_kwargs):
        result_kwargs["timestamp_stop"] = 0
        with pytest.raises(ValueError):
            radai.Result(**result_kwargs)

    def test_invalid_timestamp_alarm(self, result_kwargs):
        result_kwargs["timestamp_alarm"] = result_kwargs["timestamp_start"] - 1
        with pytest.raises(ValueError):
            radai.Result(**result_kwargs)

    def test_eq(self, result_kwargs):
        assert radai.Result(**result_kwargs) == radai.Result(**result_kwargs)

    def test_str_repr(self, result):
        str(result)
        repr(result)


# ------------------------------------------------------------------------------
# Results
# ------------------------------------------------------------------------------


def gen_results_kwargs(length, start_result):
    items = []
    assert length > 0
    for i in range(1, length + 1):
        _result = start_result.copy()
        _result.timestamp_start += start_result.get_duration() * i
        _result.timestamp_stop += start_result.get_duration() * i
        _result.timestamp_alarm += start_result.get_duration() * i
        _result.is_alarm = not _result.is_alarm
        _result.alarm_metric *= i
        items.append(_result)
    return dict(timestamp=_result.timestamp_stop, items=items)


@pytest.fixture(
    scope="function",
    params=[1, 10],
)
def length(request):
    yield request.param


@pytest.fixture(scope="function")
def results_kwargs(length, result):
    yield gen_results_kwargs(length, result)


@pytest.fixture(scope="function")
def results(results_kwargs):
    yield radai.Results(**results_kwargs)


class TestResults:
    def test_init(self, results_kwargs):
        results = radai.Results(**results_kwargs)
        assert np.isclose(
            results_kwargs["items"][-1].timestamp_stop - results.timestamp,
            0.0,
        )
        for r0, r1 in zip(results_kwargs["items"], results):
            assert r0 == r1

    def test_sort(self, results):
        _results = results.copy()
        first_result = _results.pop(0)
        _results.append(first_result)
        assert _results[-1] is first_result
        if len(results) > 1:
            assert _results != results
        else:
            assert _results == results
        _results.sort("timestamp_start")
        assert _results[0] is first_result
        assert _results == results

    def test_contains_alarm_empty(self):
        assert not radai.Results(0.0).contains_alarm()

    def test_contains_no_alarm(self, results):
        _results = results.copy()
        for r in _results:
            r.is_alarm = False
        assert not _results.contains_alarm()

    def test_contains_alarm(self, results):
        _results = results.copy()
        _results[0].is_alarm = True
        assert _results.contains_alarm()


# ------------------------------------------------------------------------------
# ResultsBatch
# ------------------------------------------------------------------------------


def gen_results_batch_kwargs(length, results):
    return dict(data=[results.copy() for _ in range(length)])


@pytest.fixture(scope="function")
def results_batch_kwargs(length, results):
    yield gen_results_batch_kwargs(length, results)


@pytest.fixture(scope="function")
def results_batch(results_batch_kwargs):
    yield radai.ResultsBatch(**results_batch_kwargs)


class TestResultsBatch:
    def test_init(self, results_batch_kwargs):
        results_batch = radai.ResultsBatch(**results_batch_kwargs)
        for r0, r1 in zip(results_batch_kwargs["data"], results_batch):
            assert r0 == r1

    @pytest.mark.parametrize(
        "name",
        [
            "origin",
            "source_id",
            "is_alarm",
            "alarm_metric",
            "alarm_metric_name",
            "timestamp_start",
            "timestamp_stop",
            "timestamp_alarm",
            "over_threshold",
            "misc",
        ],
    )
    @pytest.mark.parametrize("results_index", [None, 0])
    def test_get_all(self, name, results_index, results_batch):
        all_values = []
        for results in results_batch:
            if results_index is not None:
                all_values.append(results[results_index].get_value(name))
            else:
                for result in results:
                    all_values.append(result.get_value(name))
        assert radai.tools.is_close(
            results_batch.get_all(name, results_index), all_values
        )

    def test_copy_eq(self, results_batch):
        assert results_batch == results_batch.copy()
