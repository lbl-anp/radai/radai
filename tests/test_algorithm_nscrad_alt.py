"""Test radai.algorithms.NSCRADAlternate."""

import numpy as np
from radai.algorithms import NSCRADAlternate
from radai.spectrum_generator import RandomSpectrumGenerator
from common import TEST_OUTPUTS_PATH


def test_nscrad_alt():
    """Test the basic functionality of NSCRADAlternate (without training)."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        batch_size=1000,
    )
    alg = NSCRADAlternate(
        int_time=int_time,
        bin_edges=bin_edges,
        rebinning_matrix=None,
        nuisance_spectra=[np.ones(len(bin_edges) - 1)],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_nscrad_alt_mutation():
    """Test that NSCRADAlternate randomly rebinning never fails."""
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)

    alg = NSCRADAlternate(
        int_time=int_time,
        bin_edges=bin_edges,
        rebinning_matrix=None,
        nuisance_spectra=[np.ones(len(bin_edges) - 1)],
    )

    # try setting random rebinnings
    for _ in range(20):
        alg._set_random_binning(4)

    # try random mutations
    for _ in range(50):
        alg._randomly_perturb_binning(5)


def test_nscrad_alt_fit_known():
    """Test NSCRADAlternate including running fit with a known source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with known spectra
    alg = NSCRADAlternate(
        int_time=int_time,
        bin_edges=bin_edges,
        rebinning_matrix=5,
        nuisance_spectra=None,
    )
    alg.fit_known_sources(
        train_spectra,
        train_ids,
        [spec_src],
        n_windows=3,
        savefig=str(TEST_OUTPUTS_PATH / "test_algorithms_nscradalt_fit_known.png"),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_nscrad_alt_fit():
    """Test NSCRADAlternate including running fit with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = NSCRADAlternate(
        int_time=int_time,
        bin_edges=bin_edges,
        rebinning_matrix=5,
        nuisance_spectra=None,
    )
    alg.fit(
        train_spectra,
        train_ids,
        n_bkg=1,
        n_windows=3,
        savefig=str(TEST_OUTPUTS_PATH / "test_algorithms_nscradalt_fit_unknown.png"),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
