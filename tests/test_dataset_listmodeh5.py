import numpy as np
import pytest
import radai
import radai_test_data


# dataset settings
N_PER_SRC = 20
N_BATCH = 3
N_TIME = 10
N_ERG = 200
EDGES = np.linspace(11, 3000, num=N_ERG + 1)
SOURCES = ["Cs-137", "Co-60"]
N_SRCS = 1 + len(SOURCES)
METHODS = [
    "iter_spectra",
    "iter_waterfalls",
    "iter_encounters",
    "iter_batches",
    "spectrum_batch",
]


def test_load():
    radai.data.ListmodeH5(
        radai_test_data.dataset.listmodeh5_example, deep_validation=True
    )


def prepare_dataset(augment):
    """Prepare the ListmodH5 dataset for sampling."""
    dataset = radai.data.ListmodeH5(
        path=radai_test_data.dataset.listmodeh5_example,
        num_time_bins=N_TIME,
        energy_bin_edges=EDGES,
        batch_size=N_BATCH,
        time_step_size=1000,
        deep_validation=True,
    )

    # set up labels
    dataset.label_manager.tier = 1
    sources = dataset.label_manager.get_labels_list()
    sources.remove("BKG")
    dataset.label_manager.disable_items(*sources)
    dataset.label_manager.enable_items(*SOURCES)

    # set up labeler
    dataset.set_labeler("snr")
    dataset.set_labeler(
        dataset.labeler.copy_with_new_params(
            threshold=0.1,
            threshold_bkg_only=1e-9,
        )
    )

    if augment:
        # set up augmentations
        dataset.augmentor.binomial_downsample(
            p_background=[0.5, 1.0],
            p_source=[0.1, 0.5, 1.0],
        )
        dataset.augmentor.reverse_time(0.5)
        dataset.augmentor.set_seed(43)

    return dataset


def unpack_generate_data_results(result, method):
    """Unpack the generate_data results into individual samples.

    An individual sample is a 2D histogram with shape of either (1, N_ERG)
    or (N_TIME, N_ERG), depending on which mode is used.
    """
    # extract individual data
    data = []
    if method in ["iter_spectra", "iter_waterfalls", "iter_encounters", "iter_batches"]:
        for x in result:
            data.append(x)
    else:
        data = [result]

    # check the shapes of the samples
    for x in data:
        if method == "iter_spectra":
            assert x.data.shape == (N_ERG,)
        elif method in ["iter_waterfalls", "iter_encounters"]:
            assert x.data.shape == (N_TIME, N_ERG)
        elif method == "iter_batches":
            assert x.data.shape == (N_BATCH, N_TIME, N_ERG)
        else:
            assert x.data.shape == (N_PER_SRC * N_SRCS, 1, N_ERG)

    counts = []
    booleans = []
    scalars = []
    for x in data:
        if method == "iter_spectra":
            assert x.data.shape == (N_ERG,)
            assert x.labels.booleans.shape == (N_SRCS,)
            assert x.labels.scalars.shape == (N_SRCS,)
            counts.append(x.data.reshape((1, N_ERG)))
            booleans.append(x.labels.booleans)
            scalars.append(x.labels.scalars.reshape((1, N_SRCS)))
        elif method in ["iter_waterfalls", "iter_encounters"]:
            assert x.data.shape == (N_TIME, N_ERG)
            assert x.labels.booleans.shape == (N_SRCS,)
            assert x.labels.scalars.shape == (N_TIME, N_SRCS)
            counts.append(x.data)
            booleans.append(x.labels.booleans)
            scalars.append(x.labels.scalars)
        elif method == "iter_batches":
            assert x.data.shape == (N_BATCH, N_TIME, N_ERG)
            assert x.labels.booleans.shape == (N_BATCH, N_SRCS)
            assert x.labels.scalars.shape == (N_BATCH, N_TIME, N_SRCS)
            for j in range(N_BATCH):
                counts.append(x.data[j, :, :])
                booleans.append(x.labels.booleans[j, :])
                scalars.append(x.labels.scalars[j, :, :])
        else:
            assert x.data.shape == (N_PER_SRC * N_SRCS, 1, N_ERG)
            assert x.labels.booleans.shape == (N_PER_SRC * N_SRCS, N_SRCS)
            assert x.labels.scalars.shape == (N_PER_SRC * N_SRCS, 1, N_SRCS)
            for j in range(N_PER_SRC * N_SRCS):
                counts.append(x.data[j, :, :])
                booleans.append(x.labels.booleans[j, :])
                scalars.append(x.labels.scalars[j, :, :])

    return counts, booleans, scalars


def find_and_check_sample_labels(booleans, scalars):
    """Find the labels and check there is exactly one per sample."""
    labels = []
    # go through each sample and check labels and SNR values
    for bools, snrs in zip(booleans, scalars):
        assert bools.ndim == 1
        assert snrs.ndim == 2
        assert bools.shape[0] == snrs.shape[1]
        idx_label = np.where(bools)[0]
        assert len(idx_label) == 1
        idx_label = idx_label[0]
        labels.append(idx_label)
        idx_others = np.array([idx for idx in range(1, N_SRCS) if idx != idx_label])
        if idx_label > 0:
            assert np.all(snrs[:, idx_label] >= 0)
            assert np.allclose(snrs[:, idx_others], 0)
        else:
            assert np.all(~np.isfinite(snrs[:, 0]))
            assert np.allclose(snrs[:, 1:], 0)
    return labels


def check_number_of_samples_and_labels(labels):
    """Check that we get the expected number of samples and labels."""
    # check that we get the expected total number of samples
    assert len(labels) <= N_PER_SRC * N_SRCS

    # check that we get the expected number for each label
    labels, n_labels = np.unique(labels, return_counts=True)
    print(labels, n_labels)
    for label, n_label in zip(labels, n_labels):
        print(label, n_label)
        assert n_label <= N_PER_SRC


@pytest.mark.parametrize("method", METHODS)
@pytest.mark.parametrize("augment", [True, False])
@pytest.mark.parametrize("return_bkg", [False, True])
def test_generate_data(method, augment, return_bkg):
    """Test ListmodeH5.generate_data for different modes."""
    dataset = prepare_dataset(augment)

    # generate the data
    result = dataset.generate_data(
        method=method,
        number=N_PER_SRC * N_SRCS,
        return_bkg=return_bkg,
    )
    if return_bkg:
        result, result_b = result

    # check there is exactly one label for each sample
    # first unpack and group the data into individual samples
    counts, booleans, scalars = unpack_generate_data_results(result, method)
    labels = find_and_check_sample_labels(booleans, scalars)

    # check that we get the expected total number of samples and labels
    check_number_of_samples_and_labels(labels)

    if return_bkg:
        # extract the data samples
        counts_b, _, _ = unpack_generate_data_results(result_b, method)

        # check that we get the expected total number of samples
        assert len(counts) == len(counts_b)
        assert len(counts) <= N_PER_SRC * N_SRCS
        assert len(counts_b) <= N_PER_SRC * N_SRCS

        # check that the background samples are consistent with the non-bkg samples
        for hist2d_bs, hist2d_b in zip(counts, counts_b):
            hist2d_bs = hist2d_bs.astype(int)
            hist2d_b = hist2d_b.astype(int)
            assert np.all(hist2d_bs >= hist2d_b)
            hist2d_s = hist2d_bs - hist2d_b
            assert np.all(hist2d_s >= 0)


def test_generate_data_encounter_center_range():
    """Test ListmodeH5.generate_data with encounter_center_range set."""
    # this example should work
    dataset = prepare_dataset(True)
    dataset.generate_data(
        method="iter_encounters",
        number=N_PER_SRC * N_SRCS,
        encounter_center_range=(0, 1),
    )

    # this example should also work
    dataset = prepare_dataset(True)
    dataset.generate_data(
        method="iter_encounters",
        number=N_PER_SRC * N_SRCS,
        encounter_center_range=(-1, 2),
    )

    # failure -- improperly formed tuple
    with pytest.raises(radai.data.RadaiDataSetError):
        dataset = prepare_dataset(True)
        dataset.generate_data(
            method="iter_encounters",
            number=N_PER_SRC * N_SRCS,
            encounter_center_range=(0,),
        )

    # failure -- improperly formed tuple
    with pytest.raises(radai.data.RadaiDataSetError):
        dataset = prepare_dataset(True)
        dataset.generate_data(
            method="iter_encounters",
            number=N_PER_SRC * N_SRCS,
            encounter_center_range=(1, -1),
        )
