"""Common tools for all tests."""
from pathlib import Path
import pickle
import numpy as np
import radai


# TODO @jccurtis update the use of this path for pathlib compatibility
TEST_PATH = Path(__file__).absolute().parent
TEST_OUTPUTS_PATH = TEST_PATH / "test_outputs"
TEST_OUTPUTS_PATH.mkdir(exist_ok=True)


class FakeAlgo(radai.algorithms.Algorithm):
    def __init__(self, param0, data0, data1, ready=True):
        super().__init__(param0=param0, data0=data0, data1=data1, ready=ready)

    def _analyze(self) -> radai.Results:
        timestamp = self.spectra_buffer.get_timestamps(-1)
        return radai.Results(timestamp=timestamp)

    def calculate_threshold(self, *args, **kwargs):
        pass

    def fit(self, *args, **kwargs):
        pass

    def is_ready(self, *args, **kwargs):
        return self.ready

    def to_dict(self, path):
        path = Path(path)
        data0_path = path / "data0.npy"
        data1_path = path / "data1.pkl"
        np.save(data0_path, self.data0)
        with data1_path.open("wb") as infile:
            pickle.dump(self.data1, infile)
        return {
            "param0": self.param0,
            "data0": str(data0_path.name),
            "data1": str(data1_path.name),
        }

    @classmethod
    def from_dict(cls, dic, path):
        with (Path(path) / dic["data1"]).open("rb") as infile:
            data1 = pickle.load(infile)
        return cls(
            param0=dic["param0"],
            data0=np.load(Path(path) / dic["data0"]),
            data1=data1,
        )
