"""Test filters."""

import matplotlib.pyplot as plt
import numpy as np
import pytest
from radai.spectrum_generator import RandomSpectrumGenerator
from radai import filters
from common import TEST_OUTPUTS_PATH

np.random.seed(20)


filters_kwargs = [
    ("scalar", filters.StaticFilter, {"mean": 1600.0, "cov": 100.0}),
    ("scalar", filters.MovingAverageFilter, {"n": 20}),
    ("scalar", filters.MedianFilter, {"n": 21}),
    ("scalar", filters.EWMAFilter, {"lam": 0.05}),
    ("scalar", filters.EWMAFilterAlternate, {"lam": 0.05}),
    (
        "scalar",
        filters.KalmanFilter,
        # {"F": [[1]], "H": [[1]], "H_inv": [[1]], "Q": [[30]]},
        {
            "F": [[1, 0.1], [0, 1]],
            "H": [[1, 0]],
            "H_inv": [[1], [0]],
            "Q": [[1, 0], [0, 10]],
        },
    ),
    ("array1d", filters.StaticFilter, {"mean": 1600.0, "cov": 100.0}),
    ("array1d", filters.MovingAverageFilter, {"n": 20}),
    ("array1d", filters.MedianFilter, {"n": 21}),
    ("array1d", filters.EWMAFilter, {"lam": 0.05}),
    ("array1d", filters.EWMAFilterAlternate, {"lam": 0.05}),
    (
        "array1d",
        filters.KalmanFilter,
        # {"F": [[1]], "H": [[1]], "H_inv": [[1]], "Q": [[30]]},
        {
            "F": [[1, 0.1], [0, 1]],
            "H": [[1, 0]],
            "H_inv": [[1], [0]],
            "Q": [[1, 0], [0, 10]],
        },
    ),
    (
        "array2d",
        filters.StaticFilter,
        {"mean": [1900.0, 1300.0], "cov": [[100.0, 0.0], [0.0, 100.0]]},
    ),
    ("array2d", filters.MovingAverageFilter, {"n": 20}),
    ("array2d", filters.MedianFilter, {"n": 21}),
    ("array2d", filters.EWMAFilter, {"lam": 0.05}),
    ("array2d", filters.EWMAFilterAlternate, {"lam": 0.05}),
    (
        "array2d",
        filters.KalmanFilter,
        {
            # "F": [[1, 0], [0, 1]],
            # "H": [[1, 0], [0, 1]],
            # "H_inv": [[1, 0], [0, 1]],
            # "Q": [[30, 0], [0, 30]],
            "F": [[1, 0, 0.1, 0], [0, 1, 0, 0.1], [0, 0, 1, 0], [0, 0, 0, 1]],
            "H": [[1, 0, 0, 0], [0, 1, 0, 0]],
            "H_inv": [[1, 0], [0, 1], [0, 0], [0, 0]],
            "Q": [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 30, 0], [0, 0, 0, 30]],
        },
    ),
]


@pytest.mark.parametrize("data_dim, filter_cls, kwargs", filters_kwargs)
def test_filters_init(data_dim, filter_cls, kwargs):
    """Test that the filters can be initialized."""
    filter_cls(**kwargs)


# generate scalar (gross counts) and array (spectrum) data to test against
INT_TIME = 1.0
gen = {
    "array1d_constant": RandomSpectrumGenerator(
        [[1.0]],
        int_time=INT_TIME,
        bkg_rates=1600.0,
        n_total=1000,
        ts_start=1.6e9,
    ),
    "array2d_constant": RandomSpectrumGenerator(
        [[0.6, 0.4]],
        int_time=INT_TIME,
        bkg_rates=3200.0,
        n_total=1000,
        ts_start=1.6e9,
    ),
    "array1d_randomwalk": RandomSpectrumGenerator(
        [[1.0]],
        int_time=INT_TIME,
        bkg_rates=1600.0,
        bkg_rates_min=1000.0,
        bkg_rates_max=2500.0,
        bkg_rates_var=10.0,
        n_total=1000,
        ts_start=1.6e9,
    ),
    "array2d_randomwalk": RandomSpectrumGenerator(
        [[0.6, 0.4]],
        int_time=INT_TIME,
        bkg_rates=3200.0,
        bkg_rates_min=2000.0,
        bkg_rates_max=5000.0,
        bkg_rates_var=20.0,
        n_total=1000,
        ts_start=1.6e9,
    ),
}
init = {key: gen[key].next() for key in gen}
data = {key: [x for x in gen[key]] for key in gen}

# create scalar data by summing 1d array of counts
for data_type in ["constant", "randomwalk"]:
    init["scalar_" + data_type] = (
        init["array1d_" + data_type][0].sum(),
        init["array1d_" + data_type][1],
        init["array1d_" + data_type][2],
        init["array1d_" + data_type][3],
    )
    data["scalar_" + data_type] = [
        (counts.sum(), spec_id, ts, lt)
        for counts, spec_id, ts, lt in data["array1d_" + data_type]
    ]


@pytest.mark.parametrize("data_type", ["constant", "randomwalk"])
@pytest.mark.parametrize("data_dim, filter_cls, kwargs", filters_kwargs)
def test_filters_scalar_array(data_type, data_dim, filter_cls, kwargs):
    """Test that the filters work on scalar and array data."""
    f = filter_cls(**kwargs)

    # initialize the filter with a measurement
    data_key = data_dim + "_" + data_type
    counts, _, ts, lt = init[data_key]
    f.update(counts, ts, lt)

    # run the filter on the data and plot the results
    t = []
    x = []
    t_hat = []
    x_hat = []
    x_hat_var = []
    for counts, _, ts, lt in data[data_key]:
        x.append(counts / INT_TIME)
        t.append(ts)
        if isinstance(f, filters.MedianFilter):
            if len(f.buffer_ts) < f.n:
                pass
            else:
                prediction = f.predict(lt)
                t_hat.append(prediction[0])
                x_hat.append(prediction[1] / lt)
                x_hat_var.append(prediction[2] / lt**2)
        else:
            prediction = f.predict(lt)
            t_hat.append(ts)
            x_hat.append(prediction[0] / lt)
            x_hat_var.append(prediction[1] / lt**2)
        f.update(counts, ts, lt)

    x = np.array(x)
    x_hat = np.array(x_hat)
    x_hat_var = np.array(x_hat_var)
    if len(x.shape) == 1:
        x.shape = (x.shape[0], 1)

    plt.figure(figsize=(10, 3))
    plt.title(type(f).__name__)
    if data_dim in ["scalar", "array1d"]:
        plt.plot(t, x[:, 0], "b-", lw=0.5, label="data")
        plt.plot(t_hat, x_hat[:, 0], "r-", label="filter prediction")
        plt.fill_between(
            t_hat,
            x_hat[:, 0] - 2 * np.sqrt(x_hat_var[:, 0, 0]),
            x_hat[:, 0] + 2 * np.sqrt(x_hat_var[:, 0, 0]),
            color="r",
            alpha=0.2,
            label="95% interval of prediction",
        )
    else:
        plt.plot(t, x[:, 0], "b-", lw=0.5, label="data (dim 0)")
        plt.plot(t_hat, x_hat[:, 0], "r-", label="filter prediction (dim 0)")
        plt.fill_between(
            t_hat,
            x_hat[:, 0] - 2 * np.sqrt(x_hat_var[:, 0, 0]),
            x_hat[:, 0] + 2 * np.sqrt(x_hat_var[:, 0, 0]),
            color="r",
            alpha=0.2,
        )
        plt.plot(t, x[:, 1], "g-", lw=0.5, label="data (dim 1)")
        plt.plot(t_hat, x_hat[:, 1], "m-", label="filter prediction (dim 1)")
        plt.fill_between(
            t_hat,
            x_hat[:, 1] - 2 * np.sqrt(x_hat_var[:, 1, 1]),
            x_hat[:, 1] + 2 * np.sqrt(x_hat_var[:, 1, 1]),
            color="m",
            alpha=0.2,
        )
    plt.legend()
    plt.xlabel("Time (s)")
    plt.ylabel("Gross count rate (CPS)")
    plt.tight_layout()
    plt.savefig(TEST_OUTPUTS_PATH / f"test_filters_{data_key}_{type(f).__name__}.png")
    plt.close()


filters_kwargs_classname = [
    (
        filters.StaticFilter,
        {"classname": "StaticFilter", "mean": 1600.0, "cov": 100.0},
    ),
    (filters.MovingAverageFilter, {"classname": "MovingAverageFilter", "n": 50}),
    (filters.MedianFilter, {"classname": "MedianFilter", "n": 21}),
    (filters.EWMAFilter, {"classname": "EWMAFilter", "lam": 0.01}),
    (filters.EWMAFilterAlternate, {"classname": "EWMAFilterAlternate", "lam": 0.01}),
    (
        filters.KalmanFilter,
        {
            "classname": "KalmanFilter",
            "F": [[1, 0], [0, 1]],
            "H": [[1, 0], [0, 1]],
            "H_inv": [[1, 0], [0, 1]],
            "Q": [[30, 0], [0, 30]],
        },
    ),
]


@pytest.mark.parametrize("filter_cls, kwargs", filters_kwargs_classname)
def test_filters_make_filter(filter_cls, kwargs):
    """Test filters.make_filter."""
    obj = filters.make_filter(kwargs)
    assert isinstance(obj, filter_cls)


@pytest.mark.parametrize("filter_cls, kwargs", filters_kwargs_classname)
def test_filters_to_dict(filter_cls, kwargs):
    """Test filter can be converted to and from a dict."""
    kwargs1 = dict(**kwargs)
    obj1 = filter_cls(**kwargs1)

    # make sure we get the same instance back when using to_dict
    kwargs2 = obj1.to_dict()
    obj2 = filters.make_filter(kwargs2)
    assert isinstance(obj1, filter_cls)
    assert isinstance(obj2, filter_cls)

    # make sure we get the same instance back when using from_dict
    obj3 = filter_cls.from_dict(kwargs2)
    assert isinstance(obj1, filter_cls)
    assert isinstance(obj3, filter_cls)
