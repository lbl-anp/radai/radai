"""Test radai.algorithms.NSCRADExperimental."""

import numpy as np
from radai.algorithms import NSCRADExperimental
from radai.spectrum_generator import RandomSpectrumGenerator
from common import TEST_OUTPUTS_PATH


def test_nscrad_exp():
    """Test the basic functionality of NSCRADExperimental (without training)."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        batch_size=1000,
    )
    alg = NSCRADExperimental(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0, 400], [300, 700], [900, 1400], [2000, 2600]],
        nuisance_spectra=[np.ones(len(bin_edges) - 1)],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_nscrad_exp_fit():
    """Test NSCRADExperimental including running fit with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = NSCRADExperimental(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0.0, 100.0], [100.0, 200.0]],  # need dummy windows
        nuisance_spectra=None,
    )
    alg.fit(
        train_spectra,
        train_ids,
        n_windows=3,
        savefig=str(TEST_OUTPUTS_PATH / "test_algorithms_nscradexp_fit_unknown.png"),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
