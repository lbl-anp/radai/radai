"""Test radai.algorithms.NSCRAD."""

import numpy as np
from radai.algorithms import NSCRAD
from radai.spectrum_generator import RandomSpectrumGenerator
from radai.algorithms.nscrad import (
    validate_spectral_windows,
    spectral_windows_to_bool,
    rebinning_matrix_from_windows,
    validate_rebinning_matrix,
)
import pytest
from common import TEST_OUTPUTS_PATH


def test_validate_spectral_windows():
    """Test the tool validate_spectral_windows."""
    bin_edges = np.linspace(0, 3000, num=101)
    # single valid window
    validate_spectral_windows(bin_edges, [100, 500])
    # multiple valid windows
    validate_spectral_windows(bin_edges, [100, 500], [600, 800])
    # window outside of edges (low)
    with pytest.raises(AssertionError):
        validate_spectral_windows(bin_edges, [-100, 500])
    # window outside of edges (high)
    with pytest.raises(AssertionError):
        validate_spectral_windows(bin_edges, [100, 5000])
    # too few energies in window
    with pytest.raises(AssertionError):
        validate_spectral_windows(bin_edges, [100])
    # too many energies in window
    with pytest.raises(AssertionError):
        validate_spectral_windows(bin_edges, [100, 500, 600])
    # window is not an iterable
    with pytest.raises(AssertionError):
        validate_spectral_windows(bin_edges, "test")


def test_spectral_windows_to_bool():
    """Test the tool spectral_windows_to_bool."""
    bin_edges = np.linspace(0, 200, num=201)
    # bin edges must be ascending to result in a valid window
    arr = spectral_windows_to_bool(bin_edges, [100, 100])
    assert (~arr).all()
    # one bin is true, window edges are at its left edge and its middle
    arr = spectral_windows_to_bool(bin_edges, [100, 100.5])
    assert (~arr[:100]).all()
    assert arr[100]
    assert (~arr[101:]).all()
    # one bin is true, window edges are at its two edges
    arr = spectral_windows_to_bool(bin_edges, [100, 101])
    assert (~arr[:100]).all()
    assert arr[100]
    assert (~arr[101:]).all()
    # two bins are true, window edges are at an edge and in middle of the bin
    arr = spectral_windows_to_bool(bin_edges, [100, 101.5])
    assert (~arr[:100]).all()
    assert (arr[100:102]).all()
    assert (~arr[102:]).all()
    # two bins are true, window edges are in middle of the bins
    arr = spectral_windows_to_bool(bin_edges, [100.5, 101.5])
    assert (~arr[:100]).all()
    assert (arr[100:102]).all()
    assert (~arr[102:]).all()
    # one bin is true, window edges are at its left edge and middle
    arr = spectral_windows_to_bool(bin_edges, [101, 101.5])
    assert (~arr[:101]).all()
    assert arr[101]
    assert (~arr[102:]).all()
    # three bins are true, left window in middle, right window at edge
    arr = spectral_windows_to_bool(bin_edges, [100.5, 103])
    assert (~arr[:100]).all()
    assert (arr[100:103]).all()
    assert (~arr[103:]).all()
    # two windows
    arr = spectral_windows_to_bool(bin_edges, [100.5, 104.5], [110.5, 114.5])
    assert (~arr[:100]).all()
    assert (arr[100:105]).all()
    assert (~arr[105:110]).all()
    assert (arr[110:115]).all()
    assert (~arr[115:]).all()


def test_rebinning_matrix_from_windows():
    """Test the tool rebinning_matrix_from_windows."""
    bin_edges = np.linspace(0, 200, num=201)
    # single window
    mat = rebinning_matrix_from_windows(bin_edges, [100, 102])
    assert mat.shape == (1, len(bin_edges) - 1)
    assert np.allclose(mat[0, :100], 0.0)
    assert np.allclose(mat[0, 100:102], 1.0)
    assert np.allclose(mat[0, 102:], 0.0)
    # two windows
    mat = rebinning_matrix_from_windows(bin_edges, [100, 102], [150, 152])
    assert mat.shape == (2, len(bin_edges) - 1)
    assert np.allclose(mat[0, :100], 0.0)
    assert np.allclose(mat[0, 100:102], 1.0)
    assert np.allclose(mat[0, 102:], 0.0)
    assert np.allclose(mat[1, :150], 0.0)
    assert np.allclose(mat[1, 150:152], 1.0)
    assert np.allclose(mat[1, 152:], 0.0)


def test_validate_rebinning_matrix():
    """Test the tool validate_rebinning_matrix."""
    bin_edges = np.linspace(0, 200, num=201)
    # non-degenerate matrix of zeros and ones
    mat = np.zeros((2, 200))
    mat[0, 100:103] = 1.0
    mat[1, 150:153] = 1.0
    validate_rebinning_matrix(bin_edges, mat)
    # matrix with too few dimensions
    with pytest.raises(AssertionError):
        mat = np.zeros((200,))
        validate_rebinning_matrix(bin_edges, mat)
    # matrix not matching number of bins
    with pytest.raises(AssertionError):
        mat = np.zeros((1, 205))
        validate_rebinning_matrix(bin_edges, mat)
    # matrix of zeros (degenerate)
    with pytest.raises(AssertionError):
        mat = np.zeros((2, 200))
        validate_rebinning_matrix(bin_edges, mat)
    # matrix of ones (degenerate)
    with pytest.raises(AssertionError):
        mat = np.zeros((2, 200))
        validate_rebinning_matrix(bin_edges, mat)
    # matrix with entries other than 0 or 1
    with pytest.raises(AssertionError):
        mat = np.zeros((2, 200))
        mat[0, 0] = 5
        validate_rebinning_matrix(bin_edges, mat)
    # matrix otherwise properly formed except two rows are identical
    with pytest.raises(AssertionError):
        mat = np.zeros((3, 200))
        mat[0, 100:103] = 1.0
        mat[1, 150:153] = 1.0
        mat[2, 150:153] = 1.0
        validate_rebinning_matrix(bin_edges, mat)


def test_nscrad():
    """Test the basic functionality of N-SCRAD (without training)."""
    # generate background data and train the algorithm
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)
    gen = RandomSpectrumGenerator(
        [spectrum],
        int_time=int_time,
        batch_size=1000,
    )
    alg = NSCRAD(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0, 400], [300, 700], [900, 1400], [2000, 2600]],
        nuisance_spectra=[np.ones(len(bin_edges) - 1)],
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_nscrad_mutation():
    """Test that NSCRAD randomly rebinning never fails."""
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)

    alg = NSCRAD(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0, 400], [300, 700], [900, 1400], [2000, 2600]],
        nuisance_spectra=[np.ones(len(bin_edges) - 1)],
    )

    # try setting random rebinnings
    for _ in range(20):
        alg._set_random_binning(4)

    # try random mutations
    for _ in range(50):
        alg._randomly_perturb_binning(5)


def test_nscrad_fit_known():
    """Test N-SCRAD including running fit with a known source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with known spectra
    alg = NSCRAD(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0.0, 100.0], [100.0, 200.0]],  # need dummy windows
        nuisance_spectra=None,
    )
    alg.fit_known_sources(
        train_spectra,
        train_ids,
        [spec_src],
        n_windows=3,
        savefig=str(TEST_OUTPUTS_PATH / "test_algorithms_nscrad_fit_known.png"),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)


def test_nscrad_fit():
    """Test N-SCRAD including running fit with an unknown source."""
    # generate background data
    int_time = 1.0
    spectrum = 15 * np.ones(100)
    bin_edges = np.linspace(0, 3000, num=len(spectrum) + 1)
    np.random.seed(20)

    # make a dummy source spectrum
    spec_src = np.zeros(len(spectrum))
    spec_src[10:17] = 5.0

    gen = RandomSpectrumGenerator(
        [spectrum],
        src_spectra=[spec_src],
        int_time=int_time,
        batch_size=1000,
    )
    train_spectra, train_ids, _, _ = gen.next()

    # fit with unknown spectra
    alg = NSCRAD(
        int_time=int_time,
        bin_edges=bin_edges,
        spectral_windows=[[0.0, 100.0], [100.0, 200.0]],  # need dummy windows
        nuisance_spectra=None,
    )
    alg.fit(
        train_spectra,
        train_ids,
        n_bkg=1,
        n_windows=3,
        savefig=str(TEST_OUTPUTS_PATH / "test_algorithms_nscrad_fit_unknown.png"),
    )

    # evaluate algorithm on an individual test spectrum (analyze)
    gen.batch_size = 1
    test_spectrum, test_id, test_ts, test_lt = gen.next()
    alg.analyze(test_spectrum, test_ts, test_lt)

    # evaluate algorithm on a batch of test spectra (analyze_batch)
    gen.batch_size = 100
    test_spectra, test_ids, test_tss, test_lts = gen.next()
    alg.analyze_batch(test_spectra, test_tss, live_times=test_lts)
