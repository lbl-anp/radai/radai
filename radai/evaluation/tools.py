"""Helper variables and functions"""
import numpy as np
import pandas as pd
from ..data import ListmodeH5

# ------------------------------------------
# Helper variables
# ------------------------------------------

BIN_EDGES = np.linspace(np.sqrt(15), np.sqrt(3000), 129) ** 2

# The ordering in these lists could be modified to put similar sources adjacent
# in figures.
# Probably not great that these are hard coded though...
BKG = ["Background", "BKG"]
NORM = ["K-40", "Ra-226", "Th-232"]
Medical = [
    "Co-57",
    "F-18",
    "Tc-99m",
    "I-131",
    "Tl-201",
    "Cu-67",
    "Sr-90",
    "Lu-177",
    "Xe-133",
]
Industrial = ["Co-60", "Cs-137", "Ba-133", "Ir-192", "Am-241"]
NuclearMaterial = [
    "DepletedU",
    "DU",
    "NatU",
    "RefinedU",
    "LEU",
    "HEU",
    "FGPu",
    "WGPu",
]

category_mapping = {
    "BKG": BKG,
    "NORM": NORM,
    "Medical": Medical,
    "Industrial": Industrial,
    "NuclearMaterial": NuclearMaterial,
}
all_categories = list(category_mapping.keys())
all_sources = [s for ll in category_mapping.values() for s in ll]
alarm_categories = ["NuclearMaterial"]


sub_row = pd.Series(
    {
        "run_id": 0,
        "time": 0,
        "time_start": 0,
        "time_stop": 0,
        "label_1": "",
        "metric_1": 0.0,
        "label_2": "",
        "metric_2": 0.0,
        "label_3 ": "",
        "metric_3": 0.0,
    }
)

# ------------------------------------------
# Helper functions
# ------------------------------------------


def label2category(label):
    for cat, sourcelist in category_mapping.items():
        for source in sourcelist:
            if source in label:
                return cat


def label2isotope(label):
    """Map the full label to only the isotope part."""
    if label.startswith("Ir-192"):
        return "Ir-192"
    if "kg" in label:
        assert "-" in label
        tokens = label.split("-")
        return tokens[0]
    elif "shielding" in label:
        tokens = label.split("_shielding")
        return tokens[0]
    else:
        return label


def label2mass(label):
    """Map the full label to only the mass part."""
    if "kg" in label:
        assert "-" in label
        assert "kg_" in label
        tokens = label.split("kg_")
        tokens = tokens[0].split("-")
        return tokens[-1] + "kg"
    else:
        return ""


def label2shielding(label):
    """Map the full label to only the shielding part."""
    if "shielding" in label:
        assert "_shielding_id=" in label
        tokens = label.split("_shielding_id=")
        shld_id = int(tokens[1])
        return shld_id
    else:
        return 0


def open_radai_file(radai_h5_path, time_step_size=1000):
    # read in the radai H5 file
    data = ListmodeH5(
        radai_h5_path,
        time_step_size=time_step_size,
        num_time_bins=16,
        deep_validation=True,
        energy_bin_edges=BIN_EDGES,
    )
    # Per source type labeling
    data.label_manager.tier = 0
    return data
