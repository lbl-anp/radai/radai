"""Functions to validate and compute metrics on a submission"""
import numpy as np
import pandas as pd
from pathlib import Path

from .visualization import visualize_results
from .tools import label2category
from .tools import category_mapping, all_sources, alarm_categories, all_categories
from .tools import open_radai_file


# Expected submission column names
RUN_ID = "run_id"
TIME = "time"
LABEL_1 = "label_1"
METRIC_1 = "metric_1"


def validate_submission(
    answerkey_path,
    submission_path,
):
    answerkey_path = Path(answerkey_path)
    submission_path = Path(submission_path)

    ak = pd.read_csv(answerkey_path)
    submission = pd.read_csv(submission_path, comment="#")
    good = True

    # Check for columns needed and dtypes
    required_columns = [RUN_ID, TIME, LABEL_1, METRIC_1]
    for c in required_columns:
        if c not in submission.columns:
            print(f"{c} is a required column for submission")
            good = False

    # Check details of each column if provided.
    error_messages = []

    def _error_message_for_indices(column_name, indices, error_str):
        first_index = indices[0]
        first_value = submission.at[first_index, column_name]
        _message = (
            f'Line {indices[0] + 2}: {column_name}: "{first_value}" ' f"{error_str}"
        )
        if len(indices) > 1:
            _message += f" (and {len(indices) - 1} other instances)"
        return _message

    if RUN_ID in submission:
        # run_id values must be cast-able to integers.
        try:
            submission[RUN_ID] = submission[RUN_ID].astype(int)
        except ValueError:
            # If not, record an error message pointing to the first instance and
            # noting the number of similar errors.
            errant_indices = []
            for i, value in enumerate(submission[RUN_ID]):
                try:
                    int(value)
                except ValueError:
                    errant_indices.append(i)
            if errant_indices:
                message = _error_message_for_indices(
                    RUN_ID, errant_indices, "not an integer"
                )
                error_messages.append(message)
            else:
                # There must be at least one errant index, or the cast would
                # have succeeded.
                # TODO
                raise
        else:
            # 0 <= run_id values <= answer key run_id max
            expected_run_id_min = 0
            expected_run_id_max = ak.run_id.max()
            submission_run_id_min = submission[RUN_ID].min()
            submission_run_id_max = submission[RUN_ID].max()
            if (
                submission_run_id_min < expected_run_id_min
                or submission_run_id_max > expected_run_id_max
            ):
                message = (
                    f"{RUN_ID} values range [{submission_run_id_min}, "
                    f"{submission_run_id_max}] outside of expected range "
                    f"[{expected_run_id_min}, {expected_run_id_max}]"
                )
                error_messages.append(message)

    if TIME in submission:
        # time values must be cast-able to floats.
        try:
            submission[TIME] = submission[TIME].astype(float)
        except ValueError:
            # If not, record an error message pointing to the first instance and
            # noting the number of similar errors.
            errant_indices = []
            for i, value in enumerate(submission[TIME]):
                try:
                    float(value)
                except ValueError:
                    errant_indices.append(i)
            if errant_indices:
                message = _error_message_for_indices(
                    TIME, errant_indices, "not numeric"
                )
                error_messages.append(message)
            else:
                # There must be at least one errant index, or the cast would
                # have succeeded.
                # TODO
                raise
        else:
            # 0 <= time values <= answer key time max + a constant
            # TODO: For now, the upper bound is hard-coded to be:
            #  the length of the longest run
            #  minus
            #  the time of the latest alarm in the answer key for dataset v4.3.
            expected_time_min = 0
            expected_time_max = ak.time.max() + 122000.0
            submission_time_min = submission[TIME].min()
            submission_time_max = submission[TIME].max()
            if (
                submission_time_min < expected_time_min
                or submission_time_max > expected_time_max
            ):
                message = (
                    f"{TIME} values ranges [{submission_time_min}, "
                    f"{submission_time_max}] outside of expected range "
                    f"[{expected_time_min}, {expected_time_max}]"
                )
                error_messages.append(message)

    if LABEL_1 in submission:
        # label_1 values must be cast-able to strings.
        try:
            submission[LABEL_1] = submission[LABEL_1].astype(str)
        except ValueError as e:
            # This should not occur because values were read from a CSV.
            raise e
        else:
            # label_1 values must be in the pre-defined source list.
            labels_in_source_list = submission.label_1.apply(lambda x: x in all_sources)
            errant_indices = [
                i
                for i, is_in_list in enumerate(labels_in_source_list)
                if not is_in_list
            ]
            if errant_indices:
                source_list_str = ", ".join(f'"{s}"' for s in sorted(all_sources))
                message = _error_message_for_indices(
                    LABEL_1, errant_indices, f"not in source list: {source_list_str}"
                )
                error_messages.append(message)

    if METRIC_1 in submission:
        # metric values must be cast-able to floats.
        try:
            submission[METRIC_1] = submission[METRIC_1].astype(float)
        except ValueError:
            # If not, record an error message pointing to the first instance and
            # noting the number of similar errors.
            errant_indices = []
            for i, value in enumerate(submission[METRIC_1]):
                try:
                    float(value)
                except ValueError:
                    errant_indices.append(i)
            if errant_indices:
                message = _error_message_for_indices(
                    METRIC_1, errant_indices, "not numeric"
                )
                error_messages.append(message)
            else:
                # There must be at least one errant index, or the cast would
                # have succeeded.
                # TODO
                raise

    if error_messages:
        good = False
        for error in error_messages:
            print(error)

    if not good:
        print("Submission fails validation.")
    else:
        print("Submission passes validation.")

    return good


def answers_to_metrics(ak, fp_info, groupname):
    total_time_hr = (
        ak.run_id.max() * 1.0 - (ak.time_stop - ak.time_start).sum() / 1000.0 / 3600.0
    )

    metrics = ak.groupby(groupname).agg(
        encounters=pd.NamedAgg(column="detection", aggfunc=len),
        tp=pd.NamedAgg(column="detection", aggfunc="sum"),
        tc=pd.NamedAgg(column="classification", aggfunc="sum"),
        tid=pd.NamedAgg(column="identification", aggfunc="sum"),
    )
    metrics["fp"] = fp_info.groupby(groupname).apply(len)
    metrics = metrics.fillna(0)
    metrics.loc["Global"] = metrics.sum(axis=0)

    metrics["d_recall"] = metrics.tp / metrics.encounters
    metrics["c_recall"] = metrics.tc / metrics.encounters
    metrics["id_recall"] = metrics.tid / metrics.encounters

    # Need to be careful in computing precision since the definition of false positive
    # matters. Here it is implicit per the grouped category.

    metrics["fpr"] = metrics.fp / total_time_hr

    try:
        metrics.loc["Global", "far"] = metrics.loc[alarm_categories, "fpr"].sum()
    except KeyError:
        alarm_src_list = []
        for c in alarm_categories:
            alarm_src_list += category_mapping[c]
        for s in alarm_src_list:
            if s not in metrics.index.values:
                alarm_src_list.remove(s)
        metrics.loc["Global", "far"] = metrics.loc[alarm_src_list, "fpr"].sum()
    return metrics


def compute_metrics(
    radai_h5_path,
    answerkey_path,
    submission_path,
    max_alarm_time=160.0,
):
    data = None
    if radai_h5_path is not None:
        radai_h5_path = Path(radai_h5_path)
        data = open_radai_file(radai_h5_path, time_step_size=1000)
    answerkey_path = Path(answerkey_path)
    submission_path = Path(submission_path)

    ak = pd.read_csv(answerkey_path)
    submission = pd.read_csv(submission_path, comment="#")

    # submitted alarms longer than 90sec will be penalized more than once
    # max_alarm_time = 90.0  #already defined!

    ak["alg_label"] = "BKG"
    ak["alg_metric"] = -np.inf
    # Add any other performance things you might want to track here...

    # false positive tracking
    fp_info = []

    # Loop over each alarm in the submission
    for idx, row in submission.iterrows():
        # Find entries in the answer key that contain the submission's time
        alarm_mask = (
            (row.run_id == ak.run_id)
            & (row.time <= ak.time_stop)
            & (row.time >= ak.time_start)
        )

        # Determine whether there is a matching row in the answer key or not
        if alarm_mask.any():
            # there must be at most only one matching answer key row
            if alarm_mask.sum() > 1:
                raise RuntimeError(
                    "More than one answer key encounter is matched to this "
                    "submission; this means answer key encounters are "
                    "overlapping and this should never happen"
                )

            # If multiple submission rows match the same answer key row,
            # then keep the submission row having the largest metric
            # Note: in the future we might want to accommodate the ability to
            # select alarm types - e.g., when an HEU alarm has lower metric
            # that might override a benign NORM or medical alarm
            if (row.metric_1 > ak.loc[alarm_mask, "alg_metric"]).values[0]:
                ak.loc[alarm_mask, ["alg_label", "alg_metric"]] = (
                    row.label_1,
                    row.metric_1,
                )
        # Otherwise track false positive information
        else:
            alarm_duration = (row.time_stop - row.time_start) / 1000.0
            # penalize if the alarm_duration is longer than max_alarm_time
            for idy in range(int(1.0 + alarm_duration // max_alarm_time)):
                # Could also look up information about the background env
                # when the false alarm happened...
                fp_info.append(
                    {
                        "run_id": row.run_id,
                        "time": row.time,
                        "label": row.label_1,
                        "metric": row.metric_1,
                    }
                )

    # Track false positive info
    fp_info = pd.DataFrame(fp_info)
    # handle case where there are no false positives
    if len(fp_info) == 0:
        fp_info = pd.DataFrame(columns=["run_id", "time", "label", "metric"])
    fp_info["category"] = fp_info.label.map(label2category)

    # Process results to compute detection, classification, ID
    ak["detection"] = ak["alg_label"] != "BKG"
    # Classification doesn't work right now but if the dataset class gets updated
    # then this should work.
    ak["classification"] = ak["category"] == ak["alg_label"].apply(label2category)
    ak["identification"] = ak["isotope"] == ak["alg_label"]

    if data is not None:
        # create some figures
        _ = visualize_results(
            data.label_manager,
            ak.detection.values,
            ak.name.values,
            ak.snr_peak.values,
            label="detection",
        )
        _ = visualize_results(
            data.label_manager,
            ak.classification.values,
            ak.name.values,
            ak.snr_peak.values,
            label="classification",
        )
        _ = visualize_results(
            data.label_manager,
            ak.identification.values,
            ak.name.values,
            ak.snr_peak.values,
            label="identification",
        )

    # Compute metrics
    # calculate the total duration of all the testing data
    if data is not None:
        total_time_hr = 0
        for run_id in data.run_ids:
            total_time_hr += data.get_end_time(run_id) / 1000.0 / 3600.0
    else:
        print("Testing HDF5 not given; approximating each run as being 1 hour long")
        total_time_hr = len(np.unique(ak.run_id.values)) * 1.0
    print(total_time_hr)

    # total time in the data when there are no sources (BKG only)
    total_time_bkg_only_hr = (
        total_time_hr - (ak.time_stop - ak.time_start).sum() / 1000.0 / 3600.0
    )

    # Per encounter
    TPR = {}
    TCR = {}
    TIDR = {}

    # Per hour
    FPR = {}
    FAR = {}
    FAR["Global"] = 0.0

    ccs = all_categories + ["Global"]
    for cat in ccs:
        if cat == "BKG":
            continue
        if cat == "Global":
            mask = np.ones(len(ak), dtype=bool)
            mask2 = np.ones(len(fp_info), dtype=bool)
        else:
            mask = ak.category == cat
            mask2 = fp_info.category == cat

        if mask.sum() == 0:
            raise RuntimeError(
                f"No instances of {cat} category in the answer key; "
                "this should never happen"
            )
        else:
            TPR[cat] = ak.loc[mask, "detection"].sum() / mask.sum()
            TCR[cat] = ak.loc[mask, "classification"].sum() / mask.sum()
            TIDR[cat] = ak.loc[mask, "identification"].sum() / mask.sum()

        FPR[cat] = mask2.sum() / total_time_bkg_only_hr
        if cat in alarm_categories:
            FAR["Global"] += FPR[cat]

    metrics = pd.DataFrame(
        {
            "TPR": TPR,
            "TCR": TCR,
            "TIDR": TIDR,
            "FPR": FPR,
            "FAR": FAR,
        }
    )

    mpath = Path.joinpath(submission_path.parent, submission_path.stem + "_metrics.csv")
    #     mfn = submission_path.split('.csv')[0] + '_metrics.csv'
    metrics.to_csv(mpath)
    print(metrics)

    return mpath
