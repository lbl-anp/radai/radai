"""Functions to build answer key and example submission file"""
import numpy as np
import pandas as pd
import tqdm
from pathlib import Path
from .tools import (
    open_radai_file,
    label2category,
    label2isotope,
    label2mass,
    label2shielding,
)
import matplotlib.pyplot as plt

sub_row = pd.Series(
    {
        "run_id": 0,
        "time": 0,
        "time_start": 0,
        "time_stop": 0,
        "label_1": "",
        "metric_1": 0.0,
        "label_2": "",
        "metric_2": 0.0,
        "label_3 ": "",
        "metric_3": 0.0,
    }
)


def build_answerkey(
    radai_h5_path, snr_relative_thresh=0.05, time_step_size=1000, debug=True
):
    """Build the answer key for the dataset.

    Parameters
    ----------
    radai_h5_path : str
        Path to the source dataset.
    snr_relative_thresh : float
        Arbitrary parameter for fraction of max SNR to define window with.
    time_step_size : float
        Time discretization (in milliseconds) to use when calculating source
        windows.
    debug : bool
        Whether to make debugging plots of the source encounters.

    Returns
    -------
    answerkey_path : str
        Path to the CSV answer key that has been built.
    """

    radai_h5_path = Path(radai_h5_path)

    # read in the radai H5 file
    data = open_radai_file(radai_h5_path, time_step_size=time_step_size)

    # Build the answer key
    # Start with source location data frame
    sldf = data.get_source_locations_dataframe()

    # Add columns we want
    sldf["category"] = sldf.label.map(lambda x: label2category(x))
    sldf["isotope"] = sldf.label.map(lambda x: label2isotope(x))
    sldf["mass"] = sldf.label.map(lambda x: label2mass(x))
    sldf["shielding"] = sldf.label.map(lambda x: label2shielding(x))
    sldf["time_start"] = 0.0
    sldf["time_stop"] = 0.0
    sldf["time_max"] = 0.0
    sldf["time_start_bounding"] = 0.0
    sldf["time_stop_bounding"] = 0.0
    sldf["time_start_max_snr"] = 0.0
    sldf["time_stop_max_snr"] = 0.0
    sldf["snr_peak_recalc"] = 0.0
    sldf["snr_integral_recalc"] = 0.0

    # Loop on source encounters, getting runid and time of closest approach
    print("Processing {} encounters".format(len(sldf)))
    n_plots = 0
    n_encounters = 0
    for (rid, ts), row in tqdm.tqdm(sldf.iterrows(), total=len(sldf)):
        n_encounters += 1
        # Get SNR for the run in question
        runsnr = data.get_snr_for_run(rid)

        # Grab sufficient background around the encounter (bounding window)
        # to make sure we're getting the correct source window
        center_idx = int(
            np.round((ts - data.time_step_size / 2.0) / data.time_step_size)
        )
        n_below_thresh = 0
        n_below_left = 0
        n_below_right = 0
        width_left = 3
        width_right = 3
        while n_below_thresh < 25:
            # binwise SNRs for this source in the current window
            window_snrs = runsnr[row.label].iloc[
                center_idx - width_left : center_idx + width_right + 1
            ]
            n_below_thresh = (window_snrs < 0.01).sum()
            if n_below_thresh == 25:
                break
            # decide whether to expand window left or right
            # use a greedy approach
            snr_left = window_snrs.iloc[0]
            snr_right = window_snrs.iloc[-1]
            if snr_left > snr_right or center_idx + width_right + 1 >= len(runsnr):
                width_left += 1
            elif snr_left < snr_right or center_idx - width_left <= 0:
                width_right += 1
            else:
                if center_idx - width_left == 0:
                    width_right += 1
                    n_below_right += 1
                elif center_idx + width_right >= len(runsnr) - 1:
                    width_left += 1
                    n_below_left += 1
                else:
                    if n_below_left < n_below_right:
                        width_left += 1
                        n_below_left += 1
                    elif n_below_right < n_below_left:
                        width_right += 1
                        n_below_right += 1
                    else:
                        width_left += 1
                        n_below_left += 1
        time_start_bounding = window_snrs.index.values[0] - time_step_size / 2
        time_stop_bounding = window_snrs.index.values[-1] + time_step_size / 2
        assert time_start_bounding < time_stop_bounding

        # Flag times that are within our threshold (fraction of max)
        snr_peak = window_snrs.max()
        snrs_above_thresh = window_snrs > (snr_peak * snr_relative_thresh)
        # Get first and last occurance of timestamps above our threshold
        time_start = snrs_above_thresh.idxmax() - data.time_step_size / 2
        time_stop = snrs_above_thresh.iloc[::-1].idxmax() + data.time_step_size / 2
        time_max = window_snrs.idxmax()
        assert time_start < time_stop
        assert time_start <= time_max <= time_stop

        # find the window that when integrated maximizes the total gross-counts
        # SNR of the encounter
        # first find the total source S and background B counts
        run_s = data.get_snr_for_run(rid, method="s")
        run_b = data.get_snr_for_run(rid, method="s+b") - run_s
        # sort bins in decreasing over of S/B
        run_s_over_b = run_s / run_b
        s_over_b = run_s_over_b[row.label].iloc[
            center_idx - width_left : center_idx + width_right + 1
        ]
        s = run_s[row.label].iloc[
            center_idx - width_left : center_idx + width_right + 1
        ]
        b = run_b[row.label].iloc[
            center_idx - width_left : center_idx + width_right + 1
        ]
        idx_sort = np.argsort(s_over_b.values)[::-1]
        n_bins = 2
        idx_min = min(idx_sort[:n_bins])
        idx_max = max(idx_sort[:n_bins])
        idx_window = [idx_min, idx_max]
        S_window = s.iloc[idx_sort[:n_bins]].values.sum()
        B_window = b.iloc[idx_sort[:n_bins]].values.sum()
        snr_window = S_window / np.sqrt(B_window)
        while True:
            snr_last = snr_window
            idx_last = idx_window
            n_bins += 1
            idx_min = min(idx_sort[:n_bins])
            idx_max = max(idx_sort[:n_bins])
            idx_window = [idx_min, idx_max]
            S_window = s.iloc[idx_sort[:n_bins]].values.sum()
            B_window = b.iloc[idx_sort[:n_bins]].values.sum()
            snr_window = S_window / np.sqrt(B_window)
            if snr_window < snr_last:
                snr_window = snr_last
                idx_window = idx_last
                break
            elif n_bins >= len(idx_sort):
                raise RuntimeError(
                    "Maximum SNR not found in encounter " " - this should not occur"
                )
        time_start_max_snr = s_over_b.index[idx_min] - time_step_size / 2
        time_stop_max_snr = s_over_b.index[idx_max] + time_step_size / 2
        assert time_start_max_snr < time_stop_max_snr

        if (
            time_start > time_start_max_snr + 2 * time_step_size
            or time_stop < time_stop_max_snr - 2 * time_step_size
        ):
            print("Warning: regular windows are inside the SNR windows")

        # find several reasons for wanting to examine the result
        plot = False
        msg = ""
        # window widths
        bounding_width = (
            time_stop_bounding - time_start_bounding
        ) / data.time_step_size
        if bounding_width > 150:
            plot = True
            msg += f"bounding width {bounding_width:.1f} s\n"
        encounter_width = (time_stop - time_start) / data.time_step_size
        if encounter_width > 60:
            plot = True
            msg += f"encounter width {encounter_width:.1f} s\n"
        # shape of the encounter
        s_counts_per_time = run_s[row.label].values[
            center_idx - width_left : center_idx + width_right + 1
        ]
        times = (
            run_s.index.values[center_idx - width_left : center_idx + width_right + 1]
            / time_step_size
        )
        s_time_mean = (s_counts_per_time * times).sum() / s_counts_per_time.sum()
        s_time_std = np.sqrt(
            (s_counts_per_time * (times - s_time_mean) ** 2).sum()
            / s_counts_per_time.sum()
        )
        if np.abs(s_time_mean - center_idx) > 10.0:
            plot = True
            msg += f"encounter mean diff {s_time_mean - center_idx:.1f} s\n"
        if s_time_std > 20.0:
            plot = True
            msg += f"encounter std dev {s_time_std:.1f} s\n"
        # are there counts outside the encounter
        s_counts_per_time = np.array(run_s[row.label].values)[
            center_idx - 180 : center_idx + 181
        ]
        s_counts_per_time[180 - width_left : 180 + width_right] = 0
        s_counts_outside = s_counts_per_time.sum()
        if s_counts_outside > 20:
            plot = True
            msg += f"counts outside boundary {s_counts_outside}\n"

        if n_encounters in [
            144,
            145,
            196,
            197,
            366,
            367,
            414,
            415,
            530,
            531,
            650,
            710,
            797,
            798,
            1082,
            1238,
            1452,
            1609,
            1610,
            1648,
            1649,
            2168,
            2285,
            2286,
            2339,
            2370,
            2382,
            2383,
            2645,
            2646,
            2690,
        ]:
            plot = True
            msg += f"n_encounters: {n_encounters}\n"

        if debug and plot:
            n_plots += 1
            print("Plotting", n_encounters, n_plots)
            print(msg)

            plt.figure(num=n_encounters)
            plt.title(f"Run {rid} Source {row.label} at time {center_idx} s")
            plt.fill_between(
                np.arange(len(runsnr)), runsnr[row.label].values, color="C0", alpha=0.2
            )
            plt.plot(runsnr[row.label].values, lw=0.5, color="k")
            plt.vlines(
                [
                    time_start_bounding / data.time_step_size,
                    time_stop_bounding / data.time_step_size,
                ],
                0,
                5,
                color="C3",
                lw=1.5,
                alpha=0.8,
                label="Bounding window",
            )
            plt.vlines(
                [time_start / data.time_step_size, time_stop / data.time_step_size],
                0,
                5,
                color="C2",
                lw=2,
                alpha=0.8,
                label="Final encounter window",
            )
            plt.vlines(
                [
                    time_start_max_snr / data.time_step_size,
                    time_stop_max_snr / data.time_step_size,
                ],
                0,
                5,
                color="C4",
                lw=1.5,
                alpha=0.8,
                label="Maximum SNR window",
            )
            if len(msg) > 0:
                plt.text(center_idx + 20, 0.75, msg[:-1])
            plt.legend()
            plt.xlabel("Index")
            plt.ylabel("Source SNR")
            plt.ylim(0, 2)
            plt.xlim(center_idx - 150, center_idx + 150)

        assert (
            time_stop - time_start
        ) / data.time_step_size <= 160, "Cannot have such a wide window"

        # Add timestamps to the SLDF
        sldf.loc[
            (rid, ts),
            [
                "time_start",
                "time_stop",
                "time_max",
                "time_start_bounding",
                "time_stop_bounding",
                "time_start_max_snr",
                "time_stop_max_snr",
                "snr_peak_recalc",
                "snr_integral_recalc",
            ],
        ] = (
            time_start,
            time_stop,
            time_max,
            time_start_bounding,
            time_stop_bounding,
            time_start_max_snr,
            time_stop_max_snr,
            snr_peak,
            snr_window,
        )

    if debug:
        plt.show()

    # remove redundant column
    assert np.all(sldf.name == sldf.label)
    sldf.drop("label", axis=1, inplace=True)
    print(sldf.columns)

    # Change indexing
    answerkey = sldf.reset_index(level=1)
    answerkey.reset_index(drop=False, inplace=True)

    # Write to csv file
    akpath = Path.joinpath(radai_h5_path.parent, radai_h5_path.stem + "_answerkey.csv")
    #     akfn = radai_h5_path.split('.h5')[0] + '_answerkey.csv'
    answerkey.to_csv(akpath)

    return akpath


def build_example_submission(
    radai_h5_path,
    answerkey_path,
    detection_scale=5.0,
    identification_scale=12.0,
    false_positive_scale=0.08,
):

    radai_h5_path = Path(radai_h5_path)
    answerkey_path = Path(answerkey_path)

    # read in the answer key
    ak = pd.read_csv(answerkey_path)

    # loop over answer key, get some right
    # add false positives

    submissions = []
    for runid, row in ak.iterrows():

        # Randomly sample success based on SNR
        detection = np.random.rand() < (row.snr_peak / detection_scale)
        # If no detection, no submission, continue
        if not detection:
            continue
        # Start an entry
        s = sub_row.copy()
        s.run_id = row.run_id

        # Determine if ID is correct and enter values
        ident = np.random.rand() < (row.snr_peak / identification_scale)
        if ident:
            s.label_1 = row.isotope
        else:
            s.label_1 = np.random.choice(ak.isotope.unique())
        s.metric_1 = np.random.normal(loc=row.snr_peak, scale=1.0)

        # enter times
        s.time = row.time_start + np.random.rand() * (row.time_stop - row.time_start)
        s.time_start = s.time - int(1000 * np.random.rand())
        s.time_stop = s.time + int(3000 * np.random.rand())

        # add submission to list
        submissions.append(s)

        # randomly add a few false alarms
        if np.random.rand() < false_positive_scale:
            ss = s.copy()
            ss.label = np.random.choice(ak.isotope.unique())
            ss.time = row.time_stop + 3000
            ss.time_start = ss.time - int(1000 * np.random.rand())
            ss.time_stop = ss.time + int(3000 * np.random.rand())
            # append if we're below the hour mark
            if ss.time_stop < 3600000:
                submissions.append(ss)

    # drop into a dataframe
    submission = pd.DataFrame(submissions)

    # write to csv files
    spath = Path.joinpath(
        radai_h5_path.parent, radai_h5_path.stem + "_example_submission.csv"
    )
    #     sfn = radai_h5_path.split('.h5')[0] + '_example_submission.csv'
    submission.to_csv(spath)

    return spath
