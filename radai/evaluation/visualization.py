"""Visualization tools"""

import matplotlib.pyplot as plt
import numpy as np
from .tools import all_sources, category_mapping

GREY12 = "#1f1f1f"


def snr_vs_success(boolean_results, true_labels, true_snr, metric_name):
    fig, ax = plt.subplots(figsize=(10, 7))

    # build a list of relevant sources (sorted by category in import)
    # then map to integers for plotting
    source_names = []
    ts = np.unique(true_labels)
    for s in all_sources:
        if s in ts:
            source_names.append(s)
            if (s == "BKG") or (s == "Background") or (s == "Bkg"):
                source_names.remove(s)
    name2num = dict(zip(source_names, np.arange(len(source_names))))
    numeric_labels = np.array([*map(name2num.get, true_labels)])

    ax.scatter(
        numeric_labels[~boolean_results] - 0.1,
        true_snr[~boolean_results],
        marker="_",
        color="red",
    )
    ax.scatter(
        numeric_labels[boolean_results] + 0.1,
        true_snr[boolean_results],
        marker="+",
        color="green",
    )

    ax.set_xticks(np.unique(numeric_labels))
    ax.set_xticklabels(
        source_names,
        rotation="vertical",
        fontsize=8,
    )

    ax.semilogy()
    ax.set_xlabel("source id")
    ax.set_ylabel("peak snr")
    ax.set_title(f"{metric_name} positives and negatives")
    return fig, ax


def radar_performance_vs_label(
    label_metrics,
):

    # Set up some variables
    sources = []
    cats = []
    for cat, slist in category_mapping.items():
        for s in slist:
            if s in label_metrics.index.values:
                sources.append(s)
                cats.append(cat)

    # Values for the x axis
    angles = np.linspace(0.0, 2 * np.pi, len(label_metrics) - 1, endpoint=False)
    cmap = plt.get_cmap("inferno")
    names = np.unique(cats)
    colors = cmap(np.linspace(0, 1, len(names)))
    cat2color = {k: v for k, v in zip(names, colors)}
    cc = [cat2color[c] for c in cats]

    # Some layout stuff
    # Initialize layout in polar coordinates
    fig, ax = plt.subplots(figsize=(10, 10), subplot_kw={"projection": "polar"})

    # Set background color to white, both axis and figure.
    fig.patch.set_facecolor("white")
    ax.set_facecolor("white")
    ax.set_theta_offset(0 * np.pi / 2)
    ax.set_ylim(0, 1.0)

    # Add geometries to the plot
    # Add bar to represent different source categories
    ax.bar(
        angles,
        np.ones(len(angles)) * 0.02,
        bottom=0.98,
        alpha=0.5,
        width=2 * np.pi / len(sources),
        zorder=1,
        color=cc,
    )

    # Add bars to represent the cumulative track lengths
    drc = label_metrics.loc[sources, "d_recall"]
    crc = label_metrics.loc[sources, "c_recall"]
    idrc = label_metrics.loc[sources, "id_recall"]

    ax.bar(
        angles,
        drc - crc,
        bottom=crc,
        alpha=0.5,
        width=2 * np.pi / len(sources),
        zorder=2,
        color=plt.cm.Set1(2),
        label="detection",
    )
    ax.bar(
        angles,
        crc - idrc,
        bottom=idrc,
        alpha=0.5,
        width=2 * np.pi / len(sources),
        zorder=3,
        color=plt.cm.Set1(1),
        label="classification",
    )
    ax.bar(
        angles,
        idrc,
        bottom=0,
        alpha=0.5,
        width=2 * np.pi / len(sources),
        zorder=4,
        color=plt.cm.Set1(0),
        label="identification",
    )

    # Add dashed vertical lines. These are just references
    ax.vlines(angles, drc, 0.98, color=GREY12, ls=(0, (4, 4)), zorder=11, alpha=0.4)

    # Add labels for the regions
    # Set the labels
    ax.set_thetagrids(np.rad2deg(angles), sources)
    ax.tick_params(axis="x", pad=10.3)
    ax.tick_params(axis="y", zorder=20, labelcolor="k")

    def format_func(value, tick_number):
        return f"{int(value*100)}%"

    ax.yaxis.set_major_formatter(plt.FuncFormatter(format_func))
    ax.legend(loc=1)
    ax.grid(False, axis="x")

    plt.tight_layout()
    return fig, ax


def visualize_results(label_manager, boolean_results, true_labels, true_snr, label):
    fig, ax = plt.subplots(figsize=(10, 10))
    numeric_labels = np.array(list(map(label_manager.labels_str2num, true_labels)))
    ax.scatter(
        numeric_labels[~boolean_results] - 0.1,
        true_snr[~boolean_results],
        marker="*",
        color="red",
    )
    ax.scatter(
        numeric_labels[boolean_results] + 0.1,
        true_snr[boolean_results],
        marker="*",
        color="green",
    )

    ax.set_xticks(np.unique(numeric_labels))
    ax.set_xticklabels(
        label_manager.labels_num2str(np.unique(numeric_labels)),
        rotation="vertical",
        fontsize=8,
    )

    ax.semilogy()
    ax.set_xlabel("source id")
    ax.set_ylabel("peak snr")
    ax.set_title(f"{label} positives and negatives")
    return fig, ax
