"""Core functions across repo."""
import numpy as np

scalar_like = bool, str, bytes, bytearray, int, float
array_like = tuple, list, np.ndarray


class RadaiWarning(UserWarning):
    """General radai warning to be subclassed for further specificity."""


class RadaiError(Exception):
    """General radai error to be subclassed for further specificity."""


class RadaiAlgorithmWarning(RadaiWarning):
    """Warning related to algorithm training, testing or execution."""


class RadaiAlgorithmError(RadaiError):
    """Error related to algorithm training, testing or execution."""
