"""Multi-tier labeler data structure and mapping helpers."""
import re
from typing import Union, Callable, Iterable, Tuple
import numpy as np
import pandas as pd
from .tools import RadaiDataSetError, BACKGROUND_NAMES


def _get_unicode_dtype(items: Iterable[str]) -> str:
    return f"U{max([len(x) for x in items])}"


class LabelManager:

    _ELEMENT_REGEX = (
        r"A[cglmrstu]|B[aehikr]?|C[adeflmorsu]?|D[by]|E[rsu]|"
        r"F[emr]?|G[ade]|H[efgos]?|I[nr]?|K[r]?|L[airu]|M[dgnot]|"
        r"N[abdeiop]?|O[s]?|P[abdmortu]?|R[abefhnu]|S[bcegimnr]?|"
        r"T[abcehilm]|U?|V?|W?|X[e]|Y[b]?|Z[nr]"
    )
    _A_REGEX = r"[-]?([1-2][0-9][0-9]|[1-9][0-9]|[1-9])[m]?"
    _SPECIAL_REGEX = r"([a-zA-Z]+)(Pu|U)"
    _BACKGROUND_REGEX = r"BKG|Background"
    _ISOTOPE_REGEX = re.compile(
        r"^" f"(({_ELEMENT_REGEX}){_A_REGEX})|({_SPECIAL_REGEX})|({_BACKGROUND_REGEX})"
    )

    def __init__(
        self,
        *sources: Iterable[Union[dict, Tuple[int, str, str, str, bool]]],
        infer_isotope: bool = True,
        tier: Union[int, str] = 0,
        clear_caches: Callable = None,
    ) -> None:
        """Source ID's and names with multiple "tiers" of labeling.

        Parameters
        ----------
        sources : tuple or list or dict
            Source definition for input to `append`:
                [id_, name, [isotope, [category, [enabled]]]]
            or:
                {
                    "id": int,
                    "name": str,
                    "isotope": str,
                    "category": str,
                    "enabled": bool
                }
            The fields are:
                id_ : int
                    Source id, i.e. `1`
                name : str
                    Source name, i.e. `Cs-137-point`
                isotope : str, optional
                    Source isotope, i.e. `Cs-137`, by default "unknown" unless
                    `infer_isotope` in which case attempt to extract the isotope
                    name from the source name.
                category : str, optional
                    Source category, i.e. `Industrial`, by default "unknown"
                enabled : str, optional
                    If this source is enabled for label processing, by default
                    True.
            By default isotope and category are `unknown`. The `isotope` can be
            inferred from the `name` if `infer_isotope` and a failure will
            revert back to "unknown".
        infer_isotope : bool, optional
            Infer the isotope (tier 1 name) from the raw source name. Some valid
            examples are:
                Cs137_bare  -> Cs137
                Co-60_steel -> Co-60
                Tc-99m      -> Tc-99m
                NaturalU    -> NaturalU
            Only has an effect when `append` is called without an `isotope` kwarg.
            See the `_ISOTOPE_REGEX` for more details. By default True
        tier : Union[int, str], optional
            Labeling tier:
                0, 'id', 'name': use source name in H5 root attributes (metadata)
                2, 'isotope': use source enum isotope names
                3, 'category': use source enum categories
            by default 0
        clear_caches : Callable, optional
            Object to call when changing the tier to make sure the caches do
            not contain incorrect data.

        Raises
        ------
        NotImplementedError
            If a `source` in `sources` is a dictionary (coming soon!)
        TypeError
            If a `source` in `sources` is not a tuple, list or dictionary.
        """
        self._clear_caches = (lambda: None) if clear_caches is None else clear_caches
        self._data = pd.DataFrame(columns=["name", "isotope", "category", "enabled"])
        self._data.index.name = "id"
        self.append(*sources, infer_isotope=infer_isotope)
        self._tier = 0
        self.tier = tier

    def __len__(self) -> int:
        """The number of sources in this enum.

        Returns
        -------
        int
        """
        return self._data.shape[0]

    def __str__(self):
        s = f"{self.__class__.__name__}(tier {self.tier}, "
        if len(self) == 0:
            s += "empty)"
        else:
            s += f"{len(self)} items, {self.num_labels} labels)"
            for t in self.as_tuples():
                s += f"\n    {t}"
        return s

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"{', '.join(str(t) for t in self.as_tuples())}, tier={self.tier}"
            ")"
        )

    @property
    def tier(self) -> int:
        return self._tier

    # --------------------------------------------------------------------------
    # In-place modifications
    # --------------------------------------------------------------------------

    @tier.setter
    def tier(self, tier: Union[int, str]) -> None:
        """Source labeling group tier

        Parameters
        ----------
        tier : Union[int, str]
            Labeling tier:
                0, 'id', 'name': use source name in H5 root attributes (metadata)
                2, 'isotope': use source enum isotope names
                3, 'category': use source enum categories

        Raises
        ------
        RadaiDataSetError
            If the `tier` is not one of {0, 1, 2, 'id', 'name', 'isotope', 'category'}
        """
        if tier in (0, "id", "name"):
            tier = 0
        elif tier in (1, "isotope"):
            tier = 1
        elif tier in (2, "category"):
            tier = 2
        else:
            raise RadaiDataSetError(f"Unknown labeling tier: {tier}")
        if self.tier != tier:
            self._clear_caches()
        self._tier = tier

    def append(
        self,
        *sources: Iterable[Union[dict, Tuple[int, str, str, str]]],
        infer_isotope: bool = True,
    ) -> None:
        """Set source name and id. Background is always 0 (zero).

        Parameters
        ----------
        sources : tuple or list or dict
            Source definition for input to `append`:
                [id_, name, [isotope, [category]]]
            or:
                {"id": id_, "name": name, "isotope": isotope, "category": category}
            The fields are:
                id_ : int
                    Source id, i.e. `1`
                name : str
                    Source name, i.e. `Cs-137-point`
                isotope : str, optional
                    Source isotope, i.e. `Cs-137`, by default "unknown" unless
                    `infer_isotope` in which case attempt to extract the isotope
                    name from the source name.
                category : str, optional
                    Source category, i.e. `Industrial`, by default "unknown"
            By default isotope and category are `unknown`. The `isotope` can be
            inferred from the `name` if `infer_isotope` and a failure will
            revert back to "unknown".
        infer_isotope_names : bool, optional
            Infer the isotope name from the source name using the regex in
            `self._ISOTOPE_REGEX`. This only has effect if `isotope` is not
            provided. The following are examples of valid isotope names which
            will be extracted from the start of the source name:
                Cs-137
                Tc99m
                RefinedU
                Background
            Take note that the first character must be uppercase and isotope
            names are allowed with and without a hyphen (-) between the element
            name and the mass number (A). A metastable suffix is also allowed (m).
            By default True
        """
        data = {}
        for s in sources:
            # Handle collection
            if isinstance(s, dict):
                id_ = s["id"]
                name = s["name"]
                isotope = s.get("isotope", "unknown")
                category = s.get("category", "unknown")
                enabled = s.get("enabled", True)
            elif isinstance(s, (tuple, list)):
                id_ = s[0]
                name = s[1]
                isotope = "unknown" if len(s) < 3 else s[2]
                category = "unknown" if len(s) < 4 else s[3]
                enabled = True if len(s) < 5 else s[4]
            else:
                raise TypeError(f"Invalid source type ({type(s)}): {s}")
            # Casting
            id_ = int(id_)
            name = str(name)
            isotope = str(isotope)
            category = str(category)
            enabled = bool(enabled)
            # Special name handling
            if id_ in self.ids:
                raise RadaiDataSetError(f"{id_} already exists in {self.ids}")
            if id_ == 0:
                assert (
                    name in BACKGROUND_NAMES
                ), f"ID 0 must be one of {BACKGROUND_NAMES}. Not: {name}"
            if name in self.names:
                raise RadaiDataSetError(f"{name} already exists in {self.names}")
            if isotope == "unknown" and infer_isotope:
                try:
                    isotope = self._ISOTOPE_REGEX.search(name).group()
                except AttributeError:
                    # If the regex is not found, the search returns None
                    pass
            # Save for later
            data[id_] = {
                "name": name,
                "isotope": isotope,
                "category": category,
                "enabled": enabled,
            }
        # Append everything at once
        self._data = pd.concat(
            [self._data, pd.DataFrame.from_dict(data, orient="index")]
        )
        self._data.index.name = "id"
        self._data["enabled"] = self._data["enabled"].astype(bool)
        self._clear_caches()

    def disable_items(self, *names: str) -> None:
        msk = np.zeros(len(self), dtype=bool)
        for name in names:
            if name in BACKGROUND_NAMES:
                raise RadaiDataSetError(f"Cannot disable background label: {name}")
            for c in ("name", "isotope", "category"):
                # If any names match, that row will be disabled
                msk |= self._data[c].values == str(name)
        if msk.sum() > 0:
            self._clear_caches()
            self._data.loc[msk, "enabled"] = False

    def enable_items(self, *names: str) -> None:
        msk = np.zeros(len(self), dtype=bool)
        self._clear_caches()
        for name in names:
            if name.lower() == "all":
                self._data.loc[:, "enabled"] = True
                return
            for c in ("name", "isotope", "category"):
                # If any names match, that row will be enabled
                msk |= self._data[c].values == str(name)
        if msk.sum() > 0:
            self._data.loc[msk, "enabled"] = True

    # --------------------------------------------------------------------------
    # Views of data
    # --------------------------------------------------------------------------

    @property
    def data(self) -> pd.DataFrame:
        return self._data.copy(deep=True)

    def get_data(self, only_enabled: bool = True, copy: bool = True):
        if only_enabled:
            data = self._data.loc[self._data["enabled"].values]
        else:
            data = self._data
        if copy:
            data = data.copy(deep=True)
        return data

    @property
    def num_disabled(self) -> int:
        return np.sum(~self._data["enabled"].values)

    def as_tuples(self):
        """Generate tuple for each source entry

        Yields
        ------
        tuple
            Source id, name, isotope, category
        """
        for row in self._data.itertuples(index=True):
            yield tuple(row)

    def as_dicts(self):
        """Generate dictionary for each source entry

        Yields
        ------
        dict
            Source items keyed by: id, name, isotope, category
        """

        for row in self._data.itertuples(index=True):
            yield dict(row)

    @property
    def names(self):
        """Source names

        Returns
        -------
        list
        """
        return list(self._data["name"])

    @property
    def ids(self):
        """Source ids

        Returns
        -------
        list
        """
        return self._data.index.tolist()

    # --------------------------------------------------------------------------
    # Tools to convert between tiers
    # --------------------------------------------------------------------------

    def _ids_array_compat(
        self, ids: Union[list, np.ndarray]
    ) -> Tuple[bool, np.ndarray]:
        """Helper method for id2* converter methods

        Parameters
        ----------
        ids : Union[list, np.ndarray]
            Source id(s) to check and convert to compatible array.

        Returns
        -------
        is_scalar : bool
            If the input was a scalar integer.
        out : np.ndarray[int]
            Converted and checked ids for further processing.

        Raises
        ------
        RadaiDataSetError
            If the ids are not in the valid set of ids.
        TypeError
            If the ids are not ints.
        """
        # Convert to a numpy array if needed
        if isinstance(ids, (int, np.integer)):
            is_scalar = True
            ids = np.asarray([ids]).astype(np.uint16)
        else:
            is_scalar = False
            ids = np.asarray(ids).astype(np.uint16)
        # Unique inputs
        unique_ids = np.unique(ids)
        # Check for invalid input ids
        if np.isin(unique_ids, self.ids, assume_unique=True, invert=True).any():
            raise RadaiDataSetError(
                f"One or more input `id`'s are not in the valid set of ids: {self.ids}"
            )
        # Done!
        return is_scalar, ids, unique_ids

    def name2id(self, name: str) -> int:
        """Get source id from name.

        Parameters
        ----------
        name : str

        Returns
        -------
        int
        """
        return self._data.index[self._data["name"] == name][0]

    def id2name(self, ids: Union[int, Iterable[int]]) -> Union[str, np.ndarray]:
        """Get source name from id(s).

        Parameters
        ----------
        ids : int or Iterable[int]
            Source id(s) to convert.

        Returns
        -------
        str or np.ndarray[str]
        """
        is_scalar, ids, _ = self._ids_array_compat(ids)
        out = np.empty(shape=len(ids), dtype=_get_unicode_dtype(self.names))
        out[:] = self._data.loc[ids, "name"].values
        return str(out[0]) if is_scalar else out

    # TODO remove as_strings
    def id2isotope(
        self, ids: Union[int, Iterable[int]], as_strings: bool = True
    ) -> Union[str, np.ndarray]:
        """Get source isotope from id(s).

        Parameters
        ----------
        ids : int or Iterable[int]
            Source id(s) to convert.
        as_strings : bool, optional
            Get label strings instead of indices of the unique values (as in
            those used during target array encoding), by default False

        Returns
        -------
        str or np.ndarray[str]
        """
        is_scalar, ids, unique_ids = self._ids_array_compat(ids)
        iso_list = self._data["isotope"].unique().tolist()
        if is_scalar:
            out = self._data.loc[ids[0], "isotope"]
            return out if as_strings else iso_list.index(out)
        else:
            if as_strings:
                # Get the isotope strings (can lead to very large arrays)
                out = np.empty(shape=len(ids), dtype=_get_unicode_dtype(iso_list))
                out[:] = self._data.loc[ids, "isotope"].values
                return out
            else:
                # Get the isotope indices
                out = np.empty(len(ids), dtype=np.uint16)
                for i in unique_ids:
                    out[ids == i] = iso_list.index(self._data.loc[i, "isotope"])
                return out

    # TODO remove as_strings
    def id2category(
        self, ids: Union[int, Iterable[int]], as_strings: bool = True
    ) -> Union[str, np.ndarray]:
        """Get source category from id(s).

        Parameters
        ----------
        ids : int or Iterable[int]
            Source id(s) to convert.
        as_strings : bool, optional
            Get label strings instead of indices of the unique values (as in
            those used during target array encoding), by default False

        Returns
        -------
        str or np.ndarray[str]
        """
        is_scalar, ids, unique_ids = self._ids_array_compat(ids)
        cat_list = self._data["category"].unique().tolist()
        if is_scalar:
            out = self._data.loc[ids[0], "category"]
            return out if as_strings else cat_list.index(out)
        else:
            if as_strings:
                # Get the category strings (can lead to very large arrays)
                out = np.empty(shape=len(ids), dtype=_get_unicode_dtype(cat_list))
                out[:] = self._data.loc[ids, "category"].values
                return out
            else:
                # Get the category indices
                out = np.empty(len(ids), dtype=np.uint16)
                for i in unique_ids:
                    out[ids == i] = cat_list.index(self._data.loc[i, "category"])
                return out

    # TODO change as_strings to as_nums like below
    def id2label(
        self, ids: Union[int, Iterable[int]], as_strings: bool = True
    ) -> np.ndarray:
        """Get label(s) from raw source ids (vectorized per internal source id.)

        Parameters
        ----------
        ids : int or Iterable[int]
            Source id(s) to convert.
        as_strings : bool, optional
            Get label strings instead of indices of the unique values (as in
            those used during target array encoding), by default True

        Returns
        -------
        np.ndarray
            Array of label integer indices or strings depending on as_strings.

        Raises
        ------
        RadaiDataSetError
            If one or more of the ids are not in this object.
            If one or more of the ids are disabled.
        TypeError
            If the `ids` are not integers.
        """
        enabled = self.get_data(only_enabled=True, copy=True)
        enabled_ids = enabled.index.values
        if not np.all(np.isin(ids, enabled_ids)):
            raise RadaiDataSetError(
                "The following source ID's cannot be converted to labels because "
                f"they are disabled: {set(np.unique(ids)) - set(enabled_ids)}"
            )
        # If you want string scalars or arrays we can use the methods above
        if as_strings:
            if self.tier == 0:
                return self.id2name(ids)
            elif self.tier == 1:
                return self.id2isotope(ids, as_strings=True)
            else:
                return self.id2category(ids, as_strings=True)
        # Otherwise lets not allocate large string arrays and build the arrays
        # ourselves based on the enabled sources. This is done by creating
        # another column in the `enabled` dataframe and then building a new
        # integer array for the label number output.
        column = {0: "name", 1: "isotope", 2: "category"}[self.tier]
        unique_labels = enabled[column].unique().tolist()
        enabled["label_num"] = 0
        for i in enabled.index:
            enabled.loc[i, "label_num"] = unique_labels.index(enabled.loc[i, column])
        # Doing this with enabled.loc is takes double the time (int64 vs uint16)
        # out = enabled.loc[ids, "label_num"]
        is_scalar, ids, unique_ids = self._ids_array_compat(ids)
        if is_scalar:
            return enabled.loc[ids[0], "label_num"]
        out = np.empty(len(ids), dtype=np.uint16)
        for i in unique_ids:
            out[ids == i] = enabled.loc[i, "label_num"]
        return out

    # --------------------------------------------------------------------------
    # Labels are the values for the active tier for the enabled data rows
    # --------------------------------------------------------------------------

    def get_labels_list(self, as_nums: bool = False) -> list:
        # NOTE the pandas `unique` methods do NOT sort so these are all ordered
        #      by the appearance in the underlying dataframe
        enabled = self.get_data(only_enabled=True, copy=False)
        if self.tier == 0:
            # The names are guaranteed to be unique in the append method
            labels = enabled["name"].tolist()
        elif self.tier == 1:
            labels = enabled["isotope"].unique().tolist()
        else:
            labels = enabled["category"].unique().tolist()
        if as_nums:
            labels = list(range(len(labels)))
        return labels

    @property
    def num_labels(self):
        return len(self.get_labels_list())

    @property
    def background_label(self):
        return self.get_labels_list()[0]

    @property
    def labels_num2str_map(self) -> dict:
        """Dictionary to convert label number (int) to name (str).

        Returns
        -------
        dict
        """
        return {i: l for i, l in enumerate(self.get_labels_list())}

    @property
    def labels_str2num_map(self) -> dict:
        """Dictionary to converty label name (str) to number (int).

        Returns
        -------
        dict
        """
        return {l: i for i, l in enumerate(self.get_labels_list())}

    def labels_num2str(self, num: Union[int, Iterable[int]]) -> Union[int, np.ndarray]:
        """Convert label number(s) (int or array of int's) to name(s) (str or array).

        Parameters
        ----------
        num : int or Iterable[int]
            Label number.

        Returns
        -------
        str or array[str]
            Label name.
        """
        n2l = self.labels_num2str_map
        if isinstance(num, (int, np.integer)):
            return n2l[int(num)]
        num = np.asarray(num, dtype=int)
        out = np.empty(len(num), dtype=_get_unicode_dtype(n2l.values()))
        for i in np.unique(num):
            out[num == i] = n2l[int(i)]
        return out

    def labels_str2num(self, name: Union[str, Iterable[str]]) -> Union[int, np.ndarray]:
        """Convert label name (str) to number (int). This is vectorized but
        will be slow for large arrays because it uses a simple loop.

        Parameters
        ----------
        name : str pr Iterable[str]
            Label name(s).

        Returns
        -------
        int or array[int]
            Label number(s).
        """
        l2n = self.labels_str2num_map
        if isinstance(name, (str, np.string_, np.unicode_)):
            return l2n[str(name)]
        name = np.asarray(name, dtype=_get_unicode_dtype(l2n.keys()))
        out = np.empty(len(name), dtype=int)
        for s in np.unique(name):
            out[name == s] = l2n[str(s)]
        return out
