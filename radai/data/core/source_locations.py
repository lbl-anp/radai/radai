"""Source location data structure based on a DataFrame."""
from typing import Iterable
import numpy as np
import pandas as pd
from .tools import BACKGROUND_NAMES


class SourceLocations(object):

    __slots__ = "_data"

    def __init__(
        self,
        names: Iterable[str],
        times: Iterable[int],
        distances: Iterable[float],
        locations: Iterable[int] = None,
        shieldings: Iterable[str] = None,
        standoffs: Iterable[float] = None,
        snrs_peak: Iterable[float] = None,
        snrs_integral: Iterable[float] = None,
        labels: Iterable[str] = None,
        **columns,
    ) -> None:
        self._data = pd.DataFrame.from_dict(
            dict(
                name=names,
                time=times,
                distance=distances,
                location=locations,
                shielding=shieldings,
                standoffs=standoffs,
                snr_peak=snrs_peak,
                snr_integral=snrs_integral,
                label=labels,
                **columns,
            )
        )

    def __iter__(self):
        for _, row in self.data.iterrows():
            yield row

    def __len__(self):
        return len(self.data)

    def __getitem__(self, arg):
        return self.data.iloc[arg]

    def __str__(self) -> str:
        return str(self.data)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.data.to_dict()})"

    @property
    def data(self) -> pd.DataFrame:
        return self._data

    def get(self, column: str, unique: bool = False) -> np.ndarray:
        values = self.data[column].values
        return np.unique(values) if unique else values

    def append(
        self,
        name: str,
        time: int,
        distance: float,
        location: int = None,
        shielding: str = None,
        standoff: float = None,
        snr_peak: float = None,
        snr_integral: float = None,
        label: str = None,
        **columns,
    ) -> None:
        self._data.append(
            dict(
                name=name,
                time=time,
                distance=distance,
                location=location,
                shielding=shielding,
                standoff=standoff,
                snr_peak=snr_peak,
                snr_integral=snr_integral,
                label=label,
                **columns,
            )
        )

    def sort_by_time(self) -> None:
        self._data = self._data.sort_values("time")

    def slice_by_time(self, start: int, end: int) -> None:
        self._data = self.data.loc[
            (self.data["time"] >= start) & (self.data["time"] <= end)
        ]

    def drop_background(self) -> None:
        keep = np.ones_like(self.data["time"].values, dtype=np.bool_)
        for name in BACKGROUND_NAMES:
            keep &= self.data["name"] != name
        self._data = self.data.loc[keep]
