from ...tools import sqrt_bins

DEFAULT_ENERGY_BIN_EDGES = sqrt_bins(0, 3000, 128)
BACKGROUND_NAMES = {"BKG", "background"}


class RadaiDataSetError(Exception):
    """Exception for radai dataset IO or usage."""


class RadaiDataSetWarning(UserWarning):
    """Warning for radai dataset IO or usage."""
