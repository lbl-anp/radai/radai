"""Base class for radai data sets."""
import abc
from typing import Union
import numpy as np
from .data_tuples import SpectrumData, WaterfallData, BatchData
from .labeler import Labeler
from .label_manager import LabelManager
from .source_locations import SourceLocations


class DataSet(abc.ABC):
    """Abstract base class for radai datasets."""

    def __init__(
        self,
        run_ids: np.ndarray,
        label_manager: LabelManager,
        deep_validation: bool = False,
    ) -> None:
        # Mutable attributes (see getters and setters below)
        self._energy_bin_edges = None
        self._num_time_bins = None
        self._time_step_size = None
        self._batch_size = None
        self._labeler = None
        # Check data source
        self.validate(deep=deep_validation)
        # Immutable attributes
        run_ids = np.ravel(run_ids).astype(np.uint32)
        assert np.all(
            np.diff(run_ids) == 1
        ), "run_ids are not sorted and increase monotonically by 1"
        self._run_ids = run_ids
        assert isinstance(label_manager, LabelManager)
        self._label_manager = label_manager

    # --------------------------------------------------------------------------
    # Abstract methods
    # --------------------------------------------------------------------------

    @abc.abstractmethod
    def _validate(self, deep: bool = False) -> None:
        """Validate the data file/database structure.

        Parameters
        ----------
        deep : bool, optional
            Fully validate the data source (potentially slow), by default False

        Raises
        ------
        Exception
            When the validation fails.
        """

    @abc.abstractmethod
    def iter_spectra(self, *args, **kwargs) -> SpectrumData:
        """Iterate SpectrumData

        Yields
        ------
        SpectrumData

        Notes
        -----
        See ListmodeH5 for example implementation.
        """

    @abc.abstractmethod
    def iter_waterfalls(self, *args, **kwargs) -> WaterfallData:
        """Iterate WaterfallData

        Yields
        ------
        WaterfallData

        Notes
        -----
        See ListmodeH5 for example implementation.
        """

    @abc.abstractmethod
    def iter_batches(self, *args, **kwargs) -> BatchData:
        """Iterate BatchData

        Yields
        ------
        BatchData

        Notes
        -----
        See ListmodeH5 for example implementation.
        """

    @abc.abstractmethod
    def get_metadata(self, run_id: int = None, keys_only: bool = False):
        """Get metadata for the entire dataset or a particular run

        Parameters
        ----------
        run_id : int, optional
            Run id, by default None which corresponds to the global metadata.
        keys_only : bool, optional
            Only return the keys for the top level metadata, by default False

        Returns
        -------
        dict
        """

    @abc.abstractmethod
    def get_source_locations(self, run_id: int) -> SourceLocations:
        """Get SourceLocations for a particular run

        Parameters
        ----------
        run_id : int

        Returns
        -------
        SourceLocations
        """

    @abc.abstractmethod
    def set_labeler(self, arg: Union[str, Labeler], **kwargs) -> None:
        """Set Labeler

        Parameters
        ----------
        arg : Union[str, Labeler]
            Shortcut name for data set specific labeler or labeler object.

        Other Parameters
        ----------------
        kwargs : dict
            Other parameters to pass to the Labeler constructor if needed.
        """

    # --------------------------------------------------------------------------
    # Discretization Properties
    # --------------------------------------------------------------------------

    @property
    def energy_bin_edges(self):
        """Numpy array of float32 energy keV bin edges."""
        return self._energy_bin_edges

    @energy_bin_edges.setter
    def energy_bin_edges(self, edges):
        edges = np.ravel(edges).astype(np.float32)
        assert len(edges) > 1, edges
        assert np.all(np.diff(edges) > 0), edges
        self._energy_bin_edges = edges

    @property
    def energy_bin_centers(self):
        """Numpy array of float32 energy keV bin centers (calc'd from the edges)."""
        if self.energy_bin_edges is None:
            return None
        return self._energy_bin_edges[:-1] + np.diff(self._energy_bin_edges) / 2.0

    @property
    def num_energy_bins(self):
        if self._energy_bin_edges is None:
            return None
        else:
            return len(self.energy_bin_edges) - 1

    @property
    def time_step_size(self):
        return self._time_step_size

    @time_step_size.setter
    def time_step_size(self, step):
        step = float(step)
        assert step > 0
        self._time_step_size = step

    @property
    def num_time_bins(self):
        return self._num_time_bins

    @num_time_bins.setter
    def num_time_bins(self, num):
        num = int(num)
        assert num > 0
        self._num_time_bins = num

    @property
    def batch_size(self):
        return self._batch_size

    @batch_size.setter
    def batch_size(self, size):
        size = int(size)
        assert size > 0
        self._batch_size = size

    @property
    def discretization_summary(self):
        return {
            "num_energy_bins": len(self.energy_bin_edges) - 1,
            "energy_bin_edges (keV)": self.energy_bin_edges,
            "num_time_bins": self.num_time_bins,
            "time_step_size (ms)": self.time_step_size,
            "batch_size": self.batch_size,
        }

    # --------------------------------------------------------------------------
    # Run properties
    # --------------------------------------------------------------------------

    @property
    def run_ids(self):
        """Numpy array of sorted run_ids"""
        return self._run_ids

    @property
    def num_runs(self):
        """Number of runs."""
        return len(self._run_ids)

    # --------------------------------------------------------------------------
    # Source properties
    # --------------------------------------------------------------------------

    @property
    def label_manager(self):
        """Sources and labels."""
        return self._label_manager

    # --------------------------------------------------------------------------
    # Other properties
    # --------------------------------------------------------------------------

    @property
    def labeler(self):
        """Labeler to call when iterating data."""
        return self._labeler

    # --------------------------------------------------------------------------
    # Methods
    # --------------------------------------------------------------------------

    def validate(self, deep: bool = False) -> None:
        """Validate the data file/database structure.

        Parameters
        ----------
        deep : bool, optional
            Fully validate the data source (potentially slow), by default False

        Raises
        ------
        Exception
            When the validation fails.
        """
        self._validate(deep=deep)

    def iter_keras(self, *args, **kwargs):
        raise NotImplementedError()
