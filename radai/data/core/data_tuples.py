"""Spectrum, Waterfall and Batch (of spectra or waterfalls) data structures which
are passed from the iterator methods of a DataSet."""
import abc
import numpy as np
import pandas as pd
from tqdm.auto import tqdm
from .labels import _Labels, SpectrumLabels, WaterfallLabels, BatchLabels
from .tools import RadaiDataSetError
from ...tools import hist_outline


class _DataTuple(abc.ABC):

    __slots__ = "_data", "_time_bin_edges", "_energy_bin_edges", "_labels", "_metadata"

    def __init__(
        self,
        data: np.ndarray,
        time_bin_edges: np.ndarray,
        energy_bin_edges: np.ndarray,
        labels: _Labels = None,
        metadata: dict = None,
    ) -> None:
        self._data = np.asarray(data, dtype=np.uint32)
        self._time_bin_edges = np.asarray(time_bin_edges, dtype=np.uint32)
        self._energy_bin_edges = np.asarray(energy_bin_edges, dtype=np.float32)
        assert isinstance(labels, _Labels)
        self._labels = labels
        self._metadata = {} if metadata is None else dict(**metadata)

    @property
    def data(self) -> np.ndarray:
        """Numpy array of uint32 count data."""
        return self._data

    @property
    def time_bin_edges(self) -> np.ndarray:
        """Numpy array of uint32 milliseconds since the start of the run."""
        return self._time_bin_edges

    @property
    def energy_bin_edges(self) -> np.ndarray:
        """Numpy array of float32 energy keV bin edges."""
        return self._energy_bin_edges

    @property
    def labels(self):
        """Labels for the items."""
        return self._labels

    @property
    def metadata(self) -> dict:
        """Dictionary of user defined metadata."""
        return self._metadata

    @property
    def time_bin_centers(self) -> np.ndarray:
        return self.time_bin_edges[:-1] + np.diff(self.time_bin_edges) / 2.0

    @property
    def energy_bin_centers(self) -> np.ndarray:
        return self.energy_bin_edges[:-1] + np.diff(self.energy_bin_edges) / 2.0


class SpectrumData(_DataTuple):
    """Spectrum data

    Attributes
    ----------
    data: np.ndarray
        1D array of count data [E, ]
    run_id: int
        Run ID this data originated from
    time_bin_edges: np.ndarray
        1D array of milliseconds since the start of the run which denote the
        start and end of this spectrum.
    energy_bin_edges: np.ndarray
        1d array of energy (keV) for the first axis of the data
    time_idx: int, optional
        Integer time index for the spectrum in the run. By default 0.
    labels : SpectrumLabels, optional
        Labels for this spectrum.
    metadata: dict
        User-defined metadata
    """

    # NOTE this extends the __slots__ from _DataTuple
    __slots__ = "_run_id", "_time_idx"

    def __init__(
        self,
        data: np.ndarray,
        run_id: int,
        time_bin_edges: np.ndarray,
        energy_bin_edges: np.ndarray,
        time_idx: int = 0,
        labels: SpectrumLabels = None,
        metadata: dict = None,
    ) -> None:
        assert data.ndim == 1
        assert time_bin_edges.ndim == 1
        assert energy_bin_edges.ndim == 1
        assert time_bin_edges.shape == (2,)
        assert data.shape[0] == energy_bin_edges.shape[0] - 1
        super().__init__(
            data=data,
            time_bin_edges=time_bin_edges,
            energy_bin_edges=energy_bin_edges,
            labels=SpectrumLabels() if labels is None else labels,
            metadata=metadata,
        )
        assert isinstance(self.labels, SpectrumLabels)
        self._run_id = int(run_id)
        self._time_idx = int(time_idx)

    @property
    def run_id(self) -> int:
        """Integer run_id this data originated from."""
        return self._run_id

    @property
    def time_idx(self) -> int:
        """Integer time index for the spectrum in the run."""
        return self._time_idx

    @property
    def time(self):
        return np.mean(self.time_bin_edges)

    def plot(self, ax=None, **kwargs):
        import matplotlib.pyplot as plt

        ax = plt.gca() if ax is None else ax
        y, x = hist_outline((self.energy_bin_edges, self.data), endpoints=False)
        ax.plot(x, y, **kwargs)
        ax.set_xlabel("Energy (keV)")
        ax.set_ylabel("Counts")
        return ax


class WaterfallData(_DataTuple):
    """Waterfall/spectra timeseries data.

    Attributes
    ----------
    data: np.ndarray
        2D array of count data [T, E]
    run_id: int
        Run ID this data originated from
    time_bin_edges: np.ndarray
        1D array of miliiseconds since the start of the run denoting the edges
        for the first axis of the data
    energy_bin_edges: np.ndarray
        1d array of energy (keV) for the second axis of the data
    time_idx: int, optional
        Integer time index for the start of the waterfall in the run. By default 0.
    labels : WaterfallLabels, optional
        Labels for this waterfall (1D names/boolean from aggregation across all
        times and 2D scalars across [T, S])
    metadata: dict, optional
        User-defined metadata
    """

    # NOTE this extends the __slots__ from _DataTuple
    __slots__ = "_run_id", "_time_idx"

    def __init__(
        self,
        data: np.ndarray,
        run_id: int,
        time_bin_edges: np.ndarray,
        energy_bin_edges: np.ndarray,
        time_idx: int = 0,
        labels: WaterfallLabels = None,
        metadata: dict = None,
    ):
        super().__init__(
            data=data,
            time_bin_edges=time_bin_edges,
            energy_bin_edges=energy_bin_edges,
            labels=WaterfallLabels() if labels is None else labels,
            metadata=metadata,
        )
        assert self.data.ndim == 2
        assert self.time_bin_edges.ndim == 1
        assert self.energy_bin_edges.ndim == 1
        assert self.data.shape[0] == self.time_bin_edges.shape[0] - 1
        assert self.data.shape[1] == self.energy_bin_edges.shape[0] - 1
        assert isinstance(self.labels, WaterfallLabels)
        if self.labels.scalars is not None:
            assert (
                self.labels.scalars.shape[0] == self.data.shape[0]
            ), f"{self.labels.scalars.shape} vs {self.data.shape}"
        self._run_id = int(run_id)
        self._time_idx = int(time_idx)

    @property
    def run_id(self) -> int:
        """Integer run_id this data originated from."""
        return self._run_id

    @property
    def time_idx(self) -> int:
        """Integer time index for the start of the waterfall in the run."""
        return self._time_idx

    @property
    def num_time_bins(self):
        return self.data.shape[0]

    def iter_spectra(
        self,
        stride: int = 1,
        progress_bar: bool = False,
        progress_bar_kwargs: dict = None,
    ):
        """Iterate through the rows of this waterfall.

        Parameters
        ----------
        stride : int, optional
            Stride to step through the rows, by default 1
        progress_bar : bool, optional
            Display a progress bar, by default False
        progress_bar_kwargs : dict or None, optional

        Yields
        -------
        SpectrumData
        """
        progress_bar_kwargs = (
            dict() if progress_bar_kwargs is None else progress_bar_kwargs
        )
        progress_bar_kwargs.setdefault("desc", "Iterating time bins")
        progress_bar_kwargs["disable"] = not progress_bar
        for i in tqdm(range(self.num_time_bins), **progress_bar_kwargs):
            if i % stride > 0:
                continue
            tbe = self.time_bin_edges[i : i + 2]
            labels = SpectrumLabels(
                names=self.labels.names,
                # The boolean values in the waterfall are from aggregation and
                # thus don't apply to individual spectra.
                booleans=None,
                scalars=None if self.labels.scalars is None else self.labels.scalars[i],
            )
            yield SpectrumData(
                self.data[i],
                run_id=self.run_id,
                time_bin_edges=tbe,
                energy_bin_edges=self.energy_bin_edges,
                labels=labels,
                metadata=self.metadata,
            )

    def plot(
        self,
        ax=None,
        invert_time_axis=True,
        draw_colorbar=True,
        log_norm: bool = True,
        **kwargs,
    ):
        import matplotlib.pyplot as plt
        from matplotlib.colors import SymLogNorm

        y_bin_mesh, x_bin_mesh = np.meshgrid(
            self.time_bin_edges, self.energy_bin_edges, indexing="ij"
        )
        kwargs.setdefault("edgecolors", "none")
        if log_norm:
            kwargs["norm"] = SymLogNorm(linthresh=1, vmin=0, vmax=self.data.max())
        ax = plt.gca() if ax is None else ax
        im = ax.pcolormesh(x_bin_mesh, y_bin_mesh, self.data, **kwargs)
        if invert_time_axis:
            ax.invert_yaxis()
        ax.set_aspect("auto")
        if draw_colorbar:
            ax.figure.colorbar(im, label="counts")
        ax.set_xlabel("Energy (keV)")
        ax.set_ylabel("Time since start of run (ms)")
        return ax

    def to_dataframe(self):
        return pd.DataFrame(
            self.data,
            index=pd.Index(self.time_bin_centers, name="time"),
            columns=pd.Index(self.energy_bin_centers, name="energy"),
        )


class BatchData(_DataTuple):
    """Waterfall (T>1) or spectrum (T=1) batch.

    Attributes
    ----------
    data: np.ndarray
        3D array of count data [N, T, E]
    run_ids: np.ndarray[uint32]
        Run ID's this data originated from [N, ]
    time_bin_edges: np.ndarray
        2D array miliiseconds since the start of the run denoting the second
        axis of the data. The shape allows the user to specify the exact
        time edges for the batch. [N, T+1]
    energy_bin_edges: np.ndarray
        1D array of energy (keV) for the third axis of the data. [E + 1, ]
    time_idx: np.ndarray[uint32], optional
        1D array of time indices in the run for each item. [N, ]
        By default zeros of len(run_ids)
    labels : BatchLabels, optional
        Labels for each batch
    metadata: dict, optional
        User-defined metadata
    """

    # NOTE this extends the __slots__ from _DataTuple
    __slots__ = "_run_ids", "_time_idx"

    def __init__(
        self,
        data: np.ndarray,
        run_ids: np.ndarray,
        time_bin_edges: np.ndarray,
        energy_bin_edges: np.ndarray,
        time_idx: np.ndarray = None,
        labels: BatchLabels = None,
        metadata: dict = None,
    ):
        super().__init__(
            data=data,
            time_bin_edges=time_bin_edges,
            energy_bin_edges=energy_bin_edges,
            labels=BatchLabels() if labels is None else labels,
            metadata=metadata,
        )
        run_ids = np.asarray(run_ids, dtype=np.uint32)
        if time_idx is None:
            time_idx = np.zeros_like(run_ids)
        else:
            time_idx = np.asarray(time_idx, dtype=np.uint32)
        assert self.data.ndim == 3
        assert run_ids.ndim == 1
        assert self.time_bin_edges.ndim == 2
        assert self.energy_bin_edges.ndim == 1
        assert run_ids.shape[0] == self.time_bin_edges.shape[0]
        assert self.data.shape == (
            run_ids.shape[0],
            self.time_bin_edges.shape[1] - 1,
            self.energy_bin_edges.shape[0] - 1,
        )
        assert time_idx.shape == run_ids.shape
        assert isinstance(self.labels, BatchLabels)
        if self.labels.booleans is not None:
            assert self.labels.booleans.shape[0] == self.data.shape[0]
        if self.labels.scalars is not None:
            assert self.labels.scalars.shape[0:2] == self.data.shape[0:2]
        self._run_ids = run_ids
        self._time_idx = time_idx

    @classmethod
    def from_spectrum_data(cls, *spectrum_data):
        return cls(
            data=np.stack([s.data[np.newaxis, :] for s in spectrum_data], axis=0),
            run_ids=np.array([s.run_id for s in spectrum_data]),
            time_bin_edges=np.stack([s.time_bin_edges for s in spectrum_data], axis=0),
            energy_bin_edges=spectrum_data[0].energy_bin_edges,
            time_idx=np.array([s.time_idx for s in spectrum_data]),
            labels=BatchLabels.from_spectrum_labels(*[s.labels for s in spectrum_data]),
            metadata={"all": [s.metadata for s in spectrum_data]},
        )

    @property
    def run_ids(self) -> np.ndarray:
        """Numpy array of integer run_ids each item originated from."""
        return self._run_ids

    @property
    def time_idx(self) -> np.ndarray:
        """Array of integer time indices for the start time of each item in the
        corresponding run."""
        return self._time_idx

    def is_spectra_batch(self) -> bool:
        return self.data.shape[1] == 1

    def is_waterfall_batch(self) -> bool:
        return not self.is_spectra_batch()

    @property
    def spectra_2d(self) -> np.ndarray:
        if self.is_waterfall_batch():
            raise RadaiDataSetError(
                "Cannot get 2D spectra from batch of waterfall data"
            )
        return np.squeeze(self.data)
