from typing import Union, List
import numpy as np
from ..core.tools import RadaiDataSetError


# TODO generalize this at a later time. Unclear how to do this because this
#      class is so tightly connected to the data set (cache clearing, etc)
class ListmodeH5Augmentor(object):
    def __init__(self, data_set, seed: int = 42) -> None:
        """Augmentations for the ListmodeH5 dataset.

        Parameters
        ----------
        data_set : ListmodeH5
            Data set which is being augmented. This connection is required
            because of multiple values required from the data set and to
            automatically flush the dataset caches when the seed or augment
            parameters are changed.
        seed : int, optional
            RNG seed to add to the run ID for np.random.default_rng, by default 42
        """
        self._default_seed = int(seed)
        self._seed = self._default_seed
        self._kw = {}
        self._data_set = data_set

    def __str__(self) -> str:
        s = f"{self.__class__.__name__}("
        s += ", ".join(
            [f"default_seed={self._default_seed}", f"seed={self._seed}"]
            + [f"{m}(" + str(self._kw.get(m, "n/a")) + ")" for m in self.methods]
        )
        s += ")"
        return s

    def __repr__(self) -> str:
        return self.__str__()

    def _get_rng(self, run_id: int) -> np.random.Generator:
        """Get numpy random number generator for this run.

        Parameters
        ----------
        run_id : int
            Run ID

        Returns
        -------
        np.random.Generator
            RNG from np.random.default_rng(seed + run_id)
        """
        return np.random.default_rng(self._seed + int(run_id))

    @property
    def seed(self) -> int:
        """Seed used by the RNG. Change with set_seed or increment_seed!

        Returns
        -------
        int
        """
        return self._seed

    def set_seed(self, seed: int) -> None:
        """Set seed to be added to the run_id when creating the RNG.

        Parameters
        ----------
        seed : int
        """
        if self.seed != seed:
            self._data_set.clear_caches()
            self._seed = int(seed)

    def increment_seed(self) -> None:
        """Increment the RNG seed by the number of runs in the data set."""
        self.set_seed(self._seed + self._data_set.num_runs)

    @property
    def methods(self) -> tuple:
        """Available augmentation methods

        Returns
        -------
        tuple
            Augmentation method names. Currently:
                shift_energy
                shift_time
                reverse_time
                binomial_downsample
        """
        return "shift_energy", "shift_time", "reverse_time", "binomial_downsample"

    def will_augment(self) -> bool:
        """Does this augmentor have parameters configured to augment?

        Returns
        -------
        bool
        """
        return len(self._kw) > 0

    def augment(self, run_id: int, **arrays) -> Union[None, dict]:
        """Augment the first dimension of the arrays.

        Parameters
        ----------
        run_id : int
            Run ID

        Other Parameters
        ----------------
        arrays : np.ndarray, optional
            1D arrays to augment with special treatment based on their names. If
            no arrays are provided, None is returned.

        Returns
        -------
        dict or None
            If no arrays are passed return None else return a dictionary of
            augmented data of the same key ordering.

        Raises
        ------
        NotImplementedError
            If the shift_energy method is called (TODO).
        """
        if not len(arrays):
            return None
        if not self.will_augment():
            return arrays
        new = arrays
        length = len(next(iter(new.values())))
        if "shift_energy" in self._kw:
            raise NotImplementedError()
        if "shift_time" in self._kw:
            rng = self._get_rng(run_id)
            try:
                time = new["time"]
                # Constrain sampling window to the start/end times for this run
                low = np.clip(
                    time - self._kw["shift_time"]["s"] / 2.0, a_min=0.0, a_max=None
                )
                high = np.clip(
                    time + self._kw["shift_time"]["s"] / 2.0,
                    a_min=None,
                    a_max=self._data_set.get_end_time(run_id),
                )
                # Setting this to float32 causes aliasing at sub millisecond shifts
                new["time"] = rng.uniform(low, high)  # .astype(np.float32)
            except KeyError:
                pass
        if self.is_reversed(run_id):
            new = {k: a[::-1] for k, a in new.items()}
            # Reverse time values (ms) if needed
            try:
                new["time"] = self._data_set.get_end_time(run_id) - new["time"]
            except KeyError:
                pass
        if "binomial_downsample" in self._kw:
            rng = self._get_rng(run_id)
            # HACK Load id's if not provided. This is not the most elegant
            #      solution but the _get_listmode_raw method is cached so IO will
            #      be reduced when processing the same run_id through multiple
            #      calls of this augmentor.
            if "id" not in new:
                # Load ids for this run (cached)
                ids = self._data_set._get_listmode_raw(run_id, "id")
                # Drop disabled events if needed
                ids = ids[self._data_set._get_listmode_enabled_mask(run_id)]
            else:
                ids = new["id"]
            # Select random downsample probabilities (skip nan's)
            probs = {}
            for k, v in self._kw["binomial_downsample"].items():
                if np.all(np.isnan(v)):
                    continue
                elif isinstance(v, float):
                    probs[k] = v
                else:
                    probs[k] = rng.choice(v)
            # Source event locations and count
            se = ids > 0
            nse = se.sum()
            # Create downsampling mask
            mask = np.ones(len(ids), dtype=np.bool_)
            # Downsample background and source event independently
            # The output of binomial are integers (0 or 1) which are cast to
            # booleans in the mask.
            if "p_background" in probs:
                mask[~se] = rng.binomial(1, probs["p_background"], size=(length - nse))
            if "p_source" in probs:
                mask[se] = rng.binomial(1, probs["p_source"], size=nse)
            # Slice array(s)
            new = {k: a[mask] for k, a in new.items()}
        return new

    def clear(self):
        """Remove the augmentation parameters and reset the seed (clears data
        caches).
        """
        self._kw = {}
        self.set_seed(self._default_seed)

    def is_reversed(self, run_id: int) -> bool:
        """Is this run reversed?

        Parameters
        ----------
        run_id : int
            Run ID

        Returns
        -------
        bool
        """
        if "reverse_time" in self._kw:
            return self._get_rng(run_id).uniform() <= self._kw["reverse_time"]["p"]
        else:
            return False

    def shift_energy(self, *args, **kwargs) -> None:
        """Shift energy emulating gain variation in the channel to energy calibration.

        Raises
        ------
        NotImplementedError
        """
        self._data_set.clear_caches()
        raise NotImplementedError()

    def shift_time(self, s: float) -> None:
        """Shift time in a uniform window to simulate clock uncertainty.

        Parameters
        ----------
        s : float
            Window size in milliseconds (centered on the true time) to uniformly
            resample within.
        """
        self._data_set.clear_caches()
        self._kw["shift_time"] = {"s": float(s)}

    def reverse_time(self, p: float = 0.5) -> None:
        """Enable time reversal with probability of `p`.

        Parameters
        ----------
        p : float, optional
            Probability of reversing the direction of time. Range: [0, 1]. By
            default 0.5
        """
        self._data_set.clear_caches()
        self._kw["reverse_time"] = {"p": float(p)}

    def binomial_downsample(
        self,
        *args,
        p_background: Union[float, List[float]] = None,
        p_source: Union[float, List[float]] = None,
    ) -> None:
        """Enable binomial downsampling for background (id==0) and source events
        (id == 0) each with a unique survival probability or set of probabilities
        to select from. One or both of the probabilities must be provided.

        Parameters
        ----------
        p_background : float or list, optional
            Probability or probabilities of each background listmode event surviving.
            Range: [0, 1]. By default None (no background downsampling).
        p_source : float or list, optional
            Probability or probabilities of each source listmode event surviving.
            Range: [0, 1]. By default None (no source downsampling).

        Raises
        ------
        RadaiDataSetError
            If neither p_background or p_source provided.
        """
        if p_background is None and p_source is None:
            raise RadaiDataSetError(
                "One or both of `p_background` and `p_source` must be provided."
            )
        self._data_set.clear_caches()
        p_background = np.ravel(p_background).astype(float).tolist()
        if len(p_background) == 1:
            p_background = p_background[0]
        p_source = np.ravel(p_source).astype(float).tolist()
        if len(p_source) == 1:
            p_source = p_source[0]
        self._kw["binomial_downsample"] = {
            "p_background": p_background,
            "p_source": p_source,
        }
