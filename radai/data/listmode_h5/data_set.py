from typing import Union, Callable
from pathlib import Path
from functools import lru_cache
from deprecated import deprecated
import numpy as np
import pandas as pd
import h5py
from tqdm.auto import tqdm
from ..core import (
    DEFAULT_ENERGY_BIN_EDGES,
    BACKGROUND_NAMES,
    RadaiDataSetError,
    DataSet,
    SpectrumData,
    WaterfallData,
    BatchData,
    SourceLocations,
    SpectrumLabels,
    WaterfallLabels,
    BatchLabels,
    LabelManager,
    Labeler,
)
from .augmentor import ListmodeH5Augmentor
from .snr_labeler import ListmodeH5SNRLabeler
from .schema import SCHEMA

try:
    import fast_histogram
except ImportError:
    fast_histogram = None

# TODO: add get data for particular source_id and add to waterfall method


def _make_indices_mask(multiindex, values):
    try:
        # TODO: test this!
        return multiindex.isin([(int(x[0]), int(x[1])) for x in values])
    except Exception:
        return multiindex.isin([int(x) for x in values], level=0)


class ListmodeH5(DataSet):
    """Listmode data set stored in a single h5 file (see Notes for schema).

    Attributes
    ----------
    TODO

    Methods
    -------
    TODO

    Notes
    -----
    TODO include schema
    """

    def __init__(
        self,
        path: Union[str, Path],
        time_step_size: float = 100,
        energy_bin_edges: np.ndarray = DEFAULT_ENERGY_BIN_EDGES,
        num_time_bins: int = 100,
        batch_size: int = 32,
        deep_validation: bool = False,
        augmentor_seed: int = 42,
        infer_isotope_names: bool = True,
    ) -> None:
        """Open a listmode data set with an initialized configuration

        Parameters
        ----------
        path : Union[str, Path]
            File path to hdf5 file (will be opened for reading and validated).
        time_step_size : float, optional
            Time discretization in milliseconds, by default 1000
        energy_bin_edges : np.ndarray, optional
            Energy discretization in keV, by default 0 to 3000 with 128 bins
            spaced proportional to E**2
        num_time_bins : int, optional
            Length of spectra waterfalls with integration time of the step size,
            by default 100
        batch_size : int, optional
            Length of the waterfall batches, by default 32
        deep_validation : bool, optional
            Validate all runs instead of just the first. These doesn't need to
            be performed every time the dataset is used and instead exists for
            initial validation and testing, by default False
        augmentor_seed : int, optional
            Random number generator seed to initialize ListmodeH5Augmentor
        infer_isotope_names : bool, optional
            Infer the isotope name from the source name. See LabelManager.append
            for more details. By default True
        """
        # Open file
        self.path = Path(path)
        self.h5file = h5py.File(self.path, "r")
        # Get run ids
        run_ids = np.asarray([int(r.lstrip("run")) for r in self.h5file["runs"]])
        run_ids.sort()
        # Call the super to finish constructing the object
        super().__init__(
            run_ids,
            label_manager=LabelManager(
                *zip(
                    self.h5file.attrs["source_ids"], self.h5file.attrs["source_names"]
                ),
                infer_isotope=infer_isotope_names,
                clear_caches=self.clear_caches,
            ),
            deep_validation=deep_validation,
        )
        # Discretization parameters
        self.energy_bin_edges = energy_bin_edges
        self.time_step_size = time_step_size
        self.num_time_bins = num_time_bins
        self.batch_size = batch_size
        # Empty augmentor
        self._augmentor = ListmodeH5Augmentor(data_set=self, seed=augmentor_seed)

    def __del__(self) -> None:
        try:
            self.h5file.close()
        except ImportError:
            print("Couldn't close h5file.")

    def _validate(self, deep: bool = False) -> None:
        return SCHEMA.validate(self.h5file, deep=deep)

    def clear_caches(self):
        self.get_time_bins.cache_clear()
        self.get_source_id_map.cache_clear()
        self.get_background_id_map.cache_clear()
        self.get_shielding_id_map.cache_clear()
        self._get_listmode_raw.cache_clear()
        self._get_listmode_enabled_mask.cache_clear()
        self.get_entire_waterfall.cache_clear()
        self.get_source_locations.cache_clear()
        self.get_label_count_per_run.cache_clear()
        self.get_snr_for_run.cache_clear()
        self.get_snr_for_runs.cache_clear()
        try:
            self.labeler.clear_caches()
        except AttributeError:
            pass

    # --------------------------------------------------------------------------
    # Listmode Augmentor
    # --------------------------------------------------------------------------

    @property
    def augmentor(self):
        """Augmentor to optionally modify listmode data before further processing."""
        return self._augmentor

    def set_labeler(
        self, arg: Union[str, ListmodeH5SNRLabeler] = "snr", **kwargs
    ) -> None:
        """Set Labeler

        Parameters
        ----------
        arg : "snr" or ListmodeH5SNRLabeler, optional
            Shortcut name or object for data set specific labeler. Currently
            only the SNR labeler is supported.

        Other Parameters
        ----------------
        kwargs : dict
            Other parameters to pass to the Labeler constructor if needed.
        """
        self.clear_caches()
        if arg == "snr":
            self._labeler = ListmodeH5SNRLabeler(self, **kwargs)
        elif isinstance(arg, Labeler):
            self._labeler = arg
            self._labeler._data_set = self
        else:
            raise TypeError(f"Invalid labeler: {arg}")

    # --------------------------------------------------------------------------
    # Discretization for spectra and waterfalls
    # --------------------------------------------------------------------------

    @DataSet.energy_bin_edges.setter
    def energy_bin_edges(self, step):
        super(ListmodeH5, type(self)).energy_bin_edges.fset(self, step)
        self.clear_caches()

    @DataSet.time_step_size.setter
    def time_step_size(self, step):
        super(ListmodeH5, type(self)).time_step_size.fset(self, step)
        self.clear_caches()

    @DataSet.num_time_bins.setter
    def num_time_bins(self, num):
        super(ListmodeH5, type(self)).num_time_bins.fset(self, num)
        self.clear_caches()

    def get_end_time(self, run_id: int) -> float:
        """Get end time of run in milliseconds

        Parameters
        ----------
        run_id : int
            Run ID

        Returns
        -------
        float
        """
        m = self.get_metadata(run_id)
        return (m["end_timestamp"] - m["start_timestamp"]) * 1e3

    @lru_cache(maxsize=100)
    def get_time_bins(self, run_id: int, alignment="edges"):
        """Get the time bin structure for a particular run.

        Parameters
        ----------
        run_id : int
            Run ID
        alignment : {'edges', 'centers'}, optional
            Get the edges or the centers for the time bins.

        Returns
        -------
        array
            1D array of time bin edges (uint32) or centers (float32) in
            milliseconds.

        Notes
        -----
        This will trim the last events to assure a live_time of the `time_step_size`.
        The result is cached for up to 100 unique arg combinations. This cache
        is flushed if the time_step_size is changed.
        """
        if alignment not in ("edges", "centers"):
            raise ValueError(f"Unknown time bin alignment: {alignment}")
        edges = np.arange(
            0, self.get_end_time(run_id), self.time_step_size, dtype=np.uint32
        )
        if alignment == "edges":
            return edges
        else:
            edges = edges.astype(np.float32)
            return (edges[:-1] + edges[1:]) / 2.0

    # TODO: check if the indices passed to this method are NOT in the dataset
    # TODO: Add kwargs to sort runs and shuffle time
    def get_indices(
        self,
        for_waterfalls: bool = False,
        stride: int = 1,
        include_indices: np.ndarray = None,
        exclude_indices: np.ndarray = None,
        sort: bool = True,
    ) -> np.ndarray:
        """Get a 2D array of the spectra or waterfall (run_id, time_idx).

        Parameters
        ----------
        for_waterfalls : bool, optional
            Get the start time indices for waterfalls instead of the time index
            of all the spectra, by default False
        stride : int, optional
            Step size to skip indices, by default 1
        include_indices : np.ndarray or array-like, optional
            Only consider these indices. If 1D only these run_ids are included.
            If 2D only the run/time indices are include. By default None (include
            all indices.)
        exclude_indices : np.ndarray or array-like, optional
            Don't consider these indices (still used with include_indices). If
            1D these run_ids are excluded. If 2D the run/time indices are
            excluded. By default None (don't exclude anything).
        sort : bool, optional
            Sort the indices (run, time) before any additional processing (i.e.
            stride), by default True

        Returns
        -------
        np.ndarray
            2D array of run/time indices (N, 2)

        Raises
        ------
        RadaiDataSetError
            If the include or exclude indices are invalid.
        """
        run_time_tuples = []
        for run_id in self.iter_run_ids(progress_bar=False, run_ids=None):
            total_bins = len(self.get_time_bins(run_id, alignment="centers"))
            for i in range(total_bins):
                if for_waterfalls and i >= (total_bins - self.num_time_bins):
                    break
                run_time_tuples.append((run_id, i))
        idx = pd.MultiIndex.from_tuples(run_time_tuples, names=["run_id", "time_idx"])
        if sort:
            # Sorting run/time indices by run_id (sort=True)
            idx = idx.sortlevel(level="run_id", sort_remaining=False)[0]
        if include_indices is None:
            mask = np.ones(len(run_time_tuples), dtype=np.bool_)
        else:
            try:
                mask = _make_indices_mask(idx, include_indices)
            except Exception:
                raise RadaiDataSetError(f"Invalid include_indices: {include_indices}")
        if exclude_indices is not None:
            exclude_indices = np.array(exclude_indices).astype(int)
            try:
                mask &= ~_make_indices_mask(idx, include_indices)
            except Exception:
                raise RadaiDataSetError(f"Invalid exclude_indices: {exclude_indices}")
        arr = idx.to_numpy()
        arr = arr[mask][::stride]
        return arr

    # --------------------------------------------------------------------------
    # H5 convenience methods
    # --------------------------------------------------------------------------

    def get_metadata(
        self, run_id: int = None, keys_only: bool = False
    ) -> Union[dict, list]:
        """Get metadata for the entire dataset or a particular run

        Parameters
        ----------
        run_id : int, optional
            Run id, by default None which corresponds to the global metadata.
        keys_only : bool, optional
            Only return the keys for the top level metadata, by default False

        Returns
        -------
        dict or list
        """
        if run_id is None:
            data = self.h5file.attrs
        else:
            data = self.get_run_group(run_id).attrs
        if keys_only:
            return list(data.keys())
        else:
            return dict(**data)

    def get_run_group(self, run_id: int):
        """Get h5py group for a run.

        Parameters
        ----------
        run_id : int

        Returns
        -------
        h5py.Group
            Run group in the h5file.
        """
        return self.h5file["runs"][f"run{run_id}"]

    @lru_cache(maxsize=1)
    def get_source_id_map(self) -> dict:
        return {
            i: n
            for i, n in zip(
                self.h5file.attrs["source_ids"], self.h5file.attrs["source_names"]
            )
        }

    @lru_cache(maxsize=1)
    def get_background_id_map(self) -> dict:
        return {
            i: n
            for i, n in zip(
                self.h5file.attrs["background_ids"],
                self.h5file.attrs["background_names"],
            )
        }

    @lru_cache(maxsize=1)
    def get_shielding_id_map(self) -> dict:
        return {
            i: n
            for i, n in zip(
                self.h5file.attrs["source_shielding_ids"],
                self.h5file.attrs["source_shielding_names"],
            )
        }

    # --------------------------------------------------------------------------
    # Rad data loaders
    # --------------------------------------------------------------------------

    @lru_cache(maxsize=20)
    def _get_listmode_raw(self, run_id: int, column: str) -> np.ndarray:
        run = self.get_run_group(run_id)
        try:
            return run["listmode"][column][:]
        except KeyError:
            raise RadaiDataSetError(
                f"ListmodeH5 run {run_id} does not contain listmode.{column}"
            )

    @lru_cache(maxsize=10)
    def _get_listmode_enabled_mask(self, run_id: int) -> np.ndarray:
        ids = self._get_listmode_raw(run_id, "id")
        if self.label_manager.num_disabled == 0:
            return np.ones_like(ids, dtype=np.bool_)
        enabled = self.label_manager.get_data(only_enabled=True, copy=False)
        return np.isin(ids, enabled.index.values)

    # TODO add time and energy cuts as options to load less data from the file
    #      if needed when loading a subset of data per a run!
    def get_listmode(
        self, run_id: int, column: str, only_enabled: bool = True
    ) -> np.ndarray:
        """Get a 1D array of a listmode data column

        Parameters
        ----------
        run_id : int
            Run ID to consider
        column : {"id", "source_id", "background_id", "dt", "energy", "time"}
            Name of data column:
                * id or source_id: source id (see enum in root attrs)
                * background_id: background id (see enum in root attrs)
                * label: label from label_manager.id2label(ids, as_string=False)
                * dt: µs between events
                * energy: keV of interactions
                * time: time since start in milliseconds of interactions
        only_enabled : bool, optional
            Include only the events from enabled source(s) (see the label_manager
            for more details). Note that setting this to False will raise an
            error for the `label` column if any sources are disabled because the
            labels are not defined for disabled sources. By default True.

        Returns
        -------
        np.ndarray
            Array of listmode interaction data.

        Raises
        ------
        RadaiDataSetError
            If id/source_id/background_id are requested but not present.
            If column is unknown.

        Notes
        -----
        If LabelManager.tier > 0 then the id's in the listmode will be converted
        to the label indices for the corresponding tier, i.e.:
            self.label_manager.tier == 0
                returns the id in the h5 array
            self.label_manager.tier in (1, 2)
                returns the ids from tier 0 mapped into the corresponding label
                indices using:
                    self.label_manager.id2label(ids, as_strings=False)
        """
        # Initial checks and alternate columns to query (processed below)
        query_column = str(column)
        if column == "source_id":
            column = "id"
            query_column = "id"
        elif column == "dt":
            if self.augmentor.will_augment():
                raise NotImplementedError(
                    "Cannot get listmode `dt` when augmentor is active!"
                )
        elif column == "time":
            query_column = "dt"
        elif column == "label":
            query_column = "id"
        # Get the raw listmode data (cached above)
        arr = self._get_listmode_raw(run_id, query_column)
        # Convert time deltas in µs to time in ms
        if column == "time":
            arr = np.cumsum(arr, dtype=np.uint32).astype(np.float64) / 1e3
        # Remove disabled events
        if only_enabled:
            arr = arr[self._get_listmode_enabled_mask(run_id)]
        # Convert source ids to label
        if column == "label":
            arr = self.label_manager.id2label(arr, as_strings=False)
        # Return augmented data
        return self.augmentor.augment(run_id, **{column: arr})[column]

    def get_listmode_df(
        self, run_id: int, columns: list = None, **kwargs
    ) -> pd.DataFrame:
        """Get a DataFrame of listmode data for a run.

        Parameters
        ----------
        run_id : int
            Run id to consider
        columns : list, optional
            Load these columns into the dataframe, by default:
                ["energy", "time", "source_id", "background_id"]

        Other Parameters
        ----------------
        kwargs : dict
            Additional keyword arguments for `get_listmode`.

        Returns
        -------
        pd.DataFrame
            Table of listmode interactions with columns of energy, time and
            optionally source_id and background_id.
        """
        if columns is None:
            columns = ["energy", "time", "source_id", "background_id"]
        data = {}
        for column in columns:
            try:
                data[column] = self.get_listmode(run_id, column, **kwargs)
            except KeyError:
                print(f"WARNING: Cannot get listmode data for: {column}")
        return pd.DataFrame.from_dict(data)

    @lru_cache(maxsize=100)
    def get_entire_waterfall(
        self, run_id: int, label: Union[int, str] = None
    ) -> WaterfallData:
        """Get entire waterfall for a particular run.

        Parameters
        ----------
        run_id : int
        label : int or str
            Name or index of a source label for which to slice the listmode
            events before computing the waterfall.

        Returns
        -------
        WaterfallData
        """
        time = self.get_listmode(run_id, column="time")
        energy = self.get_listmode(run_id, column="energy")
        time_bin_edges = self.get_time_bins(run_id, alignment="edges")
        metadata = {"label": "all"}
        # Optionally cut on label
        if label is not None:
            all_labels = self.label_manager.get_labels_list()
            if isinstance(label, str):
                label_idx = all_labels.index(label)
            else:
                label_idx = int(label)
            mask = self.get_listmode(run_id, column="label") == label_idx
            time, energy = time[mask], energy[mask]
            metadata["label"] = all_labels[label_idx]
        hist = np.histogram2d(time, energy, [time_bin_edges, self.energy_bin_edges])
        return WaterfallData(
            hist[0],
            run_id,
            time_bin_edges,
            self.energy_bin_edges,
            metadata=metadata,
        )

    def get_sum_spectrum_per_label(
        self, run_ids: list = None, progress_bar: bool = False
    ) -> pd.DataFrame:
        """Extract a spectrum for each label from all or a subset of runs.

        Parameters
        ----------
        run_ids : list, optional
            A list or array of the run_ids to consider (others will be skipped.)
            Run ID's in this iterable which are not in the dataset will be
            silently ignored.
            By default None (consider all runs.)
        progress_bar : bool, optional
            Display a tqdm progress bar, by default False

        Returns
        -------
        pd.DataFrame
            Sum spectra [label, energy] from all or a subset of runs.
        """
        spectra = pd.DataFrame(
            np.zeros(
                (self.label_manager.num_labels, self.num_energy_bins), dtype=np.uint64
            ),
            index=pd.Index(self.label_manager.get_labels_list(), name="labels"),
            columns=pd.Index(self.energy_bin_centers, name="energy"),
        )
        for run_id in self.iter_run_ids(
            run_ids=run_ids, progress_bar=progress_bar, desc="Calc spectra per run"
        ):
            sl = self.get_source_locations(run_id)
            for label in sl.get("label", unique=True):
                spectra.loc[label, :] += self.get_entire_waterfall(
                    run_id=run_id, label=label
                ).data.sum(axis=0)
        return spectra

    # --------------------------------------------------------------------------
    # Source and label information
    # --------------------------------------------------------------------------

    @lru_cache(maxsize=1000)
    def get_source_locations(self, run_id: int) -> SourceLocations:
        """Get SourceLocations for a particular run sorted by time without
        background labels at time_idx=0 as in standard in the simulated h5 files.
        This will only return the sources which are currently enabled in the
        LabelManager since the other sources will not have a label value.

        Parameters
        ----------
        run_id : int

        Returns
        -------
        SourceLocations
        """
        run = self.get_run_group(run_id)
        # Get enabled sources
        ids = run["sources"]["id"][:]
        enabled_ids = self.label_manager.get_data(
            only_enabled=True, copy=False
        ).index.values
        msk = np.isin(ids, enabled_ids)
        # NOTE these warnings were disabled by @jccurtis because they polluted
        #      notebook outputs and did not provide actionable information. In
        #      the future these could be logged to DEBUG.
        # if msk.sum() < len(msk):
        #     warn(
        #         "Not including the following disabled sources in the source "
        #         f"locations for run {run_id}: {np.unique(ids[~msk])}",
        #         RadaiDataSetWarning,
        #     )
        # Make arrays of each field with masking for the enabled sources
        arrays = dict(
            names=[self.label_manager.id2name(i) for i in ids[msk]],
            labels=self.label_manager.id2label(ids[msk], as_strings=True),
            times=run["sources"]["time"][msk],
            distances=run["sources"]["distance"][msk],
        )
        try:
            arrays["locations"] = run["sources"]["location_id"][msk]
        except KeyError:
            pass
        try:
            arrays["shieldings"] = [
                self.get_shielding_id_map()[i]
                for i in run["sources"]["shielding_id"][msk]
            ]
        except KeyError:
            pass
        try:
            arrays["standoffs"] = run["sources"]["standoff"][msk]
        except KeyError:
            pass
        try:
            arrays["snrs_peak"] = run["sources"]["snr"]["peak"][msk]
        except KeyError:
            pass
        try:
            arrays["snrs_integral"] = run["sources"]["snr"]["integral"][msk]
        except KeyError:
            pass
        # Flip the direction if needed
        if self.augmentor.is_reversed(run_id):
            arrays["times"] = self.get_end_time(run_id) - arrays["times"]
        # Create source locations with or without background
        src_locs = SourceLocations(**arrays)
        # Remove background if present and sort
        src_locs.drop_background()
        src_locs.sort_by_time()
        # All done!
        return src_locs

    def get_source_locations_dataframe(self) -> pd.DataFrame:
        # Make a copy of each source locations dataframe indexed by time
        dfs = [
            self.get_source_locations(i).data.set_index("time")
            for i in self.iter_run_ids(progress_bar=False)
        ]
        df = pd.concat(dfs, keys=self.run_ids)
        df.index.rename(["run_id", "time"], inplace=True)
        return df

    @lru_cache(maxsize=1)
    def get_label_count_per_run(self) -> pd.DataFrame:
        """Get dataframe (run x label) with the count of each label per run.

        Returns
        -------
        pd.DataFrame
            2D array of sources in each run with shape: [run, source]

        Notes
        -----
        Background sources are only counted if a run contains no other sources.
        This allows filtering of background runs for some cases. The array is
        cached after the first call.
        """
        labels = self.label_manager.get_labels_list()
        counts = pd.DataFrame(
            np.zeros((self.num_runs, len(labels)), dtype=int),
            columns=pd.Index(labels, name="label"),
            index=pd.Index(self.run_ids, name="run_id"),
        )
        for run_id in counts.index:
            sl = self.get_source_locations(run_id)
            # Background only
            if len(sl) == 0:
                counts.iloc[run_id, 0] += 1
            # Contains source(s)
            else:
                for label in sl.get("label"):
                    counts.loc[run_id, label] += 1
        return counts

    def plot_num_label_per_run_heatmap(self, ax=None) -> None:
        import matplotlib.pyplot as plt
        from matplotlib.colors import ListedColormap
        import seaborn as sns

        labels = self.label_manager.get_labels_list()
        df = pd.DataFrame(
            self.get_label_count_per_run(),
            index=pd.Index(self.run_ids, name="run_id"),
            columns=pd.Index(labels),
        )
        if ax is None:
            plt.figure(figsize=(20, 20))
            ax = plt.gca()
        n_colors = df.values.max() + 1
        sns.heatmap(
            df,
            ax=ax,
            annot=True,
            linecolor="white",
            linewidths=0.1,
            cmap=ListedColormap(sns.color_palette("viridis", n_colors=n_colors)),
            cbar=False,
        )
        cbar = plt.colorbar(ax.collections[0])
        tick_locs = (np.arange(n_colors) + 0.5) * (n_colors - 1) / n_colors
        cbar.set_ticks(tick_locs)
        cbar.set_ticklabels(np.arange(n_colors))
        ax.tick_params(right=True, top=True, labelright=True, labeltop=True, rotation=0)
        plt.xticks(rotation="vertical")
        plt.title(self.h5file.filename)

    # --------------------------------------------------------------------------
    # Iterators
    # --------------------------------------------------------------------------

    def iter_run_ids(
        self, progress_bar: bool = False, run_ids: list = None, desc: str = None
    ) -> int:
        """Iterate through the sorted run ID's

        Parameters
        ----------
        progress_bar : bool, optional
            Display a tqdm progress bar, by default False
        run_ids : list, optional
            A list or array of the run_ids to consider (others will be skipped.)
            Run ID's in this iterable which are not in the dataset will be
            silently ignored.
            By default None (consider all runs.)
        desc : str, optional
            Description for the progress bar, by default "Iterating runs"

        Yields
        -------
        int
        """
        if run_ids is None:
            run_ids = self.run_ids
        else:
            run_ids = self.run_ids[np.isin(self.run_ids, run_ids)]
        for run_id in tqdm(
            run_ids,
            desc="Iterating runs" if desc is None else str(desc),
            disable=not progress_bar,
        ):
            yield run_id

    def iter_spectra(
        self,
        progress_bar: bool = False,
        indices: np.ndarray = None,
        label: Union[int, str] = None,
    ) -> SpectrumData:
        """Iterate through spectra with optional selection by indices

        Parameters
        ----------
        progress_bar : bool, optional
            Display a progress bar, by default False
        indices : np.ndarray, optional
            A 1D array of the run_ids or a 2D array of the (run_id, time_idx) to
            consider (others will be skipped.) These will be sorted before iteration
            to reduce data IO by grouping spectrum generation by run. By default
            None (all indices.)
        label : int or str
            Name or index of a source label for which to slice the listmode
            events before computing the spectra. By default, include all labels.

        Yields
        -------
        SpectrumData

        Notes
        -----
        The discretization is set with the `energy_bin_edges` and `time_step_size`
        attributes.
        """
        yielded = False
        idx = self.get_indices(include_indices=indices)
        for run_id, time_idx in tqdm(
            idx, disable=not progress_bar, desc="Iterating spectra"
        ):
            # This is cached so subsequent calls are fast
            wf = self.get_entire_waterfall(run_id, label=label)
            # Optionally create labels
            if self.labeler is None:
                labels = SpectrumLabels()
            else:
                labels = self.labeler.make_spectrum_labels((run_id, time_idx))
            # Data for this spectrum
            yielded = True
            yield SpectrumData(
                wf.data[time_idx],
                run_id=run_id,
                time_bin_edges=wf.time_bin_edges[time_idx : time_idx + 2],
                energy_bin_edges=wf.energy_bin_edges,
                time_idx=time_idx,
                labels=labels,
                metadata=wf.metadata,
            )
        if not yielded:
            print(
                "ERROR: No spectrum data generated. "
                "Check other iter methods and time bins"
            )

    def iter_waterfalls(
        self,
        progress_bar: bool = False,
        indices: np.ndarray = None,
        label: Union[int, str] = None,
    ) -> WaterfallData:
        """Iterate through waterfalls with optional selection by indices

        Parameters
        ----------
        progress_bar : bool, optional
            Display a progress bar, by default False
        indices : np.ndarray, optional
            A 1D array of the run_ids or a 2D array of the (run_id, start_time_idx)
            to consider (others will be skipped.) These will be sorted before
            iteration to reduce data IO by grouping waterfall generation by run.
            By default None (all indices.)
        label : int or str
            Name or index of a source label for which to slice the listmode
            events before computing the waterfalls. By default, include all
            labels.

        Yields
        ------
        WaterfallData

        Notes
        -----
        The discretization is set with the `energy_bin_edges`, `time_step_size`
        and `num_time_bins` attributes.
        """
        idx = self.get_indices(for_waterfalls=True, include_indices=indices)
        for run_id, time_idx in tqdm(
            idx, disable=not progress_bar, desc="Iterating waterfalls"
        ):
            # This is cached so subsequent calls are fast
            wf = self.get_entire_waterfall(run_id, label=label)
            # Optionally create labels
            if self.labeler is None:
                labels = WaterfallLabels()
            else:
                labels = self.labeler.make_waterfall_labels((run_id, time_idx))
            # Data for this waterfall
            yield WaterfallData(
                wf.data[time_idx : time_idx + self.num_time_bins],
                run_id=run_id,
                time_bin_edges=wf.time_bin_edges[
                    time_idx : time_idx + self.num_time_bins + 1
                ],
                energy_bin_edges=wf.energy_bin_edges,
                time_idx=time_idx,
                labels=labels,
                metadata=wf.metadata,
            )

    def iter_batches(
        self,
        progress_bar: bool = False,
        indices: np.ndarray = None,
        label: Union[int, str] = None,
    ) -> BatchData:
        """Iterate through batches of waterfalls

        Parameters
        ----------
        progress_bar : bool, optional
            Display a progress bar, by default False
        indices : np.ndarray, optional
            A 1D array of the run_ids or a 2D array of the (run_id, start_time_idx)
            to consider (others will be skipped.) These will be sorted before
            iteration to reduce data IO by grouping waterfall generation by run.
            By default None (all indices.)
        label : int or str
            Name or index of a source label for which to slice the listmode
            events before computing the waterfalls. By default, include all
            labels.

        Yields
        ------
        BatchData

        Notes
        -----
        The discretization is set with the `energy_bin_edges`, `time_step_size`,
        `num_time_bins` and `batch_size` attributes.
        """

        idx = self.get_indices(for_waterfalls=True, include_indices=indices)
        if len(idx) % self.batch_size > 0:
            print(f"WARNING: {len(idx) % self.batch_size} waterfalls will be dropped!")
        wfs = []
        for batch_id in tqdm(
            range(len(idx) // self.batch_size),
            disable=not progress_bar,
            desc="Iterating batches",
        ):
            batch_indices = idx[
                batch_id * self.batch_size : (batch_id + 1) * self.batch_size
            ]
            for wf in self.iter_waterfalls(
                progress_bar=False, indices=batch_indices, label=label
            ):
                wfs.append(wf)
            # Optionally create labels
            if self.labeler is None:
                labels = BatchLabels()
            else:
                labels = self.labeler.make_batch_labels(batch_indices)
            # Data for this batch
            batch_data = BatchData(
                data=np.stack([x.data for x in wfs], axis=0),
                run_ids=[x.run_id for x in wfs],
                time_bin_edges=np.stack([x.time_bin_edges for x in wfs], axis=0),
                energy_bin_edges=self.energy_bin_edges,
                time_idx=[x.time_idx for x in wfs],
                labels=labels,
                metadata=wf.metadata,
            )
            # Clear buffer
            wfs = []
            yield batch_data

    # --------------------------------------------------------------------------
    # Calculate SNR
    # --------------------------------------------------------------------------

    @lru_cache(maxsize=100)
    def get_snr_for_run(
        self,
        run_id: int,
        method="s/sqrt(s+b)",
        energy_min=None,
        energy_max=None,
        include_all_labels: bool = False,
        index_by_time: bool = True,
        aggregate_for_waterfalls: bool = False,
        aggregation_method: str = "max",
    ) -> pd.DataFrame:
        """Calculate SNR per label (including background) vs time for a run.

        The background column is defined to be equal to 0. It is included for
        unambiguous labeling downstream.

        Parameters
        ----------
        run_id : int
            Run ID to consider
        method : {"s/sqrt(b)", "s/sqrt(s+b)", "s/b", "s/(s+b)", "s", "b",
                  "currie"}, optional
            Method to calculate SNR:
                * s/sqrt(b): from Aucott 2014
                * s/sqrt(s+b): from Nicholson 2020 (default)
                * s/b: fraction of source relative to background
                * s/(s+b): fraction of source of total spectrum
                * s: total source counts (included for convenience)
                * s+b: total counts (included for convenience)
                * currie: Not Implemented
        energy_min : float, optional
            Minimum energy considered, by default None (no bound).
        energy_max : float, optional
            Maximum energy considered, by default None (no bound).
        include_all_labels : bool, optional
            Include all labels in the output dataframe columns even if not
            present in the run (including background which is defined to be 0
            before renaming using the LabelManager).
        index_by_time : bool, optional
            Use the center of each time step as the dataframe index, otherwise
            use the time index in the run.
        aggregate_for_waterfalls : bool, optional
            Group SNR values over time (window size determined by num_time_bins)
            using the aggregation_method. By default False
        aggregation_method : str, optional
            Method for aggregating SNR per source across all the time steps in
            each waterfall. This has no effect if aggregate_for_waterfalls is
            False, by default "max"

        Returns
        -------
        pd.DataFrame
            SNR values (float32) for each source label indexed by the time center
            or index for each time step (depending on the index_by_time option).
            The time index represents the time center of a spectrum or the start
            time of a waterfall (if aggregate_for_waterfalls is True).

        Notes
        -----
        The time_step_size is used to determine integration time. The result is
        cached for up to 100 unique arg combinations. The cache is cleared if
        time_step_size is changed.

        Raises
        ------
        RadaiDataSetError
            If SNR method is unknown.
            If source id per listmode interaction is not present in the h5 file.
            If the aggregation method is not known.

        References
        ----------
        .. Aucott, et. al. (2014). Effects of Background on Gamma-Ray
           Detection for Mobile Spectroscopy and Imaging Systems.
           https://doi.org/10.1109/TNS.2014.2306998
        .. Nicholson, et. al. (2020). Generation of Synthetic Data for a
           Radiation Detection Algorithm Competition.
           https://doi.org/10.1109/TNS.2020.3001754.
        """
        if method not in (
            "s/sqrt(b)",
            "s/sqrt(s+b)",
            "s/b",
            "s/(s+b)",
            "s",
            "s+b",
            "currie",
        ):
            raise RadaiDataSetError(f"Unknown SNR method: {method}")

        # ----------------------------------------------------------------------
        # Histogram time vs label
        # ----------------------------------------------------------------------

        # Get all listmode columns needed (time in ms, label as integers)
        time = self.get_listmode(run_id, "time")
        try:
            label = self.get_listmode(run_id, "label")
        except Exception:
            raise RadaiDataSetError(
                f"Cannot compute SNR without listmode label for run {run_id}. "
                "Check the source_id array in the H5 group for this run."
            )
        # TODO: move this energy cut to get_listmode to load with h5py query
        # Optional energy cut (don't load energy otherwise)
        if energy_min is not None or energy_max is not None:
            energy = self.get_listmode(run_id, "energy")
            energy_mask = np.ones_like(energy, dtype=np.bool_)
            if energy_min is not None:
                energy_mask &= energy >= energy_min
            if energy_max is not None:
                energy_mask &= energy <= energy_max
            time = time[energy_mask]
            label = label[energy_mask]
        # Aggregate (count) the ids for each time step. The result is an array of
        # time step vs label index with values of the number of occurrences of each
        # source. Previously this used pd.pivot_table which took about 41s to
        # calculate the source label dataframe for 66 runs each spanning 1 hour
        # of data. This takes 36s using numpy.histogram2d and 27s using
        # fast_histogram.histogram2d
        time_edges = self.get_time_bins(run_id, alignment="edges")
        label_edges = np.arange(self.label_manager.num_labels + 1) - 0.5
        if fast_histogram is None:
            tvl = np.histogram2d(time, label, (time_edges, label_edges))[0]
        else:
            tvl = fast_histogram.histogram2d(
                time,
                label,
                range=[
                    [time_edges[0], time_edges[-1]],
                    [label_edges[0], label_edges[-1]],
                ],
                bins=[time_edges.shape[0] - 1, self.label_manager.num_labels],
            )
        # Catch edge case with no background
        if tvl[:, 0].sum() == 0:
            raise RadaiDataSetError(
                "Cannot calculate SNR without background label column (0)."
            )

        # ----------------------------------------------------------------------
        # Calculate SNR
        # ----------------------------------------------------------------------

        B = tvl[:, 0:1]
        S = tvl
        np.seterr(invalid="ignore")
        if method == "s/sqrt(b)":
            snr = S / np.sqrt(B)
        elif method == "s/sqrt(s+b)":
            snr = S / np.sqrt(S + B)
        elif method == "s/b":
            snr = S / B
        elif method == "s/(s+b)":
            snr = S / (S + B)
        elif method == "s":
            snr = S
        elif method == "s+b":
            snr = S + B
        elif method == "currie":
            raise NotImplementedError()
        np.seterr(invalid="warn")
        # Set background SNR to NaN
        snr[:, 0] = np.nan
        # NOTE: not replacing inf with nan anymore. Thresholding will still work.
        # snr[snr == np.inf] = np.nan

        # ----------------------------------------------------------------------
        # Prepare dataframe (does not add significant overhead)
        # ----------------------------------------------------------------------

        # All label names (strings)
        labels = np.array(self.label_manager.get_labels_list())

        # Drop background (nan) and other missing labels
        if not include_all_labels:
            msk = snr.sum(axis=0) > 0
            snr = snr[:, msk]
            labels = labels[msk]

        # Make dataframe
        snr = pd.DataFrame(
            snr,
            index=pd.Index(np.arange(snr.shape[0]), name="time_idx"),
            columns=pd.Index(labels, name="label"),
        )

        # Optionally change the time index to the center of the step
        if index_by_time:
            snr.index = pd.Index(
                (snr.index.values + 0.5) * self.time_step_size,
                name="time_center",
            )

        # ----------------------------------------------------------------------
        # Aggregate SNR across time (for waterfalls)
        # ----------------------------------------------------------------------

        if aggregate_for_waterfalls:
            if aggregation_method not in ("max", "sum", "min", "mean"):
                raise RadaiDataSetError(
                    f"Unknown aggregation method: {aggregation_method}"
                )
            # Aggregate SNR in rolling time window of the waterfall time length
            snr = getattr(
                snr.rolling(
                    self.num_time_bins,
                    center=False,  # So the times are at the right index
                    axis=0,  # Along the time dim
                    closed="both",  # So the all points in the win are included
                ),
                aggregation_method,
            )().astype(np.float32)
            # Drop the leading rows leftover from the rolling agg above. The
            # first row with the an aggregate value is the num_time_bins or
            # index num_time_bins - 1
            snr = snr.iloc[self.num_time_bins - 1 :, :]
            # Reindex to get the start for the waterfall
            if index_by_time:
                snr.index -= (self.num_time_bins - 1) * self.time_step_size
                snr.index.name = "start_time"
            else:
                snr.index -= self.num_time_bins - 1
                snr.index.name = "start_time_idx"

        # ----------------------------------------------------------------------
        # Provide SNR data as dataframe of float32
        # ----------------------------------------------------------------------

        return snr

    @lru_cache(maxsize=2)
    def get_snr_for_runs(
        self,
        progress_bar: bool = False,
        run_ids: tuple = None,
        **snr_kwargs,
    ) -> pd.DataFrame:
        """Get SNR per source for every spectra in the provided run ids.

        Parameters
        ----------
        progress_bar : bool, optional
            Display a progress bar for runs, by default False
        run_ids : tuple, optional
            A list or array of the run_ids to consider (others will be skipped.)
            By default None (consider all runs.)

        Other Parameters
        ----------------
        snr_kwargs : dict, optional
            Keyword arguments for SNR calculation in get_snr_for_run.

        Notes
        -----
        This calls get_snr_for_run which is cached as described in the docstring
        for that method so subsequent calls without argument changes will be
        fast. This method is also cached for up to 2 results. For reference a
        dataset with 66 1 hour runs and 34 sources is about 30MB of memory.

        Returns
        -------
        pd.DataFrame
            SNR values (float32) indexed by run_id and start time index of each
            spectrum or waterfall with columns of every source id (even if the
            source is not present the fill value will be 0).
        """
        if run_ids is not None and len(run_ids) == 0:
            raise RadaiDataSetError("Cannot calc snr for 0 length number of runs!")
        data = {}
        for run_id in self.iter_run_ids(
            progress_bar=progress_bar,
            run_ids=None if run_ids is None else tuple(run_ids),
            desc="Calculating SNR for each run",
        ):
            # Calculate SNR for the entire run including all sources in the dataframe
            data[run_id] = self.get_snr_for_run(
                run_id, include_all_labels=True, index_by_time=False, **snr_kwargs
            )
            # Aggregation SNR values across time
        # Concatenate the run dataframes and create an additional index for run id
        # at level=0
        return pd.concat(data, names=["run_id"])

    # --------------------------------------------------------------------------
    # Simple source labeling based on evenly weighted source window
    # --------------------------------------------------------------------------

    @deprecated(
        reason="Calculating source ID's by a time window will move to "
        "ListmodeH5ClosestApproachLabeler"
    )
    def get_source_ids_in_time_window(
        self,
        run_id: int,
        time: int,
        time_window_half_width: int = 2000,
        return_bool_array: bool = False,
    ) -> np.ndarray:
        """Get an array of the source ID's within a time window for a particular run

        Parameters
        ----------
        run_id : int
            The run in which to search for source.
        time : int
            Time from the beginning of the run in milliseconds.
        time_window_half_width : int, optional
            The half width of the window around the time to search for a source,
            by default 2000
        return_bool_array : bool, optional
            Return a multi-label encoded vector similar to the output of
            `sklearn.preprocessing.MultiLabelBinarizer`, by default False

        Returns
        -------
        np.ndarray
            The source ID's in the time window either as a vector of the ID's or
            if `return_bool_array`, a vector of length num_labels with the
            found source ID's encoding by the index of True values. For example
            consider 4 sources (plus background):
                source_ids = 1, 3
                if return_bool_array:
                    return [0, 1, 0, 1, 0]
                else:
                    return [1, 3]
        """
        # Get the not-null sources (this is cached with lru_cache which
        # provides a nominal 5x speed up)
        sl = self.get_source_locations(run_id)
        # Figure out if this spectrum is near a source
        source_ids = np.asarray([], dtype=np.uint16)
        if len(sl) > 0:
            source_times = sl.get("time")
            time_from_sources = np.abs(source_times - time)
            near_sources = time_from_sources < time_window_half_width
            if np.any(near_sources):
                source_ids = sl.ids[near_sources]
        if return_bool_array:
            bool_array = np.zeros((self.label_manager.num_labels,), dtype=bool)
            # If background encode the first value
            if len(source_ids) == 0:
                bool_array[0] = True
            # Otherwise encode the source_id(s)
            else:
                bool_array[source_ids] = True
            return bool_array
        else:
            return source_ids

    # --------------------------------------------------------------------------
    # Training / Testing data evaluation
    # --------------------------------------------------------------------------

    def _check_label_coverage(self, run_ids: Union[list, None]) -> None:
        """Get source locations for all or a subset of runs and check that all
        source labels are covered.

        Parameters
        ----------
        run_ids : Union[list, None], optional
            A subset of run ID's to consider, by default None (all runs)

        Raises
        ------
        RadaiDataSetError
            If any labels in the label manager are not in the source locations
            for all or a subset of the runs.
        """
        needs_labels = set(self.label_manager.get_labels_list())
        # Get labels per run
        lcpr = self.get_label_count_per_run()
        # No background
        needs_labels.remove(self.label_manager.background_label)
        lcpr = lcpr.iloc[:, 1:]
        # Clip runs
        run_ids = self.run_ids if run_ids is None else np.unique(run_ids).astype(int)
        lcpr = lcpr.loc[run_ids]
        # Find missing labels
        present = lcpr.sum(axis=0) > 0
        has_labels = set(present.index[present.values])
        if has_labels != needs_labels:
            raise RadaiDataSetError(
                f"{len(run_ids)} requested run(s) missing "
                f"{len(needs_labels - has_labels)} labels: "
                f"{sorted(needs_labels - has_labels)}"
            )

    def _get_indices_for_each_label(
        self,
        run_ids: Union[tuple, None] = None,
        for_waterfalls: bool = False,
        allow_multiple_label_encounters: bool = False,
        progress_bar: bool = True,
    ) -> dict:
        """Get the spectrum or waterfall indices for each label in the label
        manager across all or a subset of runs.

        Parameters
        ----------
        run_ids : Union[tuple, None], optional
            A subset of run ID's to consider, by default None (all runs)
        for_waterfalls : bool, optional
            Get the indices for waterfalls with each label instead of spectra
            with each label, by default False
        allow_multiple_label_encounters : bool, optional
            Allow indices for items in which multiple labels are present, by
            default False
        progress_bar : bool, optional
            Include progress bars for various iterations, by default False

        Returns
        -------
        dict
            The indices (array of [run_id, time_idx] tuples) referenced by
            label names (string).
        """
        # Boolean dataframe for locations with source presence based on labeler
        # configuration
        ldf = self.labeler.make_boolean_dataframe(
            for_waterfalls=for_waterfalls,
            run_ids=None if run_ids is None else tuple(run_ids),
            progress_bar=progress_bar,
        )
        # Separate background and source booleans
        background = ldf.iloc[:, 0]
        sources = ldf.iloc[:, 1:]
        # Build label to indices map
        l2i = {
            self.label_manager.background_label: background.index[
                background.values
            ].to_numpy()
        }
        # Multi encounter mask
        msk_multi = sources.sum(axis="columns").values == 1
        for label in sources:
            # Rows with this label
            msk = sources[label].values
            # Optionally exclude multi-source encounters (creates new mask)
            if not allow_multiple_label_encounters:
                msk = msk & msk_multi
            # Get indices
            l2i[label] = sources.index[msk].to_numpy()

        # TODO: move these to tests once a smaller dataset is made
        # No duplicate indices for each label
        for k in l2i:
            assert l2i[k].shape[0] == len(set(l2i[k]))
        if not allow_multiple_label_encounters:
            # No overlap
            _all = set()
            _total = 0
            for k in l2i:
                _all.update(set(l2i[k]))
                _total += len(l2i[k])
            assert len(_all) == _total
            # No multi source (background items have no other labels too)
            for k in l2i:
                assert np.all(ldf.loc[l2i[k]].sum(axis="columns").values == 1)

        # Provide map
        return l2i

    def _get_augmentor_seed_and_indices_groups_per_encounter(
        self,
        num_per_bkg: int,
        num_per_src: int,
        run_ids: tuple = None,
        indices_selection_seed: int = 42,
        progress_bar: bool = False,
        encounter_center_range: tuple = (0.1, 0.9),
    ) -> list:

        # check inputs
        if len(encounter_center_range) != 2:
            raise RadaiDataSetError(
                "encounter_center_range must have two elements "
                f"not {len(encounter_center_range)}."
            )
        if encounter_center_range[0] >= encounter_center_range[1]:
            raise RadaiDataSetError(
                f"encounter_center_range: first element ({encounter_center_range[0]}) "
                f"must be less than the second ({encounter_center_range[1]})."
            )

        # sample from specified runs
        if run_ids is None:
            run_ids = self.run_ids
        print(f"Iterating through encounters using {len(run_ids)} run_ids.")
        # get and compute some helpers variables
        sldf = self.get_source_locations_dataframe()
        lcpr = self.get_label_count_per_run()
        bkg_run_ids = lcpr.loc[lcpr["BKG"] == 1].index.values
        bkg_run_ids = list(set(bkg_run_ids).intersection(set(run_ids)))
        bkg_run_idx_length = {
            rid: int(
                np.floor(self.get_end_time(rid) / self.time_step_size)
                - self.num_time_bins
            )
            for rid in bkg_run_ids
        }

        # helper function to check whether we have all of the seeds/indices we need
        def is_done(wf_idx_dic, num_per_src, num_per_bkg):
            good_src = all(
                [len(v) == num_per_src for k, v in wf_idx_dic.items() if k != "BKG"]
            )
            good_bkg = len(wf_idx_dic["BKG"]) == num_per_bkg
            return good_src and good_bkg

        # shuffle source locations
        sldf = sldf.sample(frac=1, random_state=indices_selection_seed, replace=False)

        # make vars to run our loop
        wf_idx_dic = {k: [] for k in self.label_manager.get_labels_list()}
        seed_and_indices = []
        good = is_done(wf_idx_dic, num_per_src, num_per_bkg)
        rng = np.random.default_rng(seed=indices_selection_seed)

        while not good:
            indices = []
            print("Generating indices for source encounters")
            for idx in tqdm(
                range(len(sldf)), disable=not progress_bar, desc="source encounters"
            ):
                row = sldf.iloc[idx]
                run_id = row.name[0]
                center_time = row.name[1]
                # Don't load data if we're already set with that source type
                # or if the run_id is not in our specified set.
                if (not (len(wf_idx_dic[row.label]) < num_per_src)) or (
                    run_id not in run_ids
                ):
                    continue

                # grab the time centers of the run
                # (remove those that can't be used for a waterfall)
                time_bin_centers = self.get_time_bins(
                    run_id=run_id, alignment="centers"
                )[: -self.num_time_bins + 1]
                # get boolean dataframe of waterfall indices that meet snr needs
                bdf = self.labeler.make_boolean_dataframe(
                    for_waterfalls=True, run_ids=tuple([run_id])
                )
                wf_indices = np.arange(len(time_bin_centers)).astype(int)
                # Slice around the encounter
                # (remember indices will correspond to waterfall start)
                margin = self.num_time_bins * self.time_step_size  # milliseconds
                mask = (
                    time_bin_centers
                    >= (center_time - margin * encounter_center_range[1])
                ) & (
                    time_bin_centers
                    <= (center_time - margin * encounter_center_range[0])
                )
                # combine with boolean dataframe info
                mask = mask & bdf[row.label].values
                # Ensure just one source per wf
                mask = mask & (bdf.sum(axis=1) == 1).values
                # if acceptable solutions add one
                if mask.sum():
                    slice_indices = wf_indices[mask]
                    encounter_idx = (run_id, rng.choice(slice_indices))
                    wf_idx_dic[row.label].append(encounter_idx)
                    indices.append(encounter_idx)

            # generate background indices for this iterator seed value
            print("Generating indices for background encounters")
            num_srcs_added = len(indices)
            # Sample a limited number of new backgrounds to make
            # sure we're getting augmentor variability
            for idy in tqdm(
                range(num_srcs_added), disable=not progress_bar, desc="backgrounds"
            ):
                # Break if we have enough backgrounds
                if len(wf_idx_dic["BKG"]) == num_per_bkg:
                    break
                # if we have dedicated background runs sample a background from there
                if len(bkg_run_ids) > 0:
                    brid = rng.choice(bkg_run_ids)
                    bidx = rng.integers(0, bkg_run_idx_length[brid])
                    bkg_idx = (brid, bidx)
                # otherwise, use bdf to sample a background wf idx from here
                else:
                    while True:
                        brid = rng.choice(run_ids)
                        bdf = self.labeler.make_boolean_dataframe(
                            for_waterfalls=True, run_ids=tuple([brid])
                        )
                        if len(bdf.loc[bdf["BKG"].values]) > 0:
                            break
                    bkg_sample = bdf.loc[bdf["BKG"].values].sample(
                        random_state=encounter_idx[1]
                    )
                    random_bkg_idx = bkg_sample.index.get_level_values(
                        "start_time_idx"
                    ).values[0]
                    bkg_idx = (
                        brid,
                        random_bkg_idx,
                    )
                wf_idx_dic["BKG"].append(bkg_idx)
                indices.append(bkg_idx)

            # follow the same pattern as _get_augmentor_seed_and_indices_groups
            # append tuple of seed and list corresponding to that seed
            seed_and_indices.append((self.augmentor.seed, indices))
            # check if we're all set or if we need another round.
            good = is_done(wf_idx_dic, num_per_src, num_per_bkg)
            # update augmentor seed
            self.augmentor.increment_seed()
            print(
                "finished loop, will continue until targets hit.",
                [len(v) for v in wf_idx_dic.values()],
            )

        return seed_and_indices

    def _get_augmentor_seed_and_indices_groups(
        self,
        num_per_bkg: int,
        num_per_src: int,
        run_ids: tuple = None,
        indices_selection_seed: int = 42,
        for_waterfalls: bool = False,
        per_encounter: bool = True,
        clip_for_batch: bool = False,
        progress_bar: bool = False,
        encounter_center_range: tuple = (0.1, 0.9),
    ) -> list:
        """Collect groups of indices coupled with an augmentor seed which will
        generate enough data for the provided number of background and source
        items.

        Parameters
        ----------
        num_per_bkg : int
            Number of background indices to generate.
        num_per_src : int
            Number of indices per non-background labels to generate.
        run_ids : tuple, optional
            A subset of run ID's to consider, by default None (all runs)
        indices_selection_seed : int, optional
            Random number generator seed for the indices selection for background
            and source items, by default 42
        for_waterfalls : bool, optional
            Get the indices for waterfalls with each label instead of spectra
            with each label, by default False
        clip_for_batch : bool, optional
            Clip random items to make each group an even multiple of the batch
            size, by default False
        progress_bar : bool, optional
            Include progress bars for various iterations, by default False
        encounter_center_range : tuple, optional
            The range within an encounter when the center time of a source
            encounter (as defined in the schema) can occur, normalized to the
            length of the encounter (`num_time_bins * time_step_size`)
            so 0 = start of the encounter and 1 = end of the encounter.

        Returns
        -------
        list
            Iterable of (augmentor seed, (run/time indices)) tuples covering the
            number of background and source items requested.

        Raises
        ------
        RadaiDataSetError
            If the number of background/source items require additional loops
            through the runs but the augmentor is not configured.
        """

        # Random number generator for later
        rng = np.random.default_rng(indices_selection_seed)

        # If the augmentor is disabled, get indices per label for single source
        # and check if there are enough items.
        if not self.augmentor.will_augment():
            print(
                "The augmentor is disabled, checking if there are enough "
                "items per label"
            )
            _l2i = self._get_indices_for_each_label(
                run_ids=None if run_ids is None else tuple(run_ids),
                for_waterfalls=for_waterfalls,
                allow_multiple_label_encounters=False,
                progress_bar=progress_bar,
            )
            # Check background
            if num_per_bkg > len(_l2i[self.label_manager.background_label]):
                raise RadaiDataSetError(
                    f"Selecting more background ({num_per_bkg}) than available "
                    f"({len(_l2i[self.label_manager.background_label])}) will "
                    "require an additional loop through the runs and is not "
                    "supported unless you configure the augmentor!"
                )
            # Check sources
            for label in _l2i:
                if label == self.label_manager.background_label:
                    continue
                if num_per_src >= len(_l2i[label]):
                    raise RadaiDataSetError(
                        f"Selecting more source label {label} ({num_per_src}) than "
                        f"available ({len(_l2i[label])}) will require an additional "
                        "loop through the runs and is not supported unless you "
                        "configure the augmentor!"
                    )

        # Store augmentor seed to reset it at the end
        aug_seed_init = self.augmentor.seed
        if for_waterfalls and per_encounter:
            seed_and_indices = (
                self._get_augmentor_seed_and_indices_groups_per_encounter(
                    num_per_bkg=num_per_bkg,
                    num_per_src=num_per_src,
                    run_ids=run_ids,
                    indices_selection_seed=indices_selection_seed,
                    progress_bar=progress_bar,
                    encounter_center_range=encounter_center_range,
                )
            )
            # Reset seed
            print(f"Resetting the augmentor seed to: {aug_seed_init}")
            self.augmentor.set_seed(aug_seed_init)
            # Provide results
            return seed_and_indices

        # How many indices per source still need to be generated
        _indices_remaining = pd.Series(dtype=int)
        for _l in self.label_manager.get_labels_list():
            if _l == self.label_manager.background_label:
                _indices_remaining[_l] = num_per_bkg
            else:
                _indices_remaining[_l] = num_per_src
        # The source locations do not change with augmentations
        sldf = self.get_source_locations_dataframe()
        # Seed and indices groups
        seed_and_indices = []
        # Iterate through indices selection and seed increment rounds until enough
        # indices per source are obtained.
        _i, _idx, _lab, _num, _num_extra, _l = 0, [], [], None, None, None
        _remaining_run_ids = None if run_ids is None else tuple(run_ids)
        _remaining_labels = None
        _sldf_run_msk = None
        print("Getting indices and incrementing the augmentor seed as needed ...")
        while _indices_remaining.sum() > 0:
            _i += 1
            _remaining_labels = _indices_remaining.index[
                _indices_remaining > 0
            ].tolist()
            print(
                f"Calculating seed group {_i} (seed={self.augmentor.seed}) "
                f"{_indices_remaining.sum()} indices remain and "
                f"{len(_remaining_labels)} labels remain: {_remaining_labels}"
            )
            # If background in the remaining labels, consider all runs
            if any(n in _remaining_labels for n in BACKGROUND_NAMES):
                _remaining_run_ids = None if run_ids is None else tuple(run_ids)
            else:
                # Find runs with the remaining labels to reduce label calculation
                _sldf_run_msk = np.zeros(shape=len(sldf), dtype=bool)
                for _l in _remaining_labels:
                    _sldf_run_msk |= (sldf["label"] == _l).values
                _remaining_run_ids = tuple(
                    sldf.index[_sldf_run_msk].get_level_values("run_id").unique()
                )
                if run_ids is not None:
                    _remaining_run_ids = tuple(set(run_ids) & set(_remaining_run_ids))
            # Calculate indices for labels in the current augmentation state
            _l2i = self._get_indices_for_each_label(
                run_ids=_remaining_run_ids,
                for_waterfalls=for_waterfalls,
                allow_multiple_label_encounters=False,
                progress_bar=progress_bar,
            )
            # Collect indices for each label which still requires data
            for _l in _remaining_labels:
                _num = min(_indices_remaining[_l], len(_l2i[_l]))
                _idx.extend(
                    rng.choice(_l2i[_l], size=_num, shuffle=False, replace=False)
                )
                _lab.extend([_l] * _num)
                # Update remaining indices for the next round(s)
                _indices_remaining[_l] -= _num
            # If needed check if this is an even batch size and handle accordingly
            if clip_for_batch:
                # Drop the last ones if we are at the end.
                if len(_idx) < self.batch_size:
                    print(
                        f"WARNING: dropping {len(_idx)} indices to meet "
                        f"batch size: {self.batch_size}"
                    )
                    break
                # Otherwise randomly drop indices to make this data chunk an
                # even multiple of the batch size.
                _num_extra = len(_idx) % self.batch_size
                for i in rng.choice(_num, size=_num_extra, replace=False):
                    _l = _lab.pop(i)
                    _idx.pop(i)
                    _indices_remaining[_l] += 1
            # Store augmentor seed and list of indices for data generation below
            seed_and_indices.append((self.augmentor.seed, _idx))
            # Increment augmentor seed
            self.augmentor.increment_seed()
            # Reset for next round
            _idx, _lab, _num, _num_extra, _l = [], [], None, None, None
        # Reset seed
        print(f"Resetting the augmentor seed to: {aug_seed_init}")
        self.augmentor.set_seed(aug_seed_init)
        # Provide results
        return seed_and_indices

    def generate_data(
        self,
        method: str = "iter_spectra",
        number: int = 10000,
        background_to_source_ratio: float = 1.0,
        indices_selection_seed: int = 42,
        run_ids: tuple = None,
        progress_bar: bool = True,
        return_bkg: bool = False,
        encounter_center_range: tuple = (0.1, 0.9),
    ):
        """Generate a spectra/waterfalls/batches dataset with even source and
        background labeled items.

        This is the intended data interface for training/validation. It uses the
        Labeler and ListmodeH5Augmentor to identify run/time indices which contain
        sources, select a number of indices to meet the total number of items
        with the background_to_source_ratio which is yet to be implemented.
        The returned or yielded data are DataTuples which contain data arrays,
        labels and other metadata.

        Parameters
        ----------
        method : str, optional
            Data generation type from the following options:
                - spectrum_batch: BatchData[N, 1, E] of spectra
                - iter_spectra: Iterate SpectraData (used by spectrum_batch)
                - iter_waterfalls: Iterate WaterfallData (greedy, be careful with this)
                - iter_encounters: Iterate on Encounters (waterfalls)
                - iter_batches: Iterate BatchData
                - keras_generator: NotImplemented
            By default "iter_spectra"
        number : int, optional
            Total number of items to generate, by default 10000
        background_to_source_ratio : float, optional
            The ratio of background items to any one of the non-background
            sources. By default 1.
        seed : int, optional
            Random number generator seed for the indices selection for background
            and source items, by default 42
        run_ids : tuple, optional
            A subset of run ID's to consider. This may cause issues if this
            excludes source's in the global LabelManager which are currently ALL
            required, by default None (all runs)
        progress_bar : bool, optional
            Include progress bars for various iterations during data preparation
            and production, by default True
        return_bkg : bool, optional
            Also return the background-only data. Use this if interested in
            seeing the exact source and background data generated and not just
            their sum. By default False.
        encounter_center_range : tuple, optional
            The range within an encounter when the center time of a source
            encounter (as defined in the schema) can occur, normalized to the
            length of the encounter (`num_time_bins * time_step_size`)
            so 0 = start of the encounter and 1 = end of the encounter.

        Returns
        -------
        SpectrumData or WaterfallData or BatchData
            The returned or yielded DataTuple depending on the `method`.
            Two of these will be returned if `return_bkg` is True.

        Raises
        ------
        RadaiDataSetError
            - If source ID's from the global LabelManager are not present.
            - If the data generation method is not valid.
            - If the number of items requires additional loops through the runs
              but the augmentor is not configured.
        NotImplementedError
            - If the keras_generator method (it is coming soon)!

        Notes
        -----
        The iter_* methods will sort by the run index to improve IO efficiency
        so shuffling is only performed on a per run basis (internally).
        """
        if method == "keras_generator":
            raise NotImplementedError()
        print(f"Starting data generation for method: {method}")
        print(f"The labeler is: {self.labeler}")
        # Data generation method
        if method not in (
            "spectrum_batch",
            "iter_spectra",
            "iter_waterfalls",
            "iter_batches",
            "keras_generator",
            "iter_encounters",
        ):
            raise RadaiDataSetError(f"Invalid data generation method: {method}")
        # Total number of items
        number = int(number)
        # Get label type
        for_waterfalls = method in (
            "iter_waterfalls",
            "iter_batches",
            "iter_encounters",
        )
        per_encounter = method in ("iter_encounters")
        # Clip groups of spectra or waterfalls
        clip_for_batch = method in ("iter_batches", "keras_generator")

        # ----------------------------------------------------------------------
        # Get all source locations and check that all source labels are covered
        # ----------------------------------------------------------------------

        print("Confirming all sources are present")
        self._check_label_coverage(run_ids)

        # ----------------------------------------------------------------------
        # Calculate the number of items per background and source label
        # ----------------------------------------------------------------------

        # Number per source
        num_src_labels = self.label_manager.num_labels - 1
        num_per_src = int(number / (background_to_source_ratio + num_src_labels))
        # Number per background (could be different from num_per_src)
        num_per_bkg = number - num_per_src * num_src_labels
        # Report to user
        print(
            f"Prepping to generate {num_per_bkg} background labeled items and "
            f"{num_per_src} source labeled items for {num_src_labels} sources."
        )

        # ----------------------------------------------------------------------
        # Collect indices and seeds which will generate enough data
        # ----------------------------------------------------------------------

        # Cache augmentor seed to reset it at the end
        aug_seed_init = self.augmentor.seed
        seeds_and_indices = self._get_augmentor_seed_and_indices_groups(
            num_per_bkg=num_per_bkg,
            num_per_src=num_per_src,
            run_ids=None if run_ids is None else tuple(run_ids),
            indices_selection_seed=indices_selection_seed,
            for_waterfalls=for_waterfalls,
            per_encounter=per_encounter,
            clip_for_batch=clip_for_batch,
            progress_bar=progress_bar,
            encounter_center_range=encounter_center_range,
        )
        # Reset seed
        self.augmentor.set_seed(aug_seed_init)
        # Sanity check
        _num_idx = sum(len(sia[1]) for sia in seeds_and_indices)
        if method in ("iter_batches", "keras_generator") and _num_idx < number:
            print(
                f"WARNING: dropped {number - _num_idx} items to meet batch size"
                f"of {self.batch_size} from the requested {number} items."
            )
        elif _num_idx != number:
            raise RadaiDataSetError(
                "Incorrect number of items will be generated: "
                f"{_num_idx} vs {number}. This is likely a bug. Please file an "
                "issue here: https://gitlab.com/lbl-anp/radai/radai/-/issues"
            )

        # ----------------------------------------------------------------------
        # Produce the data!
        # ----------------------------------------------------------------------

        print(
            f"Generating data ({_num_idx} items across {len(seeds_and_indices)} "
            "seed groups!"
        )

        # Cache augmentor seed to reset it at the end
        aug_seed_init = self.augmentor.seed

        # Helper to iterate for multiple seeds
        class SeedIter:
            def __init__(self, seed_setter: Callable) -> None:
                self.seeds = []
                self.iters = []
                self.seed_setter = seed_setter

            def append(self, seed, iter_):
                self.seeds.append(int(seed))
                self.iters.append(iter_)

            def __iter__(self):
                for i, (seed, iter_) in enumerate(zip(self.seeds, self.iters)):
                    print(
                        f"Iterating augmentor seed group {i+1} of "
                        f"{len(self.seeds)} (seed={seed}) ..."
                    )
                    self.seed_setter(seed)
                    for x in iter_:
                        yield x

        # Provide data based on method
        _spectrum_data = None
        _seed = None
        _indices = None
        _kw = None
        _iter = None
        _seed_iter = None
        try:
            # Spectra batch is a special case
            if method == "spectrum_batch":
                _spectrum_data = {None: []}
                if return_bkg:
                    _spectrum_data["BKG"] = []
                for label in _spectrum_data:
                    for i, (_seed, _indices) in enumerate(seeds_and_indices):
                        print(
                            f"Iterating augmentor seed group {i+1} of "
                            f"{len(seeds_and_indices)} (seed={_seed}) ..."
                        )
                        self.augmentor.set_seed(_seed)
                        for sd in self.iter_spectra(
                            progress_bar=progress_bar, indices=_indices, label=label
                        ):
                            _spectrum_data[label].append(sd)
                if not return_bkg:
                    return BatchData.from_spectrum_data(*_spectrum_data[None])
                else:
                    return (
                        BatchData.from_spectrum_data(*_spectrum_data[None]),
                        BatchData.from_spectrum_data(*_spectrum_data["BKG"]),
                    )
            elif method == "keras_generator":
                raise NotImplementedError()
            else:
                _seed_iter = {None: SeedIter(self.augmentor.set_seed)}
                if return_bkg:
                    _seed_iter["BKG"] = SeedIter(self.augmentor.set_seed)
                for label in _seed_iter:
                    for _seed, _indices in seeds_and_indices:
                        _kw = dict(
                            progress_bar=progress_bar, indices=_indices, label=label
                        )
                        if method == "iter_spectra":
                            _iter = self.iter_spectra(**_kw)
                        elif (method == "iter_waterfalls") or (
                            method == "iter_encounters"
                        ):
                            _iter = self.iter_waterfalls(**_kw)
                        elif method == "iter_batches":
                            _iter = self.iter_batches(**_kw)
                        _seed_iter[label].append(_seed, _iter)
                if not return_bkg:
                    return _seed_iter[None]
                else:
                    return _seed_iter[None], _seed_iter["BKG"]
        except KeyboardInterrupt:
            pass
        finally:
            # Reset seed
            self.augmentor.set_seed(aug_seed_init)
