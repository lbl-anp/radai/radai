"""
radai ListmodeH5 schema

Authors: Joey Curtis and James Ghawaly
Format Version: 7

Units: m/s/keV

Specific variable units:
    * (dt) ms since last event
    * (time) ms since run start
    * (timestamp) posix seconds
"""
import numpy as np
from ..core.h5_schema import (
    Root,
    Attribute,
    DataSet,
    Group,
    IterGroup,
    SharedDim,
    SharedDims,
)

VERSION = 7


def run_factory(i):
    dims = SharedDims()
    dims.register("NB", SharedDim("number of blocks along a run"))
    dims.register("NR", SharedDim("number of rain events"))
    dims.register("NL", SharedDim("number of listmode events"))
    dims.register("NS", SharedDim("number of sources"))
    dims.register("NST", SharedDim("number of timed entries for source data (1Hz)"))
    dims.register("NDT", SharedDim("number of timed detector positions (10Hz)"))
    dims.register("VT", SharedDim("number of timed variation metrics"))
    blocks = Group(
        "blocks",
        items=[
            DataSet("id", dtype="uint8", shape=("NB",), required=True),
            DataSet(
                "end_time",
                dtype="uint32",
                shape=("NB",),
                required=True,
                note=(
                    "The start time for the first block is 0. The start time "
                    "for each sequential block is the end_time from the "
                    "previous block."
                ),
            ),
            DataSet(
                "end_distance",
                dtype="float32",
                shape=("NB",),
                required=True,
                note=(
                    "The start distance for the first block is 0. The start "
                    "distance for each sequential block is the end_distance "
                    "from the previous block."
                ),
            ),
        ],
    )
    rain = Group(
        "rain",
        items=[
            DataSet("id", dtype="uint16", shape=("NR",), required=True),
            DataSet(
                "start_time",
                dtype="uint32",
                shape=("NR",),
                required=True,
                note="start time in ms since start of the run",
            ),
            DataSet(
                "end_time",
                dtype="uint32",
                shape=("NR",),
                required=True,
                note="end time in ms since start of the run",
            ),
        ],
    )
    listmode = Group(
        "listmode",
        required=True,
        items=[
            DataSet(
                "energy",
                dtype="float32",
                shape=("NL",),
                required=True,
                note="energy of photon detection event (keV)",
            ),
            DataSet(
                "dt",
                dtype="uint16",
                shape=("NL",),
                required=True,
                note="elapsed time since last photon detection event (microseconds)",
            ),
            DataSet(
                "id",
                dtype="uint16",
                shape=("NL",),
                note="source ID from which this photon originated.",
            ),
            DataSet(
                "background_id",
                dtype="uint8",
                shape=("NL",),
                note="background ID from which this photon originated.",
            ),
        ],
    )
    sources = Group(
        "sources",
        required=True,
        items=[
            DataSet(
                "id",
                dtype="uint16",
                shape=("NS",),
                required=True,
                note="unique integer label of source (must be in source_ids)",
            ),
            DataSet(
                "time",
                dtype="uint32",
                shape=("NS",),
                required=True,
                note=(
                    "elapsed milliseconds to the time of closest approach from the "
                    "start of the run"
                ),
            ),
            DataSet(
                "distance",
                dtype="float32",
                shape=("NS",),
                required=True,
                note=(
                    "distance in meters to closest approach from the start of "
                    "the run"
                ),
            ),
            DataSet(
                "location_id",
                dtype="uint16",
                shape=("NS",),
                note="unique integer label of source location",
            ),
            DataSet(
                "shielding_id",
                dtype="uint8",
                shape=("NS",),
                note="unique integer label of source shielding (0 is no shielding)",
            ),
            DataSet(
                "standoff",
                dtype="float32",
                shape=("NS",),
                note="distance from detector to source at time of closest approach",
            ),
            Group(
                "snr",
                items=[
                    DataSet(
                        "peak",
                        dtype="float32",
                        shape=("NS",),
                        note="SNR at peak of encounter",
                    ),
                    DataSet(
                        "integral",
                        dtype="float32",
                        shape=("NS",),
                        note="SNR integrated over entire encounter",
                    ),
                ],
            ),
            Group(
                "position",
                items=[
                    DataSet(
                        "time",
                        dtype="uint32",
                        shape=("NST",),
                        note="elapsed milliseconds from the start at 1Hz",
                    ),
                    DataSet(
                        "x",
                        dtype="float32",
                        shape=(
                            "NST",
                            "NS",
                        ),
                    ),
                    DataSet(
                        "y",
                        dtype="float32",
                        shape=(
                            "NST",
                            "NS",
                        ),
                    ),
                    DataSet(
                        "z",
                        dtype="float32",
                        shape=(
                            "NST",
                            "NS",
                        ),
                    ),
                ],
            ),
        ],
        note=(
            "If run is background only the arrays contain a single "
            "source_id 0 (background) at time/distance 0"
        ),
    )
    detector = Group(
        "detector",
        required=True,
        attributes=[
            Attribute("street_name", str),
            Attribute("lane", type_=list, list_item_type=np.integer),
            Attribute(
                "direction",
                np.integer,
                note="direction of travel on the road, 0 is negative and 1 is positive",
            ),
            Attribute("detector_material", str, note="Detector material like NaI(Tl)"),
            Attribute("detector_size", str, note="Size of detector in cm"),
            Attribute(
                "detector_orientation",
                str,
                note="Description of detector orientation",
            ),
        ],
        items=[
            Group(
                "position",
                items=[
                    DataSet(
                        "time",
                        dtype="uint32",
                        shape=("NDT",),
                        required=True,
                        note="elapsed milliseconds from the start of the run at 10Hz",
                    ),
                    DataSet(
                        "distance",
                        dtype="float32",
                        shape=("NDT",),
                        required=True,
                        note=(
                            "distance in meters from the starting position "
                            "of the run at 10Hz"
                        ),
                    ),
                    DataSet(
                        "velocity",
                        dtype="float32",
                        shape=("NDT",),
                    ),
                    DataSet(
                        "acceleration",
                        dtype="float32",
                        shape=("NDT",),
                    ),
                    DataSet(
                        "block_id",
                        dtype="uint8",
                        shape=("NDT",),
                        note="id of the block the detector is within",
                    ),
                    DataSet(
                        "latitude",
                        dtype="float64",
                        shape=("NDT",),
                    ),
                    DataSet(
                        "longitude",
                        dtype="float64",
                        shape=("NDT",),
                    ),
                ],
            ),
        ],
    )
    diagnostics = Group(
        "diagnostics",
        items=[
            DataSet(
                "spectral_count_rate_variation",
                dtype="float32",
                shape=("VT",),
                note="Spectral count rate variation metric",
            ),
            DataSet(
                "gross_count_rate_variation",
                dtype="float32",
                shape=("VT",),
                note="Gross count rate variation metric",
            ),
        ],
    )
    return Group(
        name=f"run{i}",
        attributes=[
            Attribute("start_time", type_=np.floating, note="generally 0.0"),
            Attribute(
                "end_time",
                type_=np.floating,
                note="generally cumulative sum of the listmode dt",
            ),
            Attribute(
                "seed",
                type_=np.integer,
                note="random initialization of run for reproducibility",
            ),
        ],
        items=[blocks, rain, listmode, sources, detector, diagnostics],
        shared_dims=dims,
        required=True,
    )


SCHEMA = Root(
    attributes=[
        Attribute("format", type_=np.integer, required=True),
        Attribute("source_ids", type_=list, list_item_type=np.integer, required=True),
        Attribute("source_names", type_=list, list_item_type=str, required=True),
        Attribute(
            "background_ids",
            type_=list,
            list_item_type=np.integer,
            note="Background source type (i.e. K/U/T/Cosmic/etc)",
        ),
        Attribute(
            "background_names",
            type_=list,
            list_item_type=str,
            note="Background source type (i.e. K/U/T/Cosmic/etc)",
        ),
        Attribute(
            "source_shielding_ids",
            type_=list,
            list_item_type=np.integer,
            required=True,
            note="(8-bit enum, 0=False, 1=True)",
        ),
        Attribute(
            "source_shielding_names",
            type_=list,
            list_item_type=str,
            required=True,
        ),
    ],
    items=[IterGroup("runs", item_factory=run_factory, required=True)],
)
