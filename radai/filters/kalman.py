"""A Kalman filter for count rates."""

import numpy as np
from .filter import CountRateFilter, _count_rate_and_cov


class KalmanFilter(CountRateFilter):
    """Kalman filter for count rate data.

    Notation follows the conventions on Wikipedia:
        https://en.wikipedia.org/wiki/Kalman_filter#Underlying_dynamical_system_model
        https://en.wikipedia.org/wiki/Kalman_filter#Details

    Uses Poisson statistics to calculate the measurement noise matrix `R`
    for each measurement.

    """

    _attrs = ["F", "H", "Q", "H_inv", "x_k", "P_k"]

    def __init__(
        self, F=None, H=None, Q=None, H_inv=None, x_k=None, P_k=None, **kwargs
    ):
        """Initialize the filter and check the parameters.

        Parameters
        ----------
        F : array_like
            The state-transition model, shape (n_state, n_state).
        H : array_like
            The observation model, shape (n_measurement, n_state).
        Q : array_like
            The process noise, shape (n_state, n_state).
        H_inv : array_like
            Not in the standard notation, this matrix is used to initialize
            the state using the first measurement.
            Shape is (n_state, n_measurement).
        """
        # ensure parameters are numpy arrays
        F = np.asarray(F)
        H = np.asarray(H)
        Q = np.asarray(Q)
        H_inv = np.asarray(H_inv)
        assert len(F.shape) == 2
        assert len(H.shape) == 2
        assert len(Q.shape) == 2
        assert len(H_inv.shape) == 2
        # check that the inputs have consistent dimensions
        n_meas, n_state = H.shape
        assert F.shape == (n_state, n_state)
        assert Q.shape == (n_state, n_state)
        assert H_inv.shape == (n_state, n_meas)
        # initialize the state and its covariance
        if x_k is not None:
            x_k = np.asarray(x_k)
            P_k = np.asarray(P_k)
            assert x_k.shape == (n_state,)
            assert P_k.shape == (n_state, n_state)
        else:
            x_k = None
            P_k = np.array(Q)
        # call base init
        kwargs["F"] = F
        kwargs["H"] = H
        kwargs["Q"] = Q
        kwargs["H_inv"] = H_inv
        kwargs["x_k"] = x_k
        kwargs["P_k"] = P_k
        super().__init__(**kwargs)

    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        # perform Kalman filter update step
        assert self.x_k is not None
        self.x_k = self.F @ self.x_k
        self.P_k = self.F @ self.P_k @ self.F.T + self.Q
        # scale by the measurement live time
        mean = self.x_k * live_time
        cov = self.P_k * live_time**2
        # add measurement covariance to the state covariance
        # this accounts for Poisson uncertainty in the measurement
        cov += np.diag(mean)
        return mean, cov

    def update(self, spectrum, timestamp, live_time):
        """Update the filter with the current measurement.

        Parameters
        ----------
        spectrum : int or array_like
            The measured spectrum, either an integer or 1D array.
        timestamp : float
            The timestamp of the measurement.
        live_time : float
            The live time of the measurement.
        """
        # calculate count rate measurement and covariance matrix
        z_k, R_k = _count_rate_and_cov(spectrum, live_time)

        # initialize the state if this is the first update
        if self.x_k is None:
            self.x_k = self.H_inv @ z_k

        # update step
        y_k = z_k - self.H @ self.x_k
        S_k = self.H @ self.P_k @ self.H.T + R_k
        K_k = self.P_k @ self.H.T @ np.linalg.inv(S_k)
        self.x_k = self.x_k + K_k @ y_k
        self.P_k = (np.eye(self.H.shape[1]) - K_k @ self.H) @ self.P_k
