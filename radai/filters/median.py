"""A median filter for count rates."""

import numpy as np
from .filter import _count_rate_sample_mean_median_cov
from .moving import MovingAverageFilter


class MedianFilter(MovingAverageFilter):
    """A filter that calculates the median of the count rates.

    The parameter `n` is the length of the buffer of spectra that are used
    in the filter, and `n` must be an odd integer >= 3. The (`n` - 1) / 2
    spectra on either side of the central measurement will be used to
    calculate the median count rate.

    For multiple dimensions, applies the filter over each dimension
    independently.

    The covariance is the second moment of the `n` - 1 measurements
    symmetrically surrounding the central measurement.
    """

    def __init__(self, n=101, **kwargs):
        """Initialize the filter and check the parameters.

        Parameters
        ----------
        n : int
            The length of the buffer to use for the median filter.

        Other Parameters
        ----------------
        See docstring for `MovingAverageFilter.__init__` for other kwargs.
        """
        assert n >= 3
        assert n % 2 == 1
        kwargs["n"] = n
        super().__init__(**kwargs)

    def predict(self, live_time):
        """Predict the mean and covariance matrix of the next measurement.

        Parameters
        ----------
        live_time : float
            The live time of the next measurement. Used to scale the count
            rate and rate covariance to the measurement time.

        Returns
        -------
        timestamp : float
            The timestamp of the filter output. This timestamp will be
            `n` + 1 measurements in the past.
        mean : array_like
            The predicted mean of the spectrum, a 1D array.
        covariance_matrix : array_like
            The predicted covariance of the spectrum, a 2D array.
        """
        if len(self.buffer.spectra) < self.n:
            raise Exception(
                "Cannot predict until buffer contains at least {} measurements".format(
                    self.n
                )
            )
        n_window = (self.n - 1) // 2
        i_median = n_window
        ts_median = self.buffer.timestamps[i_median]
        # select the data in the windows
        windows = np.ones(self.n, dtype=bool)
        windows[i_median] = False
        # calculate sample mean and covariance from buffer
        _, median, cov = _count_rate_sample_mean_median_cov(
            self.buffer.spectra[windows],
            self.buffer.live_times[windows],
        )
        assert median.shape == self.buffer.spectra[0].shape
        # scale to the measurement live time
        median *= live_time
        cov *= live_time**2
        return ts_median, median, cov
