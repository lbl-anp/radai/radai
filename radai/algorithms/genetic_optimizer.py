"""Tools to use genetic algorithms for optimization."""

import abc
import time
import numpy as np
import deap.algorithms
import deap.base
import deap.creator
import deap.tools


class GeneticOptimizer(object):
    """Optimize a scalar figure of merit using a genetic algorithm."""

    def __init__(
        self,
        alg,
        spectra,
        spectra_ids,
        src_ids=None,
        srcs_required=True,
        max_step=1,
        popsize=50,
        ngen=50,
        mutation_rate=0.5,
        crossover_rate=0.5,
        savefig=None,
        verbose=False,
    ):
        """Initialize the optimizer.

        Parameters
        ----------
        alg : Algorithm instance
            An initialized algorithm. This object carries along some
            of the settings we will need for the individuals in the population.
        spectra : array_like
            Training gamma-ray spectra (num_measurements, num_bins).
        spectra_ids : array_like
            Labels for the training data.
        src_ids : array_like, optional
            The integer labels for the sources to optimize for. By default,
            use any IDs in `spectra_ids` that are not 0.
        srcs_required : bool
            Whether to require any sources being labeled to have a valid
            optimization. By default sources are required.
        max_step : int, optional
            Maximum random change. By default 1.
        popsize : int, optional
            Number of individuals in the population. By default 50.
        ngen : int, optional
            Number of generations. By default 50.
        mutation_rate : float, optional
            The rate of mutations used by DEAP. By default 0.5.
        crossover_rate : float, optional
            The rate of crossover used by DEAP. By default 0.5.
        savefig : str, optional
            Filename to save figure of merit plot during fit().
            By default None.
        verbose : bool, optional
            Whether to print out statistics as the optimization runs.
            By default False.
        """
        self.alg = alg
        self.spectra = np.asarray(spectra)
        self.spectra_ids = np.asarray(spectra_ids)
        self.n_meas, self.n_bins = spectra.shape
        assert self.n_bins >= 2
        assert self.n_meas == len(self.spectra_ids)

        # make sure background and source IDs are consistent
        all_ids = sorted(np.unique(self.spectra_ids))
        assert 0 in all_ids
        self.src_ids = src_ids
        if self.src_ids is None:
            self.src_ids = sorted(
                [src_id for src_id in np.unique(self.spectra_ids) if src_id != 0]
            )
        for src_id in self.src_ids:
            assert src_id in all_ids
            assert src_id != 0
        self.is_bkg = self.spectra_ids == 0
        self.is_src = np.zeros_like(self.spectra_ids, dtype=bool)
        for src_id in self.src_ids:
            self.is_src |= self.spectra_ids == src_id
        assert np.any(self.is_bkg), "At least one spectrum must be labeled background"
        if srcs_required:
            assert np.any(self.is_src), "At least one spectrum must be labeled source"

        # find background and source spectra
        self.bkg_spectra = self.spectra[self.is_bkg]
        self.src_spectra = self.spectra[self.is_src]

        # calculate mean shape of background and source
        self.bkg_spectrum_mean = self.bkg_spectra.mean(axis=0)
        self.src_spectrum_mean = self.src_spectra.mean(axis=0)

        # genetic algorithm settings
        self.max_step = int(max_step)
        self.popsize = int(popsize)
        self.ngen = int(ngen)
        self.mutation_rate = float(mutation_rate)
        self.crossover_rate = float(crossover_rate)
        self.savefig = savefig
        self.verbose = verbose

        # set up DEAP creator
        self.creator = deap.creator
        self.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
        self.creator.create("Individual", np.ndarray, fitness=self.creator.FitnessMax)

        # set up DEAP toolbox
        self.toolbox = deap.base.Toolbox()
        self.toolbox.register("random_individual", self.random_individual)
        self.toolbox.register(
            "individual",
            deap.tools.initRepeat,
            self.creator.Individual,
            self.toolbox.random_individual,
            n=1,
        )

        # the 'population' consists of a list of such individuals
        self.toolbox.register(
            "population", deap.tools.initRepeat, list, self.toolbox.individual
        )
        self.toolbox.register("evaluate", self.figure_of_merit)
        self.toolbox.register("mate", self.crossover)
        self.toolbox.register("mutate", self.mutate)
        self.toolbox.register("select", deap.tools.selTournament, tournsize=3)

        # create an initial population, and initialize a hall-of-fame to store
        # the best individual
        self.pop = self.toolbox.population(n=self.popsize)
        self.hof = deap.tools.HallOfFame(1, similar=np.allclose)

        # print summary statistics for the population on each iteration
        self.stats = deap.tools.Statistics(lambda ind: ind.fitness.values)
        self.stats.register("min", np.min)
        self.stats.register("mean", np.mean)
        self.stats.register("median", np.median)
        self.stats.register("std", np.std)
        self.stats.register("max", np.max)
        self.ts = []
        self.foms = []

    @abc.abstractmethod
    def random_individual(self):
        """Create a random individual."""
        pass

    @abc.abstractmethod
    def figure_of_merit(self, individual):
        """Calculate a figure of merit for the individual."""
        pass

    @abc.abstractmethod
    def mutate(self, individual):
        """Mutate the individual."""
        pass

    @abc.abstractmethod
    def crossover(self, ind1, ind2):
        """Randomly swap the contents of the individuals."""
        pass

    def run(self):
        """Run a simple genetic algorithm.

        Modified version of deap's eaSimple:
        https://github.com/DEAP/deap/blob/master/deap/algorithms.py#L85

        Returns
        -------
        best_attribute : array_like
            Attribute of the best individual (e.g., spectral windows).
        log : deap.tools.Logbook
            Log of the optimization data.
        """
        self.log = deap.tools.Logbook()
        self.log.header = ["gen", "nevals"] + self.stats.fields

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in self.pop if not ind.fitness.valid]
        fitnesses = self.toolbox.map(self.toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        self.hof.update(self.pop)

        record = self.stats.compile(self.pop)
        self.log.record(gen=0, nevals=len(invalid_ind), **record)
        if self.verbose:
            print(self.log.stream)

        # Begin the generational process
        for gen in range(1, self.ngen + 1):
            # Select the next generation individuals
            offspring = self.toolbox.select(self.pop, len(self.pop))

            # Vary the pool of individuals
            offspring = deap.algorithms.varAnd(
                offspring,
                self.toolbox,
                cxpb=self.crossover_rate,
                mutpb=self.mutation_rate,
            )

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = self.toolbox.map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            # Update the hall of fame with the generated individuals
            self.hof.update(offspring)

            # Replace the current population by the offspring
            self.pop[:] = offspring

            # Append the current generation statistics to the logbook
            record = self.stats.compile(self.pop)
            self.log.record(gen=gen, nevals=len(invalid_ind), **record)
            self.ts.append(time.time())
            self.foms.append(record["max"])
            if self.verbose:
                print(self.log.stream)
            if gen % 10 == 0 and self.savefig:
                import matplotlib.pyplot as plt

                plt.figure(figsize=(10, 3))
                plt.plot(self.foms, ".-", ms=2, lw=0.5)
                plt.xlabel("Generation")
                plt.ylabel("Maximum figure of merit")
                plt.tight_layout()
                plt.savefig(self.savefig)
                plt.close()

        return self.hof[0], self.log
