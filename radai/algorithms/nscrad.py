"""N-SCRAD algorithm."""

import copy
import random
import numpy as np
from scipy.stats import chi2
from .algorithm import Algorithm
from ..result import Result, Results
from ..filters import EWMAFilter
from ..nmf import train_bkg_sources
from ..tools import fpr_from_far
from .nscrad_optimizer import NSCRADOptimizer


MAX_MATRIX_CONDITION = 1e12


def nearest_edge_index(bin_edges, x):
    """Convert the value to the index of the nearest bin edge.

    Parameters
    ----------
    bin_edges : array_like
        The edges of the spectral bins.
    x : float
        The value of concern.

    Returns
    -------
    idx : integer
        The index of the bin edge that x is closest to.

    Raises
    ------
    AssertionError
        If x is outside the bin edges.
    """
    assert bin_edges[0] <= x
    assert x <= bin_edges[-1]
    diff = np.abs(x - bin_edges)
    idx = np.arange(len(bin_edges), dtype=int)[diff == diff.min()][0]
    return idx


def validate_spectral_windows(bin_edges, *windows):
    """Raise AssertionError if the spectral windows are not properly formed.

    Parameters
    ----------
    bin_edges : array_like
        The edges of the spectral bins.
    windows : iterable
        Each window is a length-2 iterable giving the window boundaries.

    Raises
    ------
    AssertionError
        If windows are not properly formed.
    """
    for window in windows:
        try:
            assert len(window) == 2
        except TypeError:
            raise AssertionError("Windows must be an iterable")
        assert window[1] >= window[0]
        assert bin_edges[0] <= window[0]
        assert window[1] <= bin_edges[-1]


def spectral_windows_to_bool(bin_edges, *windows):
    """Convert the spectral windows to an array that is True in the windows.

    Parameters
    ----------
    bin_edges : array_like
        The edges of the spectral bins.
    windows : iterable
        Each window is a length-2 iterable giving the window boundaries.

    Returns
    -------
    arr : array_like
        Array of booleans that is True inside the windows given.
    """
    assert len(bin_edges) >= 2
    assert (np.diff(bin_edges) > 0).all()
    validate_spectral_windows(bin_edges, *windows)
    arr = np.zeros(len(bin_edges) - 1, dtype=bool)
    for window in windows:
        arr |= (bin_edges[:-1] < window[1]) & (window[0] < bin_edges[1:])
    return arr


def rebinning_matrix_from_windows(bin_edges, *windows):
    """Create a matrix that rebins a spectrum into the spectral windows.

    Parameters
    ----------
    bin_edges : array_like
        The edges of the spectral bins.
    windows : iterable
        Each window is a length-2 iterable giving the window boundaries.

    Returns
    -------
    matrix : array_like
        2D array of dimensions (n_windows, n_bins). Entries are 0 or 1.
        When dotted with a spectrum of the given bin edges, returns a spectrum
        binned into the windows.
    """
    assert len(bin_edges) >= 2
    assert (np.diff(bin_edges) > 0).all()
    validate_spectral_windows(bin_edges, *windows)
    matrix = np.zeros((len(windows), len(bin_edges) - 1))
    for j, window in enumerate(windows):
        arr = spectral_windows_to_bool(bin_edges, window)
        matrix[j, arr] = 1.0
    return matrix


def validate_rebinning_matrix(bin_edges, matrix):
    """Raise AssertionError if the rebinning matrix is degenerate in any way.

    Parameters
    ----------
    bin_edges : array_like
        The edges of the spectral bins.
    matrix : array_like
        2D array of dimensions (n_windows, n_bins). Entries are 0 or 1.

    Raises
    ------
    AssertionError
        If matrix is not properly formed.
    """
    assert len(matrix.shape) == 2
    assert matrix.shape[0] >= 1
    assert matrix.shape[1] == len(bin_edges) - 1
    for j0 in range(matrix.shape[0]):
        if not (np.isclose(matrix[j0], 0) | np.isclose(matrix[j0], 1)).all():
            raise AssertionError(
                "Matrix row {} contains elements other than 0 and 1".format(j0)
            )
        if not (matrix[j0] > 0).any():
            raise AssertionError("Matrix row {} is all zeros".format(j0))
        for j1 in range(j0 + 1, matrix.shape[0]):
            if np.allclose(matrix[j0], matrix[j1]):
                raise AssertionError(
                    "Matrix rows {} and {} are identical".format(j0, j1)
                )


class NSCRAD(Algorithm):
    """Nuisance-rejection Spectral Comparison Ratio Anomaly Detection.

    References:

    [1] D. M. Pfund, R. C. Runkle, K. K. Anderson and K. D. Jarman,
        "Examination of Count-Starved Gamma Spectra Using the Method of
        Spectral Comparison Ratios," in IEEE Transactions on Nuclear Science,
        vol. 54, no. 4, pp. 1232-1238, Aug. 2007.
        https://doi.org/10.1109/TNS.2007.901202

    [2] R. C. Runkle, et al., "Unattended sensors for nuclear threat
        detection", Proc. SPIE 6954, Chemical, Biological, Radiological,
        Nuclear, and Explosives (CBRNE) Sensing IX, 69541A (17 April 2008).
        https://doi.org/10.1117/12.776399

    [3] K. K. Anderson, et al., "Discriminating nuclear threats from benign
        sources in gamma-ray spectra using a spectral comparison ratio method."
        J. Radioanal. Nucl. Chem. 276, 713–718 (2008).
        https://doi.org/10.1007/s10967-008-0622-x

    [4] K. D. Jarman, et al., "A comparison of simple algorithms for gamma-ray
        spectrometers in radioactive source search applications," Applied
        Radiation and Isotopes, Volume 66, Issue 3, 2008, pp. 362-371,
        https://doi.org/10.1016/j.apradiso.2007.09.010

    [5] D. M. Pfund, et al., "Low Count Anomaly Detection at Large Standoff
        Distances," in IEEE Transactions on Nuclear Science, vol. 57, no. 1,
        pp. 309-316, Feb. 2010.
        https://doi.org/10.1109/TNS.2009.2035805

    [6] R.S. Detwiler, et al., "Spectral anomaly methods for aerial detection
        using KUT nuisance rejection," Nuclear Instruments and Methods in
        Physics Research Section A: Accelerators, Spectrometers, Detectors and
        Associated Equipment, Volume 784, 2015, pp. 339-345.
        https://doi.org/10.1016/j.nima.2015.01.040

    [7] D. M. Pfund, et al., "Improvements in the method of radiation anomaly
        detection by spectral comparison ratios," Applied Radiation and
        Isotopes, Volume 110, 2016, pp. 174-182.
        https://doi.org/10.1016/j.apradiso.2015.12.063

    [8] D. M. Pfund, "Radiation anomaly detection and classification with
        Bayes Model Selection," Nuclear Instruments and Methods in Physics
        Research Section A: Accelerators, Spectrometers, Detectors and
        Associated Equipment, Volume 904, 2018, pp. 188-194.
        https://doi.org/10.1016/j.nima.2018.07.047
    """

    _required_config_vars = [
        "far",
        "int_time",
        "lam",
        "bin_edges",
        "spectral_windows",
        "nuisance_spectra",
    ]
    _optimizer_class = NSCRADOptimizer

    def __init__(
        self,
        far=1.0 / 8.0,
        int_time=1.0,
        lam=0.001,
        bin_edges=None,
        spectral_windows=None,
        nuisance_spectra=None,
    ):
        """Initialize the algorithm.

        Parameters
        ----------
        far : float, optional
            False alarm rate in [alarms per hour]. By default 1.0 / 8.0.
        int_time : float, optional
            Mean integration time per measurement. By default 1.0.
        lam : float, optional
            The EWMA `lambda` parameter for the background estimator.
            By default 1e-3.
        bin_edges : array_like, optional
            The edges of the spectral bins. By default an empty list.
        spectral_windows : array_like, optional
            A list of length-2 lists giving desired coarse bin boundaries.
            By default an empty list.
        nuisance_spectra : array_like, optional
            A list of any nuisance spectra that will be projected out so the
            algorithm is not sensitive to changes in them. Length of each
            nuisance is len(bin_edges) - 1.  By default an empty ndarray of
            shape (0, len(bin_edges) - 1).
        """

        # Check and set up the spectral windows and nuisances
        bin_edges = [] if bin_edges is None else bin_edges
        bin_edges = np.asarray(bin_edges)
        assert len(bin_edges) >= 2
        self.bin_edges = bin_edges

        # Instantiate base functionality
        super().__init__(
            spectra_buffer_num_energy=len(bin_edges) - 1,
            far=far,
            int_time=int_time,
            lam=lam,
        )

        nuisance_spectra = [] if nuisance_spectra is None else nuisance_spectra
        if len(nuisance_spectra) > 0:
            nuisance_spectra = np.asarray(nuisance_spectra)
        else:
            nuisance_spectra = np.empty((0, len(bin_edges) - 1))
        self.nuisance_spectra = self._ensure_array_of_spectra(nuisance_spectra)

        # validate the windows and calculate the rebinning matrix
        self._set_rebinning(spectral_windows)

        # start the background estimator
        self.filter = EWMAFilter(lam=lam)

    def _set_rebinning(self, windows):
        """Set the spectral windows and recalculate the rebinning matrix.

        Parameters
        ----------
        spectral_windows : array_like
            A list of length-2 lists giving desired coarse bin boundaries.
            If None will default to an empty list.
        """
        windows = [] if windows is None else windows
        windows = np.asarray(windows)
        assert len(windows.shape) == 2
        assert windows.shape[0] >= 1
        assert windows.shape[1] == 2
        validate_spectral_windows(windows)
        self.spectral_windows = windows

        # sort windows by their central energy
        win_centers = np.array(
            [0.5 * (win[0] + win[1]) for win in self.spectral_windows]
        )
        idx_sort = np.argsort(win_centers)
        windows = []
        for j in idx_sort:
            windows.append(self.spectral_windows[j])
        validate_spectral_windows(windows)
        self.spectral_windows = windows

        # calculate the rebinning matrix and rebinned nuisances
        self.rebinning_matrix = rebinning_matrix_from_windows(
            self.bin_edges, *self.spectral_windows
        )
        validate_rebinning_matrix(self.bin_edges, self.rebinning_matrix)
        self.rebinned_nuisances = self.nuisance_spectra @ self.rebinning_matrix.T

        # calculate degrees of freedom of the statistic
        self.dof = self.rebinning_matrix.shape[0] - 1
        self.dof -= self.rebinned_nuisances.shape[0]
        assert self.dof >= 1

        # Set the threshold based on the FAR and int_time
        # (need to calculate threshold here in case the number of windows
        # or nuisances changes after instantiation)
        self.calculate_threshold(set_threshold=True)

    def _ensure_array_of_spectra(self, arr):
        """Ensure that the array is a list or array of spectra.

        Parameters
        ----------
        arr : array_like
            A list or numpy array of spectra with bins matching `bin_edges`.

        Raises
        ------
        AssertionError
            If the spectra do not match the number of bins.

        Returns
        -------
        arr : array_like
            Numpy array version of the array
        """
        arr = np.asarray(arr)
        assert len(arr.shape) == 2
        assert arr.shape[1] == len(self.bin_edges) - 1
        return arr

    def calculate_threshold(self, set_threshold=True):
        """Compute threshold far and int_time from algorithm state.

        Parameters
        ----------
        set_threshold : bool, optional
            Set threshold in config if True. By default True.

        Returns
        -------
        threshold : float
            Threshold for the N-SCRAD metric (squared Mahalanobis distance)
            given the degrees of freedom and the FAR.
        """
        fpr = fpr_from_far(far=self.far, int_time=self.int_time)
        threshold = chi2.isf(fpr, self.dof)
        if set_threshold:
            self.threshold = threshold
        return threshold

    def _transformation_matrix(self, B_k):
        """Calculate the SCR transformation matrix given the background.

        Parameters
        ----------
        B_k : array_like
            The current background estimate in coarse bins.

        Returns
        -------
        T_k : array_like
            Matrix giving the transformation from coarse spectral bins to
            Spectral Comparison Ratios (SCRs).
        """
        B_k = np.asarray(B_k)
        assert len(B_k.shape) == 1
        assert B_k.shape[0] == self.rebinning_matrix.shape[0]
        assert (B_k > 0).all()

        # calculate the transformation matrix - Pfund 2010 (3)
        T_k = np.diag(-B_k[0] / B_k)
        T_k[:, 0] = 1.0
        T_k = T_k[1:]
        return T_k

    def _spectra_to_scr_matrix(self, T_k, spectra):
        """Generate a matrix of SCRs for the given spectra.

        Parameters
        ----------
        T_k : array_like
            The SCR transformation matrix.
        spectra : array_like
            A list of spectra to be converted to SCRs.

        Returns
        -------
        A_k : array_like
            Matrix of shape (n_bins - 1, n_spectra) of SCRs of the spectra.
        """
        # check inputs
        T_k = np.asarray(T_k)
        n = self.rebinning_matrix.shape[0]
        assert T_k.shape == (n - 1, n)
        assert len(spectra) > 0
        spectra = self._ensure_array_of_spectra(spectra)

        # rebin and transform the spectra
        R = self.rebinning_matrix
        A_k = T_k @ R @ spectra.T
        return A_k

    @staticmethod
    def _projection_matrix(W, A):
        """Calculate the projection matrix for subspace A and weight matrix W.

        That is, minimize the figure of merit for the weighted least squares
        problem:
            f(x) = (y - A @ x).T @ W @ (y - A @ x)

        The solution that minimizes f is
            x = inv(A.T @ W @ A) @ A.T @ W @ y

        The projection of y onto the subspace spanned by A is therefore
            P @ y = A @ x = A @ inv(A.T @ W @ A) @ A.T @ W @ y

        So the projection matrix is
            P = A @ inv(A.T @ W @ A) @ A.T @ W

        Parameters
        ----------
        W : array_like
            A symmetric positive definite matrix, e.g., the inverse of a
            covariance matrix.
        A : array_like
            A matrix whose columns are the bases of the subspace.

        Returns
        -------
        P : array_like
            Matrix giving the projection of any vector onto the space spanned
            by A subject to weights W..
        """
        W = np.asarray(W)
        assert len(W.shape) == 2
        assert W.shape[0] == W.shape[1]
        assert np.all(np.linalg.eigvals(W) > 0)

        A = np.asarray(A)
        assert len(A.shape) == 2
        assert W.shape[1] == A.shape[0]
        assert A.shape[1] >= 1

        P = A @ np.linalg.inv(A.T @ W @ A) @ A.T @ W
        return P

    def _analyze(self) -> Results:
        """Analyze a single measurement.

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the square of the
            Mahalanobis distance of spectrum's spectral comparison ratios.
        """

        spectrum, timestamp, live_time = self.spectra_buffer[-1]
        if np.isnan(live_time):
            live_time = self.int_time

        # bin the spectrum into coarse bins
        R = self.rebinning_matrix
        C_k = R @ spectrum

        # get a background prediction from the EWMA filter
        # This filter follows Pfund 2010 (4) and (6) in tracking the mean and
        # covariance of the coarsely-binned spectrum (B_k)
        try:
            B_k, Sigma_k = self.filter.predict(live_time)
        except AssertionError:
            self.filter.update(C_k, timestamp, live_time)
            B_k, Sigma_k = self.filter.predict(live_time)

        # calculate the transformation matrix
        T_k = self._transformation_matrix(B_k)

        # calculate spectral comparison ratios - Pfund 2010 (2)
        alpha_k = T_k @ C_k

        # A note about notation:
        # In Pfund 2016 (4) "Sigma_tracking_k" is used as notation
        # for the "estimated background covariance matrix" from the filter.
        # In this context that quantity is the covariance of the spectral
        # comparison ratios (SCRs), not the tracked coarse background counts
        # (usually denoted "B").
        # By contrast, Pfund 2010 (6) and Pfund 2007 (15) use "Sigma_k"
        # for the covariance matrix of the tracked coarse background counts
        # and "S_k" for the covariance matrix of the SCRs.
        # Here we will use the latter notation, that "Sigma" is the
        # covariance matrix for the background and "S" the covariance matrix
        # for the SCRs, and since the SCRs are a linear transformation (T_k)
        # from the coarse background, the transformation from Sigma to S
        # is as follows (Pfund 2016 (8)):
        S_k = T_k @ Sigma_k @ T_k.T

        # In Pfund 2016 around (5)--(9) they discuss how S_k (there called
        # Sigma_tracking_k, see above) can underestimate the SCR covariance,
        # and show that by scaling the background estimate by a factor that
        # accounts for possible increases in gross counts the algorithm's
        # FAR can be improved.
        # The scale factor is the ratio of the "current observed total count
        # rate" (C_k) to "the estimated total count rate extrapolated
        # from the past to the present by EWMA" (B_k), but these quantities
        # are not the C_k and B_k used in this code since those are the
        # coarse counts current measured (C_k) versus tracked by EWMA (B_k).
        # Nevertheless, following the logic of (5)--(7), if we assume that
        # C_k is proportional to B_k by some factor, then we arrive at
        # the scaling being the ratio of sum(C_k) to sum(B_k).
        # Here is the implementation of that scaling:
        scale_k = sum(C_k) / sum(B_k)
        r_k = min(1 / scale_k, 1)
        S_inv_k = r_k * np.linalg.inv(S_k)

        # A case not covered in the papers is if the matrix S_k is not
        # invertible (i.e., positive definite), which can occur in practice.
        # If this occurs, we will use an estimate of the Poisson uncertainty
        # from the current measurement, clipping zero counts to 1.
        if not np.all(np.linalg.eigvals(S_k) > 0):
            print("Warning: estimated covariance matrix not invertible")
            S_k = (T_k @ R) @ np.diag(spectrum.clip(1)) @ (T_k @ R).T
            S_inv_k = np.linalg.inv(S_k)
        assert np.all(np.linalg.eigvals(S_k) > 0)

        # Calculate nuisance rejection transformation from
        # Pfund 2010 (9) / Pfund 2007 (7).
        # This is an orthogonal projection onto the linear space spanned by
        # the nuisance spectra, weighted by the inverse of the covariance:
        I = np.eye(len(C_k) - 1)
        P_k = np.zeros_like(I)
        if self.nuisance_spectra.shape[0] >= 1:
            A_k = self._spectra_to_scr_matrix(T_k, self.nuisance_spectra)
            P_k = self._projection_matrix(S_inv_k, A_k)

        # calculate N-SCRAD statistic (Mahalanobis distance^2)- Pfund 2010 (7)
        alarm_metric = alpha_k.T @ S_inv_k @ (I - P_k) @ alpha_k

        return Results(
            timestamp,
            [
                Result(
                    origin="NSCRAD",
                    source_id=1,
                    is_alarm=alarm_metric > self.threshold,
                    alarm_metric=alarm_metric,
                    alarm_metric_name="mahalanobis-distance-squared",
                    timestamp_start=timestamp - self.int_time,
                    timestamp_stop=timestamp,
                )
            ],
        )

    def _update_state(self, results: Results) -> None:

        # Get latest data
        spectrum, timestamp, live_time = self.spectra_buffer[-1]

        # Bin the spectrum into coarse bins like above
        R = self.rebinning_matrix
        C_k = R @ spectrum

        # Update filter with the rebinned spectrum unless the current
        # measurement is anomalous as noted in Pfund 2010 before (6)
        if results[0].alarm_metric < self.threshold:
            self.filter.update(C_k, timestamp, live_time)

    def to_dict(self) -> dict:
        return dict(
            far=self.far,
            int_time=self.int_time,
            lam=self.lam,
            bin_edges=self.bin_edges,
            spectral_windows=self.spectral_windows,
            nuisance_spectra=self.nuisance_spectra,
        )

    def is_ready(self) -> bool:
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case the algorithm has the parameters needed to analyze. These
        could have been provided by a user or from training.
        """
        return self._all_ready(
            "far",
            "int_time",
            "lam",
            "bin_edges",
            "spectral_windows",
            "nuisance_spectra",
            "rebinning_matrix",
            "dof",
            "threshold",
        )

    def _figure_of_merit(self, bkg_spectra, src_templates):
        """Calculate the figure of merit to use when optimizing windows.

        The figure of merit is given in Pfund 2007 (16), Pfund 2010 (14),
        and Pfund 2016 (3):

            FOM = trace(A.T @ inv(S) @ (I - P) @ A)

        Here A is a matrix whose columns are the SCRs of the sources of
        interest, S is a covariance matrix, and P is the projection onto
        the SCRs of the nuisances. The sources of interest will be normalized
        to a total counts of 1000 each, as is done in Pfund 2007 and 2010.

        During the evaluation of the algorithm, the matrix S is estimated
        using a moving filter. It was not immediately obvious how S should
        be calculated. The description in Pfund 2016 is thus:

            "The covariance [S] was estimated from measured background
            spectra..."

        and in Pfund 2010:

            "[T]he covariance matrix, S, and projection, P, were evaluated
            using the aggregate of the [training data] and so had no time or
            location dependence."

        Pfund 2007 provides more information:

            If the reference spectrum is chosen so that [background count
            estimators] are unbiased estimators of the [background counts]...
            then the SCRs will have a mean over the [training data] of zero.
            This is the case if one uses the mean spectrum over the [training
            data] as the reference spectrum and if the spectra... are in the
            [training data].... The SCR points for spectra in the [training
            data] were well described by multivariate normal distributions...
            where S was the covariance matrix for SCRs in the [training data].

        This seems to indicate that we first calculate the mean background for
        the training data:

            B_bar = (1/n) sum(B_i),

        then use this mean to create the SCR transformation matrix T, and then
        transform all of the training data to SCRs using T:

            alpha_i = T @ B_i

        This definition ensures that the mean of the SCRs is the zero vector.
        The covariance matrix S is therefore

            cov[alpha] = Alpha.T @ Alpha / (n - 1)

        where Alpha is the matrix whose rows are the alpha_i's.

        Parameters
        ----------
        bkg_spectra : array_like
            The training set of background data.
        src_templates : array_like
            The threat spectra to optimize for.

        Returns
        -------
        fom : float
            The N-SCRAD figure of merit.
        """
        # calculate the mean background spectrum
        bkg_spectra = self._ensure_array_of_spectra(bkg_spectra)
        bkg_mean = bkg_spectra.mean(axis=0)

        # calculate the SCR transformation matrix
        R = self.rebinning_matrix
        B_bar = R @ bkg_mean
        T = self._transformation_matrix(B_bar)

        # reject these windows if the mean counts in any window becomes
        # too small. From Pfund 2016, the counts in each window must be
        # "greater than about 30."
        # To ensure this criterion is satisfied for the full range of count
        # rates, we scale the mean spectrum by the minimum gross counts
        # and check against that.
        gross = bkg_spectra.sum(axis=1)
        bkg_min = bkg_mean * gross.min() / bkg_mean.sum()
        B_min = R @ bkg_min
        assert (B_min >= 30.0).all()

        # calculate the covariance of the background SCRs
        alphas = T @ R @ bkg_spectra.T
        assert np.allclose(alphas.mean(axis=1), 0)
        S = alphas @ alphas.T / (bkg_spectra.shape[0] - 1)
        S_inv = np.linalg.inv(S)

        # check if S is invertible but matrix condition number too large
        # this rejects matrices that are close to being singular
        if np.linalg.cond(S) > MAX_MATRIX_CONDITION:
            raise np.linalg.LinAlgError("Condition value of S too large")

        # calculate projection onto the space spanned by the nuisances
        I = np.eye(len(B_bar) - 1)
        P = np.zeros_like(I)
        if self.nuisance_spectra.shape[0] >= 1:
            A_n = self._spectra_to_scr_matrix(T, self.nuisance_spectra)
            P = self._projection_matrix(S_inv, A_n)

        # normalize all source spectra to 1000 counts
        src_templates = self._ensure_array_of_spectra(src_templates)
        src_templates = src_templates / src_templates.sum(axis=1)[:, None]
        src_templates *= 1000
        A_t = self._spectra_to_scr_matrix(T, src_templates)

        # calculate the FOM
        fom = np.trace(A_t.T @ S_inv @ (I - P) @ A_t)
        return fom

    def _set_random_binning(self, n_windows):
        """Generate random windows that give a valid rebinning matrix.

        Parameters
        ----------
        n_windows : int
            Number of spectral windows to generate.

        Returns
        -------
        windows : array_like
            A list of length-2 lists giving desired coarse bin boundaries.
        """
        # generate random windows until we have a valid rebinning matrix
        windows = []
        while len(windows) < n_windows:
            e0, e1 = sorted(np.random.choice(self.bin_edges[:-1], size=2))
            windows.append([e0, e1])
            R = rebinning_matrix_from_windows(self.bin_edges, *windows)
            try:
                validate_rebinning_matrix(self.bin_edges, R)
            except AssertionError:
                windows = windows[:-1]
        self._set_rebinning(windows)
        return windows

    def _randomly_perturb_binning(self, max_step):
        """Randomly adjust the spectral windows.

        Parameters
        ----------
        max_step : int
            Maximum number of bins to shift by.
        """
        windows = copy.deepcopy(self.spectral_windows)
        while True:
            self._set_rebinning(windows)
            # choose which window and which edge
            win = random.randint(0, len(windows) - 1)
            edge = random.randint(0, 1)
            # determine the bin edge index
            x = windows[win][edge]
            idx = nearest_edge_index(self.bin_edges, x)
            # determine lower edge index (inclusive)
            if edge == 0:
                min_idx = 0
            else:
                min_idx = nearest_edge_index(self.bin_edges, windows[win][0]) + 1
            # determine upper edge index (inclusive)
            if edge == 0:
                max_idx = nearest_edge_index(self.bin_edges, windows[win][1]) - 1
            else:
                max_idx = len(self.bin_edges) - 1
            # adjust the bin edge
            min_idx = max(idx - max_step, min_idx)
            max_idx = min(idx + max_step, max_idx)
            idx = random.randint(min_idx, max_idx)
            self.spectral_windows[win][edge] = self.bin_edges[idx]
            # validate the windows and rebinning matrix
            try:
                self._set_rebinning(self.spectral_windows)
            except AssertionError:
                continue
            # make sure the windows are different from the originals
            if np.allclose(windows, self.spectral_windows):
                continue
            break
        self._set_rebinning(self.spectral_windows)

    def fit_known_sources(
        self, spectra, spectra_ids, src_templates, n_windows=None, **kwargs
    ):
        """Compute N-SCRAD windows for the given sources and nuisances.

        In this version of `fit`, we assume that we know the source spectra
        (`src_spectra`), and we assume that we want all of them grouped
        together into one statistic.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_templates : array_like
            The source spectra. The length of each template must be
            len(bin_edges) - 1.
        n_windows : int, optional
            Number of spectral windows to optimize. No guidance appears to be
            given in the literature on how to choose the number of windows,
            but must be at least two more than the number of nuisances.
            By default 2 + len(self.nuisance_spectra).
        kwargs:
            Kwargs sent to the NSCRADOptimizer.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        if n_windows is None:
            n_windows = 2 + len(self.nuisance_spectra)

        bkg_spectra = spectra[spectra_ids == 0]
        src_templates = self._ensure_array_of_spectra(src_templates)

        # initialize with random windows
        while True:
            # generate random windows and make sure FOM is valid
            self._set_random_binning(n_windows)
            try:
                self._figure_of_merit(bkg_spectra, src_templates)
            except (AssertionError, np.linalg.LinAlgError):
                continue
            break

        # set up and run the optimizer
        opt = self._optimizer_class(
            self,
            spectra,
            spectra_ids,
            src_templates=src_templates,
            srcs_required=False,
            **kwargs
        )
        best_windows, best_fom = opt.run()
        self._set_rebinning(best_windows)

        # save data produced during training
        train_info = {}
        train_info["ts"] = opt.ts
        train_info["foms"] = opt.foms
        train_info["bkg_spectrum"] = bkg_spectra.mean(axis=0)
        train_info["src_templates"] = src_templates
        return train_info

    def fit(
        self,
        spectra,
        spectra_ids,
        src_ids=None,
        n_bkg=1,
        eps=1e-6,
        sparsity=0.0,
        verbose=False,
        n_windows=None,
        **kwargs
    ):
        """Compute N-SCRAD windows for the given sources and nuisances.

        In this version of `fit`, we assume that we do not know the source
        spectral templates but we need to discover them from the labeled
        training data. We also assume that all of the specified sources
        will be grouped together into one statistic.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources. By default, use any IDs in
            `spectra_ids` that are not 0.
        n_bkg : int, optional
            The number of NMF components to use for the background.
            By default 1.
        eps : float, optional
            `eps` kwarg sent to nmf.train_bkg_sources. By default 1e-6.
        sparsity : float, optional
            `sparsity` kwarg sent to nmf.train_bkg_sources. By default 0.
        verbose : bool, optional
            `verbose` kwarg sent to nmf.train_bkg_sources. By default False.
        n_windows : int, optional
            Number of spectral windows to optimize. No guidance appears to be
            given in the literature on how to choose the number of windows,
            but must be at least two more than the number of nuisances.
            By default 2 + len(self.nuisance_spectra).
        kwargs:
            Kwargs sent to the optimizer.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # find a background model and single-component source templates
        V_bkg, V_src = train_bkg_sources(
            spectra,
            spectra_ids,
            src_ids=src_ids,
            n_bkg=n_bkg,
            n_src=1,
            eps=eps,
            sparsity=sparsity,
            verbose=verbose,
        )

        # send source templates to fit_known_sources()
        src_templates = []
        for src_id in V_src.keys():
            src_templates.append(V_src[src_id].flatten())
        src_templates = np.array(src_templates)
        train_info = self.fit_known_sources(
            spectra, spectra_ids, src_templates, n_windows=n_windows, **kwargs
        )

        # add some data to train_info
        train_info["V_bkg"] = V_bkg

        return train_info
