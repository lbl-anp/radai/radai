"""Multiple parallel Censored Energy Window (CEW) algorithms."""

import numpy as np
from .cew import CEW
from .multiplexed_anomaly_for_id import MultiplexedAnomalyForID
from ..nmf import train_bkg_sources


class MultiplexedCEW(MultiplexedAnomalyForID):
    """Multiple Censored Energy Window (CEW) algorithms run in parallel.

    This algorithm contains independently trained CEW algorithms to both
    detect and identify.
    """

    sub_alg_class = CEW

    def fit_known_sources(
        self,
        spectra,
        spectra_ids,
        src_spectra,
        src_ids=None,
        **kwargs,
    ):
        """Compute source and background weights for the given source.

        Will destroy any current sub-algorithms and create new ones.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_spectra : array_like
            The source spectra to identify. The absolute scale of the spectra
            will not affect the results of `fit`.
        src_ids : array_like, optional
            The integer labels for the sources. Must have the same length
            as src_spectra. By default range(1, len(src_spectra) + 1).
        kwargs : dict, optional
            Additional kwargs sent to the sub-algorithm fit_known_sources().

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n
        src_spectra = np.asarray(src_spectra)
        assert src_spectra.shape[1] == m

        src_ids = list(range(1, len(src_spectra) + 1)) if src_ids is None else src_ids
        assert len(src_ids) == len(src_spectra)

        # instantiate and train each of the sub-algorithms
        self.sub_alg = {}
        self.src_ids = []
        sub_dict = self.to_dict()
        train_info = {}
        for src_id, src_spectrum in zip(src_ids, src_spectra):
            self.sub_alg[src_id] = self.sub_alg_class(**sub_dict)
            train_info[src_id] = self.sub_alg[src_id].fit_known_sources(
                spectra,
                spectra_ids,
                src_spectrum,
                **kwargs,
            )
            self.src_ids.append(src_id)

        return train_info

    def fit_nmf(
        self,
        spectra,
        spectra_ids,
        src_ids=None,
        n_bkg=1,
        eps=1e-6,
        sparsity=1e-6,
        verbose=False,
        alpha=1.0,
        fit_intercept=True,
        **kwargs,
    ):
        """Compute background templates and one template per source before
        fitting with fit_known_sources().

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources to optimize for. By default,
            use any IDs in `spectra_ids` that are not 0.
        eps : float, optional
            `eps` kwarg sent to nmf.train_bkg_sources. By default 1e-6.
        sparsity : float, optional
            `sparsity` kwarg sent to nmf.train_bkg_sources. By default 1e-6.
        verbose : bool, optional
            `verbose` kwarg sent to nmf.train_bkg_sources. By default False.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.

        Other Parameters
        ----------------
        kwargs : dict, optional
            Additional kwargs sent to the sub-algorithm fit_known_sources().

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n

        all_ids = sorted(np.unique(spectra_ids))
        assert 0 in all_ids
        if src_ids is None:
            src_ids = sorted(
                [src_id for src_id in np.unique(spectra_ids) if src_id != 0]
            )
        assert len(src_ids) >= 1
        for src_id in src_ids:
            assert src_id in all_ids
            assert src_id != 0

        # find a background model and single-component source templates
        V_bkg, V_src = train_bkg_sources(
            spectra,
            spectra_ids,
            src_ids=src_ids,
            n_bkg=n_bkg,
            n_src=1,
            eps=eps,
            sparsity=sparsity,
            verbose=verbose,
        )

        # instantiate and train each of the sub-algorithms
        self.sub_alg = {}
        self.src_ids = []
        sub_dict = self.to_dict()
        train_info = {}
        # send source template to fit_known_sources()
        for src_id, src_template in V_src.items():
            self.sub_alg[src_id] = self.sub_alg_class(**sub_dict)
            train_info[src_id] = self.sub_alg[src_id].fit_known_sources(
                spectra,
                spectra_ids,
                src_template.ravel(),
                alpha=alpha,
                fit_intercept=fit_intercept,
                **kwargs,
            )
            self.src_ids.append(src_id)
        # include the bkg templates
        train_info["V_bkg"] = V_bkg
        return train_info
