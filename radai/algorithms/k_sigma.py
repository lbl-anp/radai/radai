"""Gross-counts (k-sigma)"""

from .algorithm import Algorithm
from ..result import Result, Results
from ..tools import fpr_from_far
from ..filters import make_filter, CountRateFilter, StaticFilter
from scipy.stats import norm
import numpy as np


def compute_threshold(far: float, int_time: float) -> float:
    """Compute threshold for given FAR and integration time.

    Parameters
    ----------
    far : float
        false alarm rate in [alarms per hour]
    int_time : float
        mean integration time per measurement

    Returns
    -------
    threshold
        negative log survival function of gaussian based on far and int_time.
    """
    fpr = fpr_from_far(far=far, int_time=int_time)
    threshold = -np.log(fpr)
    return threshold


class KSigma(Algorithm):
    def __init__(
        self,
        filter: CountRateFilter,
        far=1.0 / 8.0,
        filter_far=None,
        int_time=1.0,
        threshold=None,
        **kwargs
    ):
        """
        Parameters
        ----------
        filter : Filter
            Background filter
        far : float
            false alarm rate in [alarms per hour]
        filter_far : float
            false alarm rate in [alarms per hour] for whether the data
            should be used to update the filter. This FAR is generally
            going to be much larger than the far for the alarm. By default
            equal to `far`.
        int_time : float
            mean integration time per measurement
        threshold
            negative log survival function of gaussian based on bkg model, far,
            and int_time. By default None, in which case it will be calculated
            from far.
        """
        # Instantiate background filter
        filter = make_filter(filter)

        if filter_far is None:
            filter_far = far

        # Instantiate attributes
        super().__init__(
            far=far, filter_far=filter_far, int_time=int_time, filter=filter
        )

        # Set the threshold (analytic)
        self.threshold = threshold
        if self.threshold is None:
            self.calculate_threshold(set_threshold=True)

        # Set the threshold for the filter
        self.filter_threshold = compute_threshold(
            far=self.filter_far, int_time=self.int_time
        )

    def fit(self, spectra, live_times=None) -> None:
        """Compute background model from background only data.

        Mean and standard deviation of count-rates define the gaussian
        background model.

        Parameters
        ----------
        spectra : 2d array
            background gamma-ray data (num_measurements, num_bins)
        live_times : 1d array (optional)
            integration time for each measurement
        """
        if live_times is None:
            live_times = np.full(shape=spectra.shape[0], fill_value=self.int_time)
        # fit the filter parameters (only possible for StaticFilter)
        assert isinstance(self.filter, StaticFilter)
        self.filter = self.filter.from_data(spectra.sum(axis=1), live_times)

    def calculate_threshold(self, set_threshold: bool = True) -> float:
        """Compute threshold far and int_time from algorithm state.

        Parameters
        ----------
        set_threshold : bool
            Set threshold if True

        Returns
        -------
        threshold
            negative log survival function of gaussian based on bkg model, far,
            and int_time
        """
        threshold = compute_threshold(far=self.far, int_time=self.int_time)
        if set_threshold:
            self.threshold = threshold
        return threshold

    def _analyze(self) -> Results:
        """Analyze single measurement

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the negative log survival
            function of a Gaussian w/ bkg model.
        """

        spectrum, timestamp, live_time = self.spectra_buffer[-1]
        if np.isnan(live_time):
            live_time = self.int_time

        # check that the filter can predict, otherwise first call update
        try:
            self.filter.predict(live_time)
        except Exception:
            self.filter.update(spectrum.sum(), timestamp, live_time)
            self.filter.predict(live_time)

        alarm_metric = self._compute_alarm_metric(spectrum.sum(), timestamp, live_time)

        return Results(
            timestamp,
            [
                Result(
                    origin="KSigma",
                    source_id=1,
                    is_alarm=alarm_metric > self.threshold,
                    alarm_metric=alarm_metric,
                    alarm_metric_name="negative-log-survival",
                    timestamp_start=timestamp - self.int_time,
                    timestamp_stop=timestamp,
                )
            ],
        )

    def _update_state(self, results: Results) -> None:
        """Update the background filter from the SpectraBuffer ONLY IF the
        passed Results do not contain an alarm.

        Parameters
        ----------
        results : Results
        """
        # Update the background filter when the current data does not result in
        # an alarm.
        # The relevant "alarm" here is the filter alarm, which the user may wish
        # to have a different threshold than the algorithm.
        # If the result object is empty (len==0) then the filter is not ready, we
        # need to also update it. The data could be corrupted, but we don't have much
        # choice. It is up to the user to make sure the first n samples are background.
        if not len(results) or not results[0].alarm_metric > self.filter_threshold:
            spectrum, timestamp, live_time = self.spectra_buffer[-1]
            self.filter.update(spectrum.sum(), timestamp, live_time)

    def to_dict(self) -> dict:
        return dict(
            filter=self.filter.to_dict(),
            far=self.far,
            filter_far=self.filter_far,
            int_time=self.int_time,
            threshold=self.threshold,
        )

    def is_ready(self):
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case the algorithm has the parameters needed to analyze and it's
        filter has enough data. The parameters could have been provided by a
        user or from training.
        """
        return self._all_ready(
            "threshold", "far", "filter_far", "int_time"
        ) and self._all_ready(self.filter)

    # ---------------------------- Helper Methods -----------------------------
    def _compute_alarm_metric(self, counts, timestamp, live_time):
        """Negative log of the gaussian background model survival function"""
        self.check_ready()
        mean, var = self.filter.predict(live_time)
        return -norm.logsf(
            x=counts,
            loc=mean[0],
            scale=np.sqrt(max(mean[0], var[0, 0])),
        )
