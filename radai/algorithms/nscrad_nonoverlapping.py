"""N-SCRAD algorithm alternative with non-overlapping bins."""

import copy
import random
import numpy as np
from .nscrad import NSCRAD, nearest_edge_index


class NSCRADNonOverlapping(NSCRAD):
    """Nuisance-rejection Spectral Comparison Ratio Anomaly Detection.

    This version requires the coarse bins be non-overlapping.
    """

    def _set_rebinning(self, windows):
        """Set the spectral windows and recalculate the rebinning matrix.

        Parameters
        ----------
        spectral_windows : array_like
            Array of edges of the non-overlapping windows.
        """
        super()._set_rebinning(windows)
        # check that windows are non-overlapping
        windows = np.asarray(self.spectral_windows).flatten()
        assert np.all(np.diff(windows) >= 0)

    def _set_random_binning(self, n_windows):
        """Generate random windows that give a valid rebinning matrix.

        Parameters
        ----------
        n_windows : int
            Number of spectral windows to generate.

        Returns
        -------
        windows : array_like
            Array of edges of the non-overlapping windows.
        """
        edge_indices = np.arange(len(self.bin_edges), dtype=int)
        indices = np.random.choice(edge_indices, size=2 * n_windows, replace=False)
        indices = sorted(indices)
        windows = [
            [self.bin_edges[i0], self.bin_edges[i1]]
            for (i0, i1) in zip(indices[::2], indices[1::2])
        ]
        self._set_rebinning(windows)
        return windows

    def _randomly_perturb_binning(self, max_step):
        """Randomly adjust the spectral windows.

        Parameters
        ----------
        max_step : int
            Maximum number of bins to shift by.
        """
        windows = copy.deepcopy(self.spectral_windows)
        while True:
            self._set_rebinning(windows)
            # choose which window and which edge
            win = random.randint(0, len(windows) - 1)
            edge = random.randint(0, 1)
            # determine the bin edge index
            x = windows[win][edge]
            idx = nearest_edge_index(self.bin_edges, x)
            # determine lower edge index (inclusive)
            if edge == 0:
                if win == 0:
                    min_idx = 0
                else:
                    min_idx = nearest_edge_index(self.bin_edges, windows[win - 1][1])
            else:
                min_idx = nearest_edge_index(self.bin_edges, windows[win][0]) + 1
            # determine upper edge index (inclusive)
            if edge == 0:
                max_idx = nearest_edge_index(self.bin_edges, windows[win][1]) - 1
            else:
                if win == len(windows) - 1:
                    max_idx = len(self.bin_edges) - 1
                else:
                    max_idx = nearest_edge_index(self.bin_edges, windows[win + 1][0])
            # adjust the bin edge
            min_idx = max(idx - max_step, min_idx)
            max_idx = min(idx + max_step, max_idx)
            idx = random.randint(min_idx, max_idx)
            self.spectral_windows[win][edge] = self.bin_edges[idx]
            # validate the windows and rebinning matrix
            try:
                self._set_rebinning(self.spectral_windows)
            except AssertionError:
                continue
            # make sure the windows are different from the originals
            if np.allclose(windows, self.spectral_windows):
                continue
            break
        self._set_rebinning(self.spectral_windows)
