"""Experimental N-SCRAD algorithm that does not require known sources."""

import numpy as np
from .nscrad import NSCRAD, MAX_MATRIX_CONDITION
from .nscrad_optimizer import NSCRADOptimizer


class NSCRADExperimentalOptimizer(NSCRADOptimizer):
    """Optimize the N-SCRAD figure of merit using a genetic algorithm."""

    def figure_of_merit(self, individual):
        """Assign a figure of merit to each individual."""
        individual = individual.flatten()
        try:
            self._assign_individual_to_alg(individual)
            fom = self.alg._figure_of_merit(self)
            return (fom,)
        except (AssertionError, np.linalg.LinAlgError):
            return (-1,)


class NSCRADExperimental(NSCRAD):
    """Nuisance-rejection Spectral Comparison Ratio Anomaly Detection.

    This version uses spectral windows but attempts to train on labeled
    spectra instead of known spectral shapes.

    This version, as far as we can tell, is not entertained in the literature.
    """

    _optimizer_class = NSCRADExperimentalOptimizer

    def _figure_of_merit(self, optimizer):
        """Calculate the figure of merit to use when optimizing windows.

        This experimental version of the figure of merit uses only labeled
        training data, not known spectral shapes. It is an extension of tha
        FOM given in the literature.

        Parameters
        ----------
        optimizer : GeneticOptimizer
            Optimzer instance containing labled training data.

        Returns
        -------
        fom : float
            The N-SCRAD figure of merit.
        """
        assert isinstance(optimizer, NSCRADExperimentalOptimizer)

        # calculate the mean background spectrum
        bkg_mean = optimizer.bkg_spectra.mean(axis=0)

        # calculate the SCR transformation matrix
        R = self.rebinning_matrix
        B_bar = R @ bkg_mean
        T = self._transformation_matrix(B_bar)

        # reject these windows if the mean counts in any window becomes
        # too small. From Pfund 2016, the counts in each window must be
        # "greater than about 30."
        # To ensure this criterion is satisfied for the full range of count
        # rates, we scale the mean spectrum by the minimum gross counts
        # and check against that.
        gross = optimizer.bkg_spectra.sum(axis=1)
        bkg_min = bkg_mean * gross.min() / bkg_mean.sum()
        B_min = R @ bkg_min
        assert (B_min >= 30.0).all()

        # calculate the covariance of the background SCRs
        alphas = T @ R @ optimizer.bkg_spectra.T
        assert np.allclose(alphas.mean(axis=1), 0)
        S = alphas @ alphas.T / (optimizer.bkg_spectra.shape[0] - 1)
        S_inv = np.linalg.inv(S)

        # check if S is invertible but matrix condition number too large
        # this rejects matrices that are close to being singular
        if np.linalg.cond(S) > MAX_MATRIX_CONDITION:
            raise np.linalg.LinAlgError("Condition value of S too large")

        # calculate projection onto the space spanned by the nuisances
        I = np.eye(len(B_bar) - 1)
        P = np.zeros_like(I)
        if self.nuisance_spectra.shape[0] >= 1:
            A_n = self._spectra_to_scr_matrix(T, self.nuisance_spectra)
            P = self._projection_matrix(S_inv, A_n)

        # convert all source spectra to SCRs
        A_t = self._spectra_to_scr_matrix(T, optimizer.src_spectra)

        # calculate the FOM
        fom = (A_t * (S_inv @ (I - P) @ A_t)).sum(axis=(0, 1))
        return fom

    def fit(self, spectra, spectra_ids, src_ids=None, n_windows=None, **kwargs):
        """Compute N-SCRAD windows for the given sources and nuisances.

        In this version of `fit`, we assume that we do not know the source
        spectral templates but we need to discover them from the labeled
        training data. We also assume that all of the specified sources
        will be grouped together into one statistic.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources. By default, use any IDs in
            `spectra_ids` that are not 0.
        n_windows : int, optional
            Number of spectral windows to optimize. No guidance appears to be
            given in the literature on how to choose the number of windows,
            but must be at least two more than the number of nuisances.
            By default 2 + len(self.nuisance_spectra).
        kwargs:
            Kwargs sent to the optimizer.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        if n_windows is None:
            n_windows = 2 + len(self.nuisance_spectra)

        # set up the optimizer
        opt = self._optimizer_class(
            self, spectra, spectra_ids, src_ids=src_ids, srcs_required=False, **kwargs
        )

        # initialize with random windows
        while True:
            # generate random windows and make sure FOM is valid
            self._set_random_binning(n_windows)
            try:
                self._figure_of_merit(opt)
            except (AssertionError, np.linalg.LinAlgError):
                continue
            break

        # set up the optimizer (again)
        # this is a kludge to re-initialize the population
        # if the number of windows have changed
        # TODO: a training data class would help avoid this
        opt = self._optimizer_class(
            self, spectra, spectra_ids, src_ids=src_ids, srcs_required=False, **kwargs
        )

        best_windows, best_fom = opt.run()
        self._set_rebinning(best_windows)

        # save data produced during training
        train_info = {}
        train_info["ts"] = opt.ts
        train_info["foms"] = opt.foms
        train_info["bkg_spectrum"] = opt.bkg_spectra.mean(axis=0)
        return train_info
