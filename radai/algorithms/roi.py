"""Region of Interest (ROI) algorithm."""

import numpy as np
from sklearn import linear_model
from .cew import CEW


class ROI(CEW):
    """Region of Interest (ROI) algorithm.

    This algorithm is a special case of CEW, where the desired source
    and background bins are specified.
    """

    def __init__(
        self,
        far=1.0 / 8.0,
        int_time=1.0,
        bin_edges=(),
        src_regions=(),
        bkg_regions=(),
        w_s=(),
        w_b=(),
        intercept=0.0,
    ):
        """Initialize the algorithm.

        Parameters
        ----------
        far : float
            false alarm rate in [alarms per hour]
        int_time : float
            mean integration time per measurement
        bin_edges : array_like
            The edges of the spectral bins.
        src_regions : array_like
            A list of length-2 lists giving desired source bin boundaries.
        bkg_regions : array_like
            A list of length-2 lists giving desired background bin boundaries.
        w_s : array_like
            Source weights, an array of 0s and 1s.
            Length is the number of spectrum bins.
        w_b : array_like
            Background weights, a 1D array of floats that are 0 where w_s is 1.
            Length is the number of spectrum bins.
        intercept : float
            The intercept of the linear regression model on the background.
        """
        # Instantiate object
        super().__init__(
            far=far,
            int_time=int_time,
            w_s=w_s,
            w_b=w_b,
            intercept=intercept,
        )

        # Check the source and background regions
        self._set_regions(bin_edges, src_regions, bkg_regions)

    def _set_regions(self, bin_edges, src_regions, bkg_regions):
        """Check the source and background regions and set them.

        Parameters
        ----------
        bin_edges : array_like
            The edges of the spectral bins.
        src_regions : array_like
            A list of length-2 lists giving desired source bin boundaries.
        bkg_regions : array_like
            A list of length-2 lists giving desired background bin boundaries.
        """
        assert len(bin_edges) >= 2
        assert (np.diff(bin_edges) > 0).all()
        for regions in [src_regions, bkg_regions]:
            assert len(regions) >= 1
            for w in regions:
                assert len(w) == 2
                assert w[1] > w[0]
                assert bin_edges[0] <= w[0]
                assert w[1] <= bin_edges[-1]
        self.bin_edges = bin_edges
        self.src_regions = src_regions
        self.bkg_regions = bkg_regions

    def fit(
        self,
        spectra,
        spectra_ids,
        alpha=1.0,
        fit_intercept=True,
    ):
        """Compute source and background weights for the ROIs given.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        alpha : float
            Regularization strength for the ridge regression.
        fit_intercept: bool
            Whether to allow an intercept in the linear model.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        assert len(spectra.shape) == 2
        n, m = spectra.shape

        # find background shape from training data
        n_bkg = sum(spectra_ids == 0)
        assert n_bkg >= 1, "Need more than {} background spectra to train".format(n_bkg)
        bkg_spectra = spectra[spectra_ids == 0]

        # set up bool arrays where source and background regions are given
        bin_edges = np.array(self.bin_edges)
        where_src = np.zeros(len(bin_edges) - 1, dtype=bool)
        where_bkg = np.zeros_like(where_src)
        for w in self.src_regions:
            where_src |= (bin_edges[:-1] <= w[1]) & (w[0] <= bin_edges[1:])
        for w in self.bkg_regions:
            where_bkg |= (bin_edges[:-1] <= w[1]) & (w[0] <= bin_edges[1:])
        assert sum(where_src) >= 1
        assert sum(where_bkg) >= 1

        # ensure source and background regions do not overlap
        # bin specification is clumsy, so keep source bins, and remove any
        # background bins flagged as source bins.
        where_bkg &= ~where_src
        assert sum(where_src) >= 1
        assert sum(where_bkg) >= 1
        assert ~(where_src & where_bkg).any()

        # source weights are 1 where source regions are overlap
        w_s = where_src.astype(float)

        # use ridge regression to find the background weights
        # these weights should "zero out" the source counts using the
        # the bins specified by the desired background regions
        model = linear_model.Ridge(alpha=alpha, fit_intercept=fit_intercept)
        model.fit(bkg_spectra[:, where_bkg], bkg_spectra @ w_s)
        w_b = np.zeros_like(w_s)
        w_b[where_bkg] = model.coef_

        # save the weights
        self._set_weights(w_s, w_b)
        self.intercept = model.intercept_

        # save data produced during training
        train_info = {}
        train_info["where_src"] = where_src
        train_info["where_bkg"] = where_bkg

        return train_info

    def to_dict(self) -> dict:
        return dict(
            far=self.far,
            int_time=self.int_time,
            w_s=self.w_s,
            w_b=self.w_b,
            intercept=self.intercept,
            src_regions=self.src_regions,
            bkg_regions=self.bkg_regions,
            bin_edges=self.bin_edges,
        )
