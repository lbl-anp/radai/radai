"""Censored Energy Window (CEW) algorithm."""

from .algorithm import Algorithm
from .cew_optimizer import CEWOptimizer
from ..result import Result, Results
from ..tools import fpr_from_far
from ..nmf import train_bkg_sources
from scipy.stats import norm
import numpy as np
from sklearn import linear_model


class CEW(Algorithm):
    """Censored Energy Window (CEW) algorithm.

    The primary reference is this report, but it is not widely available:

    [1] K. Nelson and S. Labov, "Aggregation of mobile radiation data,"
        Lawrence Livermore Nat. Lab., Livermore, CA, USA, Tech. Rep., 2010.

    These references give some details about the algorithm:

    [2] K. Miller and A. Dubrawski, "Gamma-Ray Source Detection With Small
        Sensors," IEEE TNS 65 (4), 2018.
        https://doi.org/10.1109/TNS.2018.2811049

    [3] K. Miller, et al., "Evaluation of coded aperture radiation detectors
        using a Bayesian approach," NIM A, 839, 29--38, 2016.
        https://doi.org/10.1016/j.nima.2016.09.027

    [4] E. Lei, "Robust Detection of Radiation Threat," unknown if/where
        published, but available here:
        https://www.ml.cmu.edu/research/dap-papers/S17/dap-lei-eric.pdf
    """

    def __init__(
        self,
        far=1.0 / 8.0,
        int_time=1.0,
        w_s=None,
        w_b=None,
        intercept=0.0,
        min_var=1.0,
    ):
        """Initialize the algorithm.

        Parameters
        ----------
        far : float, optional
            False alarm rate in [alarms per hour]
        int_time : float, optional
            Mean integration time per measurement
        w_s : array_like, optional
            Source weights, an array of 0s and 1s.
            Length is the number of spectrum bins.
        w_b : array_like, optional
            Background weights, a 1D array of floats that are 0 where w_s is 1.
            Length is the number of spectrum bins.
        intercept : float, optional
            The intercept of the linear regression model on the background.
            By default 0.
        min_var : float, optional
            The minimum variance ... TODO
        """
        w_s = [] if w_s is None else w_s
        w_b = [] if w_b is None else w_b

        # Instantiate object
        super().__init__(
            far=far,
            int_time=int_time,
            w_s=w_s,
            w_b=w_b,
            intercept=intercept,
            min_var=float(min_var),
        )

        # Check the source and background weights
        self._set_weights(self.w_s, self.w_b, self.intercept)

        # Set the threshold based on the FAR and int_time
        _ = self.calculate_threshold(set_threshold=True)

    def _set_weights(self, w_s, w_b, intercept=None):
        """Check the weights and set them.

        Parameters
        ----------
        w_s : array_like
            Source weights, an array of 0s and 1s.
            Length is the number of spectrum bins.
        w_b : array_like
            Background weights, a 1D array of floats that are 0 where w_s is 1.
            Length is the number of spectrum bins.
        intercept : float
            The intercept of the linear regression model on the background.
            By default 0.
        """
        w_s = np.atleast_1d(w_s)
        w_b = np.atleast_1d(w_b)
        assert len(w_s) == len(w_b)
        if len(w_s) > 0:
            for w in np.unique(w_s):
                assert np.isclose(w, 0.0) or np.isclose(w, 1.0)
            assert np.allclose(w_b[w_s > 0.99], 0.0)
        self.w_s = w_s
        self.w_b = w_b
        self.intercept = 0.0 if intercept is None else intercept

    def calculate_threshold(self, set_threshold: bool = True) -> float:
        """Compute threshold far and int_time from algorithm state.

        Parameters
        ----------
        set_threshold : bool
            Set threshold if True.

        Returns
        -------
        threshold : float
            Negative log survival function of gaussian based on `far`
            and `int_time`.
        """
        fpr = fpr_from_far(far=self.far, int_time=self.int_time)
        threshold = -np.log(fpr)
        if set_threshold:
            self.threshold = threshold
        return threshold

    def _analyze(self) -> Results:
        """Analyze a single measurement.

        The metric used is given explicitly in [2] and [3]. The source window
        `w_s` is dotted with the spectrum to obtain the in-window counts `n_s`.
        Meanwhile, the background window `w_b` is dotted with the spectrum to
        obtain `n_b`, which is an estimate of the mean of `n_s` using only
        the spectrum outside the window. The metric consists of the difference
        between `n_s` and `n_b`, divided by the square root of the variance
        of that difference, and the metric should be distributed as a unit
        normal.

        In [2] and [3], the variance is estimated as `n_b` since `n_s` is
        Poisson. However, using `n_b` as the variance slightly underestimates
        the variance since it does not include the variance of `n_b` itself,
        which is `w_b ** 2` dotted with the spectrum. In some cases, not
        including this term can lead to higher than expected false alarm
        rates. This correction does not seem to be noted in the literature.

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the negative log survival
            function of a Gaussian.
        """

        spectrum, timestamp, live_time = self.spectra_buffer[-1]
        if np.isnan(live_time):
            live_time = self.int_time

        # check inputs and weights
        w_s = self.w_s
        w_b = self.w_b
        assert len(spectrum) == len(w_s)
        assert len(spectrum) == len(w_b)

        # use background weights to predict source window counts
        n_s = np.dot(w_s, spectrum)
        n_b = max(np.dot(w_b, spectrum) + self.intercept, 0.0)
        mean = n_b
        # variance is slightly larger than n_b
        # this extra term is not in [2] or [3]
        var = max(n_b + np.dot(w_b**2, spectrum), self.min_var)
        assert var > 0.0, "CEW variance should never be negative"

        alarm_metric = -norm.logsf(
            x=n_s,
            loc=mean,
            scale=np.sqrt(var),
        )

        return Results(
            timestamp,
            [
                Result(
                    origin="CEW",
                    source_id=1,
                    is_alarm=alarm_metric > self.threshold,
                    alarm_metric=alarm_metric,
                    alarm_metric_name="negative-log-survival",
                    timestamp_start=timestamp - self.int_time,
                    timestamp_stop=timestamp,
                )
            ],
        )

    def _max_snr_source_window(
        self,
        spectra,
        spectra_ids,
        src_spectrum,
        max_src_counts_fraction=0.5,
    ):
        """Compute the source window with maximum SNR.

        This part of the algorithm is explained in [4].

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_spectrum : array_like
            The source spectrum. The absolute scale will not affect the
            results of `fit`.
        max_src_counts_fraction:float, optional
            Maximum fraction of the mean counts that a CEW source window is
            allowed to include. Prevents CEW source windoes from "running away"
            and taking up the majority of the spectral bins. By default 0.5.

        Returns
        -------
        w_s : array_like
            Source weights, an array of 0s and 1s.
            Length is the number of spectrum bins.
        train_info : dict
            Dictionary of data produced during training.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n

        # find mean background shape from training data
        bkg_spectra = spectra[spectra_ids == 0, :]
        n_bkg = bkg_spectra.shape[0]
        n_min = 1
        assert n_bkg >= n_min, "Need at least {} background spectra to train".format(
            n_min
        )
        # ensure mean background shape has no nonzero elements
        bkg_spectrum = bkg_spectra.sum(axis=0).clip(1).astype(float) / len(bkg_spectra)

        # use the given source spectrum shape
        src_spectrum = np.asarray(src_spectrum)
        assert len(src_spectrum.shape) == 1
        assert len(src_spectrum) == m

        # determine the source weights
        # find bins that maximize source SNR
        s_over_b = src_spectrum / bkg_spectrum
        i_sort = np.argsort(s_over_b)[::-1]
        snr = np.zeros_like(s_over_b)
        src_counts_fraction = np.zeros_like(s_over_b)
        for k in range(1, len(i_sort)):
            src = src_spectrum[i_sort[:k]].sum()
            bkg = bkg_spectrum[i_sort[:k]].sum()
            snr[k] = src / np.sqrt(bkg)
            src_counts_fraction[k] = bkg / bkg_spectrum.sum()
        # find maximum SNR
        # but prevent source window from taking up more than half the counts
        k_max = np.argmax(snr * (src_counts_fraction <= max_src_counts_fraction))
        i_src = i_sort[: k_max + 1]
        # set the source weights
        w_s = np.zeros_like(src_spectrum)
        w_s[i_src] = 1.0

        # save data produced during training
        train_info = {}
        train_info["bkg_spectrum"] = bkg_spectrum
        train_info["src_spectrum"] = src_spectrum
        train_info["s_over_b"] = s_over_b
        train_info["i_sort"] = i_sort
        train_info["snr"] = snr
        train_info["src_counts_fraction"] = src_counts_fraction
        train_info["k_max"] = k_max
        train_info["i_src"] = i_src

        return w_s, train_info

    def _perform_regression(
        self, spectra, spectra_ids, w_s, alpha=1.0, fit_intercept=True
    ):
        """Perform linear regression to set the background weights.

        Use ridge regression to find the background weights `w_b`.
        These weights should "zero out" the source counts obtained using
        `w_s` by using the rest of the spectrum outside the bins specified
        by `w_s`. This part of the algorithm is explained in [2].

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        w_s : array_like
            Source weights, an array of 0s and 1s.
            Length is the number of spectrum bins.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.

        Returns
        -------
        w_b : array_like
            Background weights, an array of floats.
            Length is the number of spectrum bins.
        intercept : float
            Regression intercept.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n
        w_s = np.asarray(w_s)
        assert len(w_s) == m

        bkg_spectra = spectra[spectra_ids == 0, :]
        n_bkg = bkg_spectra.shape[0]
        n_min = 1
        assert n_bkg >= n_min, "Need at least {} background spectra to train".format(
            n_min
        )

        # perform the regression
        where_bkg = w_s < 0.5
        model = linear_model.Ridge(alpha=alpha, fit_intercept=fit_intercept)
        model.fit(bkg_spectra[:, where_bkg], bkg_spectra @ w_s)
        w_b = np.zeros_like(w_s, dtype=float)
        w_b[where_bkg] = model.coef_

        return w_b, model.intercept_

    def fit_known_sources(
        self,
        spectra,
        spectra_ids,
        src_spectrum,
        alpha=1.0,
        fit_intercept=True,
        max_src_counts_fraction=0.5,
    ):
        """Compute source and background weights for the given source.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_spectrum : array_like
            The source spectrum. The absolute scale will not affect the
            results of `fit`.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.
        max_src_counts_fraction:float, optional
            Maximum fraction of the mean counts that a CEW source window is
            allowed to include. Prevents CEW source windoes from "running away"
            and taking up the majority of the spectral bins. By default 0.5.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # determine the source weights
        w_s, train_info = self._max_snr_source_window(
            spectra,
            spectra_ids,
            src_spectrum,
            max_src_counts_fraction=max_src_counts_fraction,
        )

        # use ridge regression to find the background weights and intercept
        w_b, intercept = self._perform_regression(
            spectra,
            spectra_ids,
            w_s,
            alpha=alpha,
            fit_intercept=fit_intercept,
        )

        # save the weights and intercept
        self._set_weights(w_s, w_b, intercept)

        # save data produced during training
        train_info["alpha"] = alpha
        return train_info

    def fit_nmf(
        self,
        spectra,
        spectra_ids,
        src_ids=None,
        n_bkg=1,
        eps=1e-6,
        sparsity=1e-6,
        verbose=False,
        alpha=1.0,
        fit_intercept=True,
        max_src_counts_fraction=0.5,
    ):
        """Compute source and background weights for the given source.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources to optimize for. By default,
            use any IDs in `spectra_ids` that are not 0.
        eps : float, optional
            `eps` kwarg sent to nmf.train_bkg_sources. By default 1e-6.
        sparsity : float, optional
            `sparsity` kwarg sent to nmf.train_bkg_sources. By default 1e-6.
        verbose : bool, optional
            `verbose` kwarg sent to nmf.train_bkg_sources. By default False.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.
        max_src_counts_fraction:float, optional
            Maximum fraction of the mean counts that a CEW source window is
            allowed to include. Prevents CEW source windoes from "running away"
            and taking up the majority of the spectral bins. By default 0.5.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n

        all_ids = sorted(np.unique(spectra_ids))
        assert 0 in all_ids
        if src_ids is None:
            src_ids = sorted(
                [src_id for src_id in np.unique(spectra_ids) if src_id != 0]
            )
        assert len(src_ids) >= 1
        for src_id in src_ids:
            assert src_id in all_ids
            assert src_id != 0

        # find a background model and single-component source templates
        V_bkg, V_src = train_bkg_sources(
            spectra,
            spectra_ids,
            src_ids=src_ids,
            n_bkg=n_bkg,
            n_src=1,
            eps=eps,
            sparsity=sparsity,
            verbose=verbose,
        )

        # send source template to fit_known_sources()
        src_template = V_src[src_id].flatten()
        train_info = self.fit_known_sources(
            spectra,
            spectra_ids,
            src_template,
            alpha=alpha,
            fit_intercept=fit_intercept,
            max_src_counts_fraction=max_src_counts_fraction,
        )

        # add some data to train_info
        train_info["V_bkg"] = V_bkg

        return train_info

    def fit(
        self,
        spectra,
        spectra_ids,
        src_ids=None,
        alpha=1.0,
        fit_intercept=True,
        max_src_counts_fraction=0.5,
        **kwargs
    ):
        """Compute source and background weights for the given source ID.

        The source spectrum is not known, but different spectral windows
        are tried until the statistic is optimized. This is an experimental
        feature; the published versions assume the source spectrum itself
        is known.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources to optimize for. By default,
            use any IDs in `spectra_ids` that are not 0.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.
        max_src_counts_fraction:float, optional
            Maximum fraction of the mean counts that a CEW source window is
            allowed to include. Prevents CEW source windoes from "running away"
            and taking up the majority of the spectral bins. By default 0.5.
        kwargs : dict
            Keyword args to be sent to the CEWOptimizer.

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # set up and run the optimizer
        opt = CEWOptimizer(
            self,
            spectra,
            spectra_ids,
            src_ids=src_ids,
            alpha=alpha,
            fit_intercept=fit_intercept,
            max_src_counts_fraction=max_src_counts_fraction,
            **kwargs,
        )
        w_s, w_b, intercept, log = opt.run()
        self._set_weights(w_s, w_b, intercept)

        # save data produced during training
        train_info = {}
        train_info["ts"] = opt.ts
        train_info["foms"] = opt.foms
        train_info["bkg_spectrum"] = opt.bkg_spectrum_mean
        train_info["src_spectrum"] = opt.src_spectrum_sub
        train_info["s_over_b"] = opt.s_over_b
        return train_info

    def to_dict(self) -> dict:
        return dict(
            far=self.far,
            int_time=self.int_time,
            w_s=self.w_s,
            w_b=self.w_b,
            intercept=self.intercept,
        )

    def is_ready(self):
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case the algorithm has the parameters needed to analyze. These
        could have been provided by a user or from training.
        """
        # NOTE this could be removed if the init set these to None
        if len(self.w_b) == 0 or len(self.w_s) == 0:
            return False
        return self._all_ready("w_s", "w_b", "intercept")
