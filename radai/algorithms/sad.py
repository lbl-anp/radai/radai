"""Spectral Anomaly Detection (SAD) algorithm."""

from .algorithm import Algorithm
from ..result import Result, Results, ResultsBatch
from ..bins import Bins
from ..tools import calculate_num_fa, compute_threshold_empirical
import numpy as np


class SAD(Algorithm):
    """Spectral Anomaly Detection (SAD) algorithm.

    This implementation follows the algorithm as described in [1].

    The primary reference for the original implementation of this algorithm is
    in a report that is not widely available ([2]). Details of the original
    implementation can be found in [3 - 4].

    References
    ----------
    [1] Bilton, K. (2020). "Data-driven Approaches to Spectral Gamma-ray Source
        Detection and Identification." https://escholarship.org/uc/item/59k407r4

    [2] Karl Nelson and Simon Labov. "Detection and alarming with sords unimaged
        data: Background data analysis." Lawrence Livermore National Lab
        Technical Report, 2009.

    [3] Tandon, P. (2018). "Bayesian Aggregation of Evidence for Detection and
        Characterization of Patterns in Multiple Noisy Observations."
        https://doi.org/10.1184/R1/6714947.v1

    [4] E. Lei, "Robust Detection of Radiation Threat," unknown if/where
        published, but available here:
        https://www.ml.cmu.edu/research/dap-papers/S17/dap-lei-eric.pdf
    """

    def __init__(
        self,
        far=1.0 / 8.0,
        int_time=1.0,
        num_components=3,
        bin_edges=None,
        bin_centers=None,
        num_bins=None,
        threshold=None,
        std_mat_inv=None,
        std_mat=None,
        basis=None,
        mean=None,
    ):
        bins = Bins(num=num_bins, edges=bin_edges, centers=bin_centers)
        basis = None if basis is None else np.asarray(basis)
        std_mat = None if std_mat is None else np.asarray(std_mat)
        std_mat_inv = None if std_mat_inv is None else np.asarray(std_mat_inv)
        mean = None if mean is None else np.asarray(mean)
        if (basis is not None) and (std_mat is not None) and (std_mat_inv is not None):
            VTV = np.dot(basis.T, basis)
            transformation = std_mat_inv.dot(VTV.dot(std_mat))
        else:
            transformation = None

        # Instantiate the algorithm base functionality
        super().__init__(
            spectra_buffer_num_energy=bins.num,
            far=far,
            int_time=int_time,
            num_components=num_components,
            bins=bins,
            std_mat=std_mat,
            std_mat_inv=std_mat_inv,
            basis=basis,
            transformation=transformation,
            mean=mean,
        )
        self.threshold = threshold

    def fit(self, spectra, live_times=None):
        """Compute background model from background-only data.

        Principal component analysis (PCA) is performed on the correlation
        matrix of the background-only data. The first k principal components,
        where k is `num_components`, are used to define the transformation
        matrix, which is used to approximate the background spectrum of a given
        measured spectrum.

        Parameters
        ----------
        spectra : 2d array
            background gamma-ray data (num_measurements, num_bins)
        """
        training = np.array(spectra, dtype=np.float64)
        assert training.shape[1] == len(self.bins)

        # Mean center data and compute the covariance matrix
        mean = training.mean(axis=0)
        self.mean = mean
        training -= mean
        cov_mat = training.T.dot(training) / (training.shape[0] - 1)

        # Compute variance matrix
        std_mat_inv = np.diag(1 / (np.sqrt(np.diag(cov_mat))))
        std_mat = np.diag(np.sqrt(np.diag(cov_mat)))
        self.std_mat_inv = std_mat_inv
        self.std_mat = std_mat

        # Compute the correlation matrix C = sigma^-1 * Sigma * sigma^-1
        corr_mat = std_mat_inv.dot(cov_mat.dot(std_mat_inv))

        # Perform the SVD on the correlation matrix
        U, S, V = np.linalg.svd(corr_mat)

        # Perform the SVD flip
        max_abs_cols = np.argmax(np.abs(U), axis=0)
        signs = np.sign(U[max_abs_cols, range(U.shape[1])])
        V *= signs[:, np.newaxis]

        basis = V[: self.num_components]
        self.basis = basis

        # Compute transformation
        VTV = np.dot(basis.T, basis)
        transformation = std_mat_inv.dot(VTV.dot(std_mat))
        self.transformation = transformation

    def calculate_threshold(self, spectra, set_threshold=True):
        """Compute threshold empirically based on training data

        Parameters
        ----------
        spectra : 2D array
            background data for training
        set_threshold : boolean
            Set threshold in algorithm state

        Returns
        -------
        threshold
            sad metric set empirically based on far and int_time
        """
        # Validation
        if self.basis is None:
            print("Fit the model before calculating thresholds.")
        spectra = np.array(spectra)

        # Find the number of false alarms given the FAR and training set
        num_fa = max(calculate_num_fa(spectra.shape[0]), 1)

        # Fit the spectra to the model and get the loss then find threshold
        loss = self._calculate_alarm_metric(spectra)
        threshold = compute_threshold_empirical(loss, num_fa)

        if set_threshold is True:
            self.threshold = threshold

    def _analyze(self) -> Results:
        """Analyze single measurement

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the L2 norm of the
            reconstruction error scaled by the inverse square root of the gross
            counts of the measured spectrum.
            Misc values:
                weights : array of floats
                    PCA weights for spectrum
        """

        spectrum, timestamp, _ = self.spectra_buffer[-1]

        # Fit the spectra to the model
        spectrum = np.atleast_2d(spectrum)
        weights = self._transform(spectrum)
        alarm_metric = self._calculate_alarm_metric(spectrum)

        return Results(
            timestamp,
            [
                Result(
                    origin="SAD",
                    source_id=1,
                    is_alarm=alarm_metric > self.threshold,
                    alarm_metric=alarm_metric,
                    alarm_metric_name="scaled-l2-norm",
                    timestamp_start=timestamp - self.int_time,
                    timestamp_stop=timestamp,
                    weights=weights,
                )
            ],
        )

    def analyze_batch(self, spectra, timestamps, live_times=None):
        """Analyze multiple measurements with a vectorized implementation.

        Parameters
        ----------
        spectra : 2D array (n_measurements, n_bins)
            Set of spectra for analysis
        timestamps : 1D array
            Timestamps at the end of each measurement being analyzed.
        live_times : 1D array (optional)
            Integration time for each measurement

        Returns
        -------
        ResultsBatch
            Results objects containing the attributes listed in `analyze`.
        """
        _weights = self._transform(np.atleast_2d(spectra))
        _alarm_metrics = self._calculate_alarm_metric(np.atleast_2d(spectra))
        results_batch = ResultsBatch()
        for weights, alarm_metric, timestamp in zip(
            _weights, _alarm_metrics, timestamps
        ):
            results_batch.append(
                Results(
                    timestamp,
                    [
                        Result(
                            origin="SAD",
                            source_id=1,
                            is_alarm=alarm_metric > self.threshold,
                            alarm_metric=alarm_metric,
                            alarm_metric_name="scaled-l2-norm",
                            timestamp_start=timestamp - self.int_time,
                            timestamp_stop=timestamp,
                            weights=weights,
                        )
                    ],
                )
            )
        return results_batch

    def to_dict(self) -> dict:
        return dict(
            far=self.far,
            int_time=self.int_time,
            num_components=self.num_components,
            bin_edges=self.bins.edges,
            bin_centers=self.bins.centers,
            threshold=self.threshold,
            std_mat_inv=self.std_mat_inv,
            std_mat=self.std_mat,
            basis=self.basis,
            mean=self.mean,
        )

    def is_ready(self) -> bool:
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case the algorithm has the parameters needed to analyze. These
        could have been provided by a user or from training.
        """
        return self._all_ready(
            "threshold",
            "transformation",
            "basis",
            "std_mat",
            "std_mat_inv",
            "mean",
        )

    # ---------------------------- Helper Methods -----------------------------
    def _calculate_alarm_metric(self, X):
        """Calculate the SAD metric."""
        Xhat = (X - self.mean).dot(self.transformation) + self.mean
        return np.linalg.norm(X - Xhat, axis=1) / np.sqrt(X.sum(axis=1))

    def _transform(self, X):
        """
        Compute PCA weights
        """
        X = np.atleast_2d(X)
        return (X - self.mean).dot(self.std_mat_inv).dot(self.basis.T)

    def _inverse_transform(self, Z):
        """
        Compute spectrum estimate from weights
        """
        Z = np.atleast_2d(Z)
        return Z.dot(self.basis).dot(self.std_mat) + self.mean
