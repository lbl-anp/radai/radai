"""N-SCRAD algorithm alternate version without contiguity constraint."""

import random
import numpy as np
from ..filters import EWMAFilter
from .nscrad import NSCRAD, validate_rebinning_matrix
from .nscrad_optimizer import NSCRADOptimizer


class NSCRADAlternateOptimizer(NSCRADOptimizer):
    """Optimize the N-SCRAD figure of merit using a genetic algorithm."""

    _optimize_attr = "rebinning_matrix"

    def _assign_individual_to_alg(self, individual):
        """Assign the individual (rebinning params) to the algorithm."""
        # reshape the parameters into a rebinning matrix
        rebinning = np.reshape(individual.flatten(), self.alg.rebinning_matrix.shape)
        self.alg._set_rebinning(rebinning)

    def mutate(self, individual):
        """Mutate the individual."""
        while True:
            try:
                self._assign_individual_to_alg(np.array(individual).flatten())
                self.alg._randomly_perturb_binning(n_flip=self.max_step)
                return [self._extract_individual_from_alg()]
            except AssertionError:
                continue

    def crossover(self, ind1, ind2):
        """Randomly swap the rows of the rebinning matrices."""
        self._assign_individual_to_alg(ind1.flatten())
        rebin1 = np.array(self.alg.rebinning_matrix)
        self._assign_individual_to_alg(ind2.flatten())
        rebin2 = np.array(self.alg.rebinning_matrix)

        # form new individuals
        n_tries = 0
        while True:
            n_tries += 1
            if n_tries == 20:
                return ind1, ind2
            cross = np.random.choice([True, False], size=rebin1.shape)
            rebin3 = np.array(rebin1)
            rebin4 = np.array(rebin2)
            rebin3[cross] = rebin2[cross]
            rebin4[~cross] = rebin1[~cross]
            try:
                self._assign_individual_to_alg(rebin3.flatten())
                ind3 = self._extract_individual_from_alg()
                self._assign_individual_to_alg(rebin4.flatten())
                ind4 = self._extract_individual_from_alg()
            except AssertionError:
                continue
            break
        return ind3, ind4


class NSCRADAlternate(NSCRAD):
    """Nuisance-rejection Spectral Comparison Ratio Anomaly Detection.

    This version does not use spectral windows and instead allows the
    rebinning matrix to be any matrix of zeros and ones, not necessarily
    from contiguous spectral regions.

    This version, as far as we can tell, is not considered in the literature.
    """

    _required_config_vars = [
        "far",
        "int_time",
        "lam",
        "bin_edges",
        "rebinning_matrix",
        "nuisance_spectra",
    ]
    _optimizer_class = NSCRADAlternateOptimizer

    def __init__(
        self,
        far=1.0 / 8.0,
        int_time=1.0,
        lam=0.001,
        bin_edges=None,
        rebinning_matrix=None,
        nuisance_spectra=None,
    ):
        """Initialize the algorithm.

        Parameters
        ----------
        far : float, optional
            False alarm rate in [alarms per hour]. By default 1.0 / 8.0.
        int_time : float, optional
            Mean integration time per measurement. By default 1.0.
        lam : float, optional
            The EWMA `lambda` parameter for the background estimator.
            By default 1e-3.
        bin_edges : array_like, optional
            The edges of the spectral bins. By default an empty list.
        rebinning_matrix : array_like or int, optional
            2D array of dimensions (n_windows, n_bins). Entries are 0 or 1.
            If an integer, will use it as the number of windows and create
            an identity matrix of shape (n_windows, len(bin_edges) - 1).
            If None will default to an identity matrix of shape
            (2 + len(nuisance_spectra), len(bin_edges) - 1).
        nuisance_spectra : array_like, optional
            A list of any nuisance spectra that will be projected out so the
            algorithm is not sensitive to changes in them. Length of each
            nuisance is len(bin_edges) - 1.  By default an empty ndarray of
            shape (0, len(bin_edges) - 1).
        """

        # Initialize NSCRAD algorithm to setup bin_edges, nuisance_spectra, etc
        super().__init__(
            far=far,
            int_time=int_time,
            lam=lam,
            bin_edges=bin_edges,
            spectral_windows=None,
            nuisance_spectra=nuisance_spectra,
        )

        # validate the windows and calculate the rebinning matrix
        self._set_rebinning(rebinning_matrix)

        # Set the threshold based on the FAR and int_time
        self.calculate_threshold(set_threshold=True)

        # start the background estimator
        self.filter = EWMAFilter(lam=lam)

    def _set_rebinning(self, rebinning_matrix):
        """Set the spectral windows and recalculate the rebinning matrix.

        Parameters
        ----------
        rebinning_matrix : array_like or int, optional
            2D array of dimensions (n_windows, n_bins). Entries are 0 or 1.
            If an integer, will use it as the number of windows and create
            an identity matrix of shape (n_windows, len(bin_edges) - 1).
            If None will default to an identity matrix of shape
            (2 + len(nuisance_spectra), len(bin_edges) - 1).
        """
        # set a default matrix if None
        if rebinning_matrix is None:
            rebinning_matrix = np.eye(
                2 + len(self.nuisance_spectra), len(self.bin_edges) - 1
            )
        elif isinstance(rebinning_matrix, int):
            n_windows = rebinning_matrix
            rebinning_matrix = np.eye(n_windows, len(self.bin_edges) - 1)

        # calculate the rebinning matrix and rebinned nuisances
        self.rebinning_matrix = np.asarray(rebinning_matrix)
        validate_rebinning_matrix(self.bin_edges, self.rebinning_matrix)
        self.rebinned_nuisances = self.nuisance_spectra @ self.rebinning_matrix.T

        # calculate degrees of freedom of the statistic
        self.dof = self.rebinning_matrix.shape[0] - 1
        self.dof -= self.rebinned_nuisances.shape[0]
        assert self.dof >= 1

        # kludge to get inheritance working for fit method
        self.spectral_windows = self.rebinning_matrix

    def to_dict(self) -> dict:
        return dict(
            far=self.far,
            int_time=self.int_time,
            lam=self.lam,
            bin_edges=self.bin_edges,
            rebinning_matrix=self.rebinning_matrix,
            nuisance_spectra=self.nuisance_spectra,
        )

    def is_ready(self) -> bool:
        """Is this algorithm ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        In this case the algorithm has the parameters needed to analyze. These
        could have been provided by a user or from training.
        """
        return self._all_ready(
            "far",
            "int_time",
            "lam",
            "bin_edges",
            "nuisance_spectra",
            "rebinning_matrix",
            "dof",
            "threshold",
        )

    def _set_random_binning(self, n_windows):
        """Generate random windows that give a valid rebinning matrix.

        Parameters
        ----------
        n_windows : int
            Number of spectral windows to generate.

        Returns
        -------
        rebinning_matrix : array_like
            2D array of dimensions (n_windows, n_bins). Entries are 0 or 1.
        """
        # generate random windows until we have a valid rebinning matrix
        windows = []
        while len(windows) < n_windows:
            win = np.random.choice([0, 1], size=len(self.bin_edges) - 1)
            windows.append(win)
            R = np.array(windows)
            if len(R.shape) == 1:
                R.shape = (1, len(R))
            try:
                validate_rebinning_matrix(self.bin_edges, R)
            except AssertionError:
                windows = windows[:-1]
        self._set_rebinning(windows)
        return windows

    def _randomly_perturb_binning(self, n_flip=5):
        """Randomly adjust the rebinning matrix.

        Parameters
        ----------
        n_flip : int, optional
            Number of rebinning matrix elements to flip. By default 5.
        """
        for _ in range(n_flip):
            mat = np.array(self.rebinning_matrix)
            # choose which matrix element to flip
            i = random.randint(0, mat.shape[0] - 1)
            j = random.randint(0, mat.shape[1] - 1)
            self.rebinning_matrix[i, j] = 1 - self.rebinning_matrix[i, j]
            # validate the windows and rebinning matrix
            try:
                self._set_rebinning(self.rebinning_matrix)
            except AssertionError:
                self._set_rebinning(mat)
