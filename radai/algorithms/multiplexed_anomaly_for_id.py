"""Multiple anomaly algorithms run in parallel to obtain source ID."""

from .algorithm import Algorithm
from ..result import Results
import numpy as np


class MultiplexedAnomalyForID(Algorithm):
    """Multiple anomaly algorithms run in parallel to obtain source ID.

    The sub-algorithms must all be the same class (specified by
    `sub_alg_class`) and all have the same integration time and FAR.
    This class assumes each sub-algorithm is optimized for a particular
    source ID. When analyzing data, the source ID corresponding to the
    sub-algorithm with the largest alarming metric is chosen as the source ID.
    """

    sub_alg_class = None

    def __init__(self, far=1.0 / 8.0, int_time=1.0, **kwargs):
        """Initialize the algorithm.

        Parameters
        ----------
        far : float
            false alarm rate in [alarms per hour]
        int_time : float
            mean integration time per measurement
        alg{j} : dict
            Dictionary version of the algorithm to be used for source ID `j`.
        """
        # Instantiate object
        super().__init__(
            far=far,
            int_time=int_time,
            **kwargs,
        )

        # Instantiate sub-algorithms
        self.sub_alg = {}
        self.src_ids = []
        n_sub_algs = len(kwargs.keys())
        for sub_alg_key in kwargs:
            src_id = int(sub_alg_key[3:])
            assert src_id > 0
            assert (
                src_id not in self.src_ids
            ), f"Duplicate source ID: {src_id}, {self.src_ids}"
            sub_dict = kwargs[sub_alg_key]
            # ensure all sub-algorithms have the same int_time
            sub_dict["int_time"] = int_time
            # override sub-algorithm FAR using the Bonferroni correction
            sub_dict["far"] = far / n_sub_algs
            self.sub_alg[src_id] = self.sub_alg_class(**sub_dict)
            self.src_ids.append(src_id)
        self.src_ids = sorted(list(self.sub_alg.keys()))
        self.n_sub_algs = len(self.src_ids)

        # Set the threshold for each sub-algorithm
        self.calculate_threshold(set_threshold=True)
        # set threshold to None to denote that it is not used
        self.threshold = None

    def calculate_threshold(self, set_threshold: bool = True) -> float:
        """Compute threshold far and int_time from algorithm state.

        Parameters
        ----------
        set_threshold : bool
            Set threshold if True.

        Returns
        -------
        threshold : float
            Negative log survival function of gaussian based on `far`
            and `int_time`.
        """
        for src_id in self.src_ids:
            self.sub_alg[src_id].calculate_threshold(set_threshold=set_threshold)

    def _analyze(self) -> Results:
        """Analyze a single measurement.

        Returns
        -------
        Results
            Analysis results. The `alarm_metric` is the negative log survival
            function of a Gaussian.
        """
        spectrum = self.spectra_buffer.get_spectra(-1)
        timestamp = self.spectra_buffer.get_timestamps(-1)
        live_time = self.spectra_buffer.get_live_times(-1)

        # get results from each sub-algorithm
        results = Results(timestamp)
        for src_id in self.src_ids:
            # NOTE each sub algorithm maintains its own spectra buffer
            result = self.sub_alg[src_id].analyze(
                spectrum, timestamp=timestamp, live_time=live_time
            )[0]
            result.source_id = src_id
            results.append(result)
        return results

    def fit(self, spectra, spectra_ids, src_ids=None, **kwargs):
        """Compute source and background weights for the given sources.

        Will destroy any current sub-algorithms and create new ones.

        Parameters
        ----------
        spectra : array_like
            Training gamma-ray data (num_measurements, num_bins).
        spectra_ids : array_like
            Integer label for the training set, length num_measurements.
        src_ids : array_like, optional
            The integer labels for the sources. By default the spectra_ids
            that are not 0.
        kwargs : dict, optional
            Additional kwargs sent to the sub-algorithm fit().

        Returns
        -------
        train_info : dict
            Dictionary of data produced during training.
        """
        # check inputs
        spectra = np.asarray(spectra)
        spectra_ids = np.asarray(spectra_ids)
        assert len(spectra.shape) == 2
        n, m = spectra.shape
        assert len(spectra_ids) == n

        # make sure background and source IDs are consistent
        all_ids = sorted(np.unique(spectra_ids))
        assert 0 in all_ids
        if src_ids is None:
            src_ids = sorted(
                [src_id for src_id in np.unique(spectra_ids) if src_id != 0]
            )
        assert len(src_ids) >= 1
        for src_id in src_ids:
            assert src_id in all_ids
            assert src_id != 0
            assert src_id > 0

        # instantiate and train each of the sub-algorithms
        self.sub_alg = {}
        self.src_ids = []
        sub_dict = self.to_dict()
        train_info = {}
        for src_id in src_ids:
            self.sub_alg[src_id] = self.sub_alg_class(**sub_dict)
            train_info[src_id] = self.sub_alg[src_id].fit(
                spectra,
                spectra_ids,
                src_ids=[src_id],
                **kwargs,
            )
            self.src_ids.append(src_id)

        return train_info

    def to_dict(self, **kwargs) -> dict:
        kwargs = {"far": self.far, "int_time": self.int_time}
        for src_id in self.src_ids:
            kwargs[f"alg{src_id}"] = self.sub_alg[src_id].to_dict()
        return kwargs

    def is_ready(self) -> bool:
        """Are all the sub algorithms ready to analyze?

        Returns
        -------
        bool

        Notes
        -----
        The multiplexed algorithm is ready if it has sub algorithms and they are
        all ready.
        """
        return len(self.sub_alg) != 0 and all(
            self.sub_alg[src_id].is_ready() for src_id in self.src_ids
        )
