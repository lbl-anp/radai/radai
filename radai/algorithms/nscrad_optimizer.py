"""Optimization tools for N-SCRAD."""

import numpy as np
from .genetic_optimizer import GeneticOptimizer


class NSCRADOptimizer(GeneticOptimizer):
    """Optimize the N-SCRAD figure of merit using a genetic algorithm."""

    _optimize_attr = "spectral_windows"

    def __init__(
        self,
        *args,
        src_templates=None,
        **kwargs,
    ):
        """Initialize the optimizer.

        Parameters
        ----------
        args : list
            List of arguments needed for GeneticAlgorithm.
        src_templates : array_like
            The source spectra. The length of each template must be
            len(bin_edges) - 1. By default None, which will lead to an error.
        kwargs : dict
            Keyword arguments needed for GeneticAlgorithm.
        """
        # N-SCRAD-specific settings
        self.src_templates = src_templates

        super().__init__(*args, **kwargs)

    def _assign_individual_to_alg(self, individual):
        """Assign the individual (rebinning params) to the algorithm."""
        # reshape the parameters into spectral windows
        individual = individual.flatten()
        n = len(individual)
        windows = [win for win in np.reshape(individual, (n // 2, 2))]
        # try to assign the windows to the algorithm instance
        self.alg._set_rebinning(windows)

    def _extract_individual_from_alg(self):
        """Extract an individual (rebinning params) from the algorithm."""
        individual = np.array(getattr(self.alg, self._optimize_attr)).flatten()
        return self.creator.Individual(individual)

    def random_individual(self):
        """Create a random individual."""
        n_windows = self.alg.rebinning_matrix.shape[0]
        self.alg._set_random_binning(n_windows)
        ind = self._extract_individual_from_alg()
        return ind

    def figure_of_merit(self, individual):
        """Assign a figure of merit to each individual."""
        individual = individual.flatten()
        try:
            self._assign_individual_to_alg(individual)
            fom = self.alg._figure_of_merit(self.bkg_spectra, self.src_templates)
            return (fom,)
        except (AssertionError, np.linalg.LinAlgError):
            return (-1,)

    def mutate(self, individual):
        """Mutate the individual."""
        while True:
            try:
                self._assign_individual_to_alg(np.array(individual).flatten())
                self.alg._randomly_perturb_binning(max_step=self.max_step)
                return [self._extract_individual_from_alg()]
            except AssertionError:
                continue

    def crossover(self, ind1, ind2):
        """Randomly swap the rows of the rebinning matrices."""
        self._assign_individual_to_alg(ind1.flatten())
        win1 = np.array(self.alg.spectral_windows)
        self._assign_individual_to_alg(ind2.flatten())
        win2 = np.array(self.alg.spectral_windows)

        # form new individuals
        n_tries = 0
        while True:
            n_tries += 1
            if n_tries == 20:
                return ind1, ind2
            cross = np.random.choice([True, False], size=win1.shape[0])
            win3 = np.array(win1)
            win4 = np.array(win2)
            win3[cross] = win2[cross]
            win4[~cross] = win1[~cross]
            try:
                self._assign_individual_to_alg(win3.flatten())
                ind3 = self._extract_individual_from_alg()
                self._assign_individual_to_alg(win4.flatten())
                ind4 = self._extract_individual_from_alg()
            except AssertionError:
                continue
            break
        return ind3, ind4

    def run(self):
        """Run a simple genetic algorithm.

        Modified version of deap's eaSimple.

        Returns
        -------
        best_attribute : array_like
            Attribute of the best individual (e.g., spectral windows).
        log : deap.tools.Logbook
            Log of the optimization data.
        """
        best, log = super().run()
        self._assign_individual_to_alg(best)
        return getattr(self.alg, self._optimize_attr), self.log
