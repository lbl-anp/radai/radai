""""""

from typing import Union
from pathlib import Path
import numpy as np

try:
    import tensorflow as tf
    from tensorflow.keras import metrics
except ImportError:
    tf = None
    metrics = None

# Importing this is used to load into tensorflow
from ..tf_utils import WarmUp

from radai.algorithms import Algorithm
from radai.result import Results, ResultsBatch


class NN_Classifier(Algorithm):
    """Abstract neural network class"""

    def __init__(
        self,
        name: str,
        **nn_config,
    ):
        """ """
        super().__init__(
            name=name,
            spectra_buffer_num_energy=nn_config["energy_dim"],
            spectra_buffer_num_time=nn_config["time_dim"],
            nn_config=nn_config,
        )
        # Build model
        self._build_model(nn_config)

    # ---------------------------- Required Methods ---------------------------
    def fit(
        self,
        data,
        data_labels,
        learning_rate=1.0e-3,
        n_epochs=200,
        validation_split=0.2,
        batch_size=1024,
        validation_data=None,
        rescale=False,
        **kwargs,
    ):
        """ """
        # check input
        data = np.asarray(data)
        data_labels = np.asarray(data_labels)
        # should work for spectra algorithms or waterfall algorithms
        data = self._reshape(data)

        if rescale:
            data = self._rescale(data, fit=True)

        assert len(np.unique(data_labels)) == self.nn_config["n_sources"] + 1
        assert (
            sorted(np.unique(data_labels))
            == np.arange(0, self.nn_config["n_sources"] + 1, 1)
        ).all()

        # check number of each label
        occurances = np.bincount(data_labels)
        assert (occurances > 20).all()
        print("Training data consists of:")
        for idx, val in enumerate(occurances):
            print("  source {}: {}".format(idx, val))

        self._fit(
            data,
            data_labels,
            learning_rate=learning_rate,
            n_epochs=n_epochs,
            validation_split=validation_split,
            batch_size=batch_size,
            validation_data=validation_data,
            **kwargs,
        )

    def calculate_threshold(self, set_threshold: bool = True) -> float:
        raise NotImplementedError("Threshold setting moved to training module.")

    def _analyze(self, *args) -> Results:
        """Analyze a single measurement.

        Parameters
        ----------
        args : Any
            NOT USED, exists for compatibility.

        Returns
        -------
        radai.Results
            Analysis results.
        """
        if not self.spectra_buffer.is_ready():
            raise RuntimeError("Cannot analyze incomplete buffer!")

        # check dims
        data = self._reshape(self.spectra_buffer.spectra.copy())
        # rescale if configured
        data = self._rescale(data)

        # Run a data through the network (batch size of 1)
        out = self.model.call(tf.convert_to_tensor(data, dtype=np.float32)).numpy()[0]
        # Define alarms based on source score
        results = self._outputs_to_results(out, self.spectra_buffer.timestamps[0])

        return results

    def analyze_data(self, data, timestamp) -> Results:
        """Analyze preformatted data
        Warning! Doesn't use the buffer
        """
        data = self._reshape(data)
        data = self._rescale(data)
        out = self.model.call(tf.convert_to_tensor(data, dtype=np.float32)).numpy()
        return self._outputs_to_results(out[0], timestamp)

    def to_dict(self, path: Union[str, Path]) -> dict:
        """Serialize the algorithm state to a dictionary.

        Parameters
        ----------
        path : Union[str, Path]
            The directory in which to write auxillary/support files for this
            algorithm's state. These paths are included in the configuration
            dictionary and automatically loaded in `from_dict`/`from_config`.

        Returns
        -------
        dict
            Serialized algorithm parameters.

        Notes
        -----
        The net weights will be saved to the `checkpoint` field of the
        configuration within the path location
        """
        if self.is_trained:
            checkpoint_path = Path(path) / "weights/weights"
            print(f"Saving weights into: {checkpoint_path}")
            self.model.save_weights(checkpoint_path)
            self.nn_config["checkpoint_path"] = checkpoint_path

        dic = dict(
            name=self.name,
            nn_config=self.nn_config,
            _is_trained=self._is_trained,
        )
        return dic

    @classmethod
    def from_dict(cls, dic: dict):
        """Initialize algorithm from dictionary.

        Parameters
        ----------
        dic : dict
            Algorithm init kwargs.

        Returns
        -------
        Algorithm
            The initialized algorithm.
        """
        c = cls(**dic)
        return c

    # ---------------------------- Base Methods -----------------------------

    def analyze_batch(
        self,
        data,
        timestamps=None,
        live_times=None,
        max_batch_size: int = 100,
    ) -> ResultsBatch:
        """Analyze multiple measurements

        Parameters
        ----------
        data : batch of data consistent with config
            (n_items, **data_in_model_expectation_format)
        timestamps : 1D array
            Timestamps at the end of each measurement being analyzed.
        live_times : 1D array (optional)
            Integration time for each measurement

        Returns
        -------
        radai.ResultsBatch
            Results objects containing the attributes listed in `analyze`.
        """
        data = self._reshape(data)
        data = self._rescale(data)

        # Run the whole tensor through the model
        if timestamps is None:
            timestamps = np.arange(data.shape[0])

        out = []
        for idx in range(int(np.ceil(data.shape[0] / max_batch_size))):
            ida = idx * max_batch_size
            idb = (idx + 1) * max_batch_size
            out.extend(
                self.model.call(
                    tf.convert_to_tensor(data[ida:idb], dtype=np.float32)
                ).numpy()
            )

        # produce Results for each waterfall, list into ResultsBatch object
        return ResultsBatch(
            [self._outputs_to_results(o, t) for o, t in zip(out, timestamps)]
        )

    def is_trained(self):
        return self._is_trained

    def is_ready(self) -> bool:
        """
        Check if ready to train and/or predict

        Returns
        ----------
        :return: bool
            True if ready, False otherwise
        """
        return self._all_ready(
            "model",
            "threshold",
        )

    # ---------------------------- Other Methods -----------------------------

    def _check_nn_configs(self, nn_config):
        raise NotImplementedError()

    def _build_model(self, nn_config):
        """ """
        self._check_nn_configs(nn_config)
        raise NotImplementedError()
        # self.model, self._is_trained, self.threshold = build_model(nn_config)

    def _fit(
        self,
        data,
        data_labels,
        learning_rate=1.0e-3,
        n_epochs=200,
        validation_split=0.2,
        batch_size=1024,
        validation_data=None,
        **kwargs,
    ):

        # Use warmup of the learning rate to avoid momentum
        # from getting off track at the start of training
        # warm_up = learning_rate
        warm_up = WarmUp(
            initial_learning_rate=np.float32(learning_rate),
            decay_schedule_fn=lambda x: np.float32(learning_rate),
            warmup_steps=50,
            power=1.0,
            name="warmup",
        )

        # set up optimizer
        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(warm_up),
            loss="sparse_categorical_crossentropy",
            metrics=[
                metrics.SparseCategoricalAccuracy(name="acc"),
                metrics.SparseTopKCategoricalAccuracy(k=3, name="top_k_acc"),
            ],
        )

        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor="val_loss", patience=1000, verbose=1
        )

        _ = self.model.fit(
            tf.convert_to_tensor(data, dtype=tf.float32),
            tf.convert_to_tensor(data_labels, dtype=tf.int32),
            epochs=n_epochs,
            callbacks=[early_stopping],
            batch_size=batch_size,
            shuffle=True,
            verbose=1,
            validation_split=validation_split,
            validation_data=validation_data,
            **kwargs,
        )

    def _outputs_to_results(self, outputs, timestamp) -> Results:
        results = []
        for idx, output in enumerate(outputs):
            results.append(self._output_to_result(idx, output, timestamp))
        return Results(timestamp, results)

    def _output_to_result(self, idx, output, timestamp):
        raise NotImplementedError()
        # alarm = False
        # if idx > 0 and output > self.threshold:
        #     alarm = True

        # return Result(
        #         origin=__class__.__name__,
        #         source_id=idx,
        #         is_alarm=alarm,
        #         alarm_metric=output,
        #         alarm_metric_name="softmax",
        #         timestamp_start=timestamp -
        #           self.nn_config['int_time'] * self.self.nn_config['time_dim'],
        #         timestamp_stop=timestamp,
        #     )

    def _reshape(self, data):
        """
        data should be (ntimes, nenergies) or (nitems, ntimes, nenergies)

        returns (nitems, ntimes, nenergies) -- rescaled if config specifies
        """
        data = np.asarray(data)
        assert len(data.shape) in (2, 3)
        assert data.shape[-1] == self.nn_config["energy_dim"]
        assert data.shape[-2] == self.nn_config["time_dim"]
        if len(data.shape) == 2:
            data = data[None, :, :]
        return data

    def _rescale(self, data, fit=False):
        """Rescale data, fit if specified"""
        view = np.reshape(data, (-1, data.shape[-1]))
        if fit:
            print("Fitting re-scale mean and stdev")
            self.nn_config["mean"] = np.mean(view, axis=0)
            self.nn_config["stdev"] = np.std(view, axis=0)

        # do nothing if not configured
        if not all(x in self.nn_config.keys() for x in ("mean", "stdev")):
            return data
        # rescale
        return np.reshape(
            (view - self.nn_config["mean"]) / (self.nn_config["stdev"] + 1.0e-12),
            data.shape,
        )
