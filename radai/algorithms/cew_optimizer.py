"""Censored Energy Window (CEW) algorithm."""

import numpy as np
from .genetic_optimizer import GeneticOptimizer


class CEWOptimizer(GeneticOptimizer):
    """Optimize the CEW figure of merit using a genetic algorithm."""

    def __init__(
        self,
        *args,
        alpha=1.0,
        fit_intercept=True,
        max_src_counts_fraction=0.5,
        **kwargs,
    ):
        """Initialize the optimizer.

        Parameters
        ----------
        args : list
            List of arguments needed for GeneticAlgorithm.
        alpha : float, optional
            Regularization strength for the ridge regression. By default 1.0.
        fit_intercept: bool, optional
            Whether to allow an intercept in the linear model. By default True.
        max_src_counts_fraction:float, optional
            Maximum fraction of the mean counts that a CEW source window is
            allowed to include. Prevents CEW source windoes from "running away"
            and taking up the majority of the spectral bins. By default 0.5.
        kwargs : dict
            Keyword arguments needed for GeneticAlgorithm.
        """
        # CEW-specific settings
        self.alpha = alpha
        self.fit_intercept = fit_intercept
        self.max_src_counts_fraction = float(max_src_counts_fraction)

        super().__init__(*args, **kwargs)

        # perform a rough estimate of source to background
        scale_per_bin = self.src_spectrum_mean / self.bkg_spectrum_mean.clip(1)
        scale = (scale_per_bin > 0.1).min()
        self.src_spectrum_sub = scale * self.src_spectrum_mean - self.bkg_spectrum_mean
        self.s_over_b = self.src_spectrum_sub / self.bkg_spectrum_mean.clip(1)

    def valid_individual(self, ind):
        """Return bool telling whether the individual is valid."""
        w_s = ind.flatten()
        src_win = w_s > 0.9
        n_s = sum(src_win)
        src_counts_fraction = (
            self.bkg_spectrum_mean[src_win].sum() / self.bkg_spectrum_mean.sum()
        )
        return not (n_s == 0 or src_counts_fraction > self.max_src_counts_fraction)

    def random_individual(self):
        """Create a random individual."""
        w_s = np.zeros(self.n_bins)
        while not self.valid_individual(w_s):
            p = 0.1 * np.ones(self.n_bins)
            w_s = np.random.binomial(1, p)
        ind = self.creator.Individual(w_s)
        return ind

    def _perform_regression(self, w_s):
        """Perform regression to get background weights."""
        w_b, intercept = self.alg._perform_regression(
            self.spectra,
            self.spectra_ids,
            w_s,
            alpha=self.alpha,
            fit_intercept=self.fit_intercept,
        )
        return w_b, intercept

    def figure_of_merit(self, individual):
        """Calculate a figure of merit for each individual."""
        w_s = individual.flatten()
        # fit background weights on background spectra
        w_b, intercept = self._perform_regression(w_s)
        # evaluate CEW metric on source spectra
        src_spectra = self.spectra[self.is_src]
        n_s = src_spectra @ w_s
        n_b = src_spectra @ w_b + intercept
        var = n_b + src_spectra @ (w_b**2)
        if (var > 0.0).all():
            metric = (n_s - n_b) / np.sqrt(var)
            return ((metric**2).mean(),)
        else:
            return (-1,)

    def mutate(self, individual):
        """Mutate the individual."""
        w1 = np.array(individual.flatten())
        p = np.ones(len(w1))
        # increase probability of choosing bins adjacent to source bins
        # this choice slightly favors making connected spectral regions
        p[:-1][w1[1:] > 0.9] += 0.5
        p[1:][w1[:-1] > 0.9] += 0.5
        p = p / p.sum()
        indices = np.arange(len(w1), dtype=int)
        for _ in range(self.max_step):
            while True:
                w2 = np.array(w1)
                j = np.random.choice(indices, p=p)
                w2[j] = 1 - w2[j]
                if self.valid_individual(w2):
                    w1 = w2
                    break
        return [self.creator.Individual(w1)]

    def crossover(self, ind1, ind2):
        """Randomly swap the contents of the individuals."""
        w1 = np.array(ind1.flatten())
        w2 = np.array(ind2.flatten())
        w3 = np.array(w1)
        w4 = np.array(w2)

        while True:
            # choose a random index at which to crossover
            j = np.random.randint(len(w1))
            w3[:j] = w1[:j]
            w3[j:] = w2[j:]
            w4[:j] = w2[:j]
            w4[j:] = w1[j:]
            if self.valid_individual(w3) and self.valid_individual(w4):
                break

        return self.creator.Individual(w3), self.creator.Individual(w4)

    def run(self):
        """Run a simple genetic algorithm.

        Modified version of deap's eaSimple.

        Returns
        -------
        best_attribute : array_like
            Attribute of the best individual (e.g., spectral windows).
        log : deap.tools.Logbook
            Log of the optimization data.
        """
        best, log = super().run()

        # convert best individual (w_s) into other settings needed
        w_s = best.flatten()
        w_b, intercept = self._perform_regression(w_s)
        return w_s, w_b, intercept, self.log
