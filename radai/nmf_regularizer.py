"""Regularizers for NMF utility."""

import abc
import numpy as np
import inspect
import importlib


class NMF_Regularizer(abc.ABC):
    """Base class for regularizers for NMF weight updates.

    The regularizer must be able to compute gradients and loss with input
    of the current weights.

    Any regularizer should follow this usage pattern:
        (1) Instantiate the regularizer
            reg = NMF_Regularizer(**kwargs)
        (2) Provide gradients when supplied with weights
        (3) Provide loss when supplied with weights
    """

    # named quantities sufficient to fully describe the filter state
    _attrs = []

    def __init__(self, **kwargs):
        """Initialize the regularizer."""
        # Check classname
        self.classname = type(self).__name__
        if "classname" in kwargs:
            assert kwargs.pop("classname") == self.classname
        # Initialize attributes to None
        for k in self._attrs:
            setattr(self, k, None)
        # Handle remaining kwargs as attributes
        for k, v in kwargs.items():
            assert k in self._attrs
            setattr(self, k, v)

    @classmethod
    def from_dict(cls, dic: dict):
        """Create a filter from a dict."""
        return cls(**dic)

    def to_dict(self) -> dict:
        """Return the filter settings and state as a dictionary."""
        d = {}
        for k in self._attrs:
            d[k] = getattr(self, k)
        d["classname"] = type(self).__name__
        return d

    # ---------------------------- Abstract Methods ---------------------------

    @abc.abstractmethod
    def grad_neg(self, A):
        """Return the negative portion of the gradient (as a positive).

        Parameters
        ----------
        A : array_like
            The current weight values to compute the gradient of the regularizer.

        Returns
        _______
        grad_neg : array_like
            The gradient for each weight. Has the same dimension as A.
        """
        pass

    @abc.abstractmethod
    def grad_pos(self, A):
        """Return the positive portion of the gradient.

        Parameters
        ----------
        A : array_like
            The current weight values to compute the gradient of the regularizer.

        Returns
        _______
        grad_neg : array_like
            The gradient for each weight. Has the same dimension as A.
        """
        pass

    @abc.abstractmethod
    def loss(self, A):
        """Return the negative log likelihood of the regularizer given the weights.

        Parameters
        ----------
        A : array_like
            The current weight values.

        Returns
        _______
        loss : float
            The scalar loss function.
        """
        pass


class Sparsity(NMF_Regularizer):
    """L_{1/2} regularizer on weights that correspond to non-frozen bases.

    The cost function here was originally based on [1], which is the sum of
    the square root of the weights. However, the square of that sum (i.e.,
    the "true" L_{1/2} norm) was found by us to converge faster in multiple
    cases.

    [1] Y. Qian, et al., "Hyperspectral Unmixing via $L_{1/2}$ Sparsity-
        Constrained Nonnegative Matrix Factorization," in IEEE Transactions
        on Geoscience and Remote Sensing, vol. 49, no. 11, pp. 4282-4297,
        Nov. 2011.
        https://doi.org/10.1109/TGRS.2011.2144605
    """

    _attrs = ["coeff", "frozen"]

    def grad_pos(self, A):
        grad = np.zeros_like(A)
        ls = np.sqrt(A[:, ~self.frozen]).sum() ** 2
        grad[:, ~self.frozen] = self.coeff * np.sqrt(ls) / np.sqrt(A[:, ~self.frozen])
        return grad

    def grad_neg(self, A):
        return 0

    def loss(self, A):
        return self.coeff * np.sqrt(A[:, ~self.frozen]).sum() ** 2


class Gamma(NMF_Regularizer):
    """Regularizer for weights drawn from a Gamma distribution.

    The parameters of the Gamma distribution are not stored directly but are
    calculated from the provided mean and covariance of the weights. The form
    of the Gamma distribution is from [2]:

        Gamma(α, β)(x) = 1 / Γ(α) (α / β)^α x^(α-1) exp(-α x / β)

    The mean of x is β and the variance is β^2/α.

    The loss function is the negative log of the above function:

        -log Gamma(α, β)(x) = -(α-1) log x + α x / β + f(α, β)

    [2] Lange, Bahn, & Little, "A Theoretical Study of Some Maximum Likelihood
        Algorithms for Emission and Transmission Tomography," IEEE
        Transactions on Medical Imaging, vol. 6, no. 2, pp. 106-114, June
        1987. DOI: 10.1109/TMI.1987.4307810
        https://ieeexplore.ieee.org/abstract/document/4307810
    """

    _attrs = ["coeff", "mean", "cov", "frozen"]

    def __init__(self, coeff, mean, cov, **kwargs):
        # check that inputs are ok.
        coeff, mean, cov = self._check_args(coeff, mean, cov)
        # populate object
        kwargs["coeff"] = coeff
        kwargs["cov"] = cov
        kwargs["mean"] = mean
        super().__init__(**kwargs)
        self._compute_alpha()

    def _check_args(self, coeff, mean, cov):
        coeff = np.asarray(coeff)
        # if no cov (e.g. filter not init) then don't run reg
        if cov is None:
            coeff = np.array([0.0])
        else:
            assert cov.ndim == 3
            assert cov.shape[1] == cov.shape[2]
            assert (cov >= 0).all()
            assert mean is not None
            assert mean.shape[0] == cov.shape[0]
            assert (coeff >= 0.0).all()
        return coeff, mean, cov

    def _compute_alpha(self):
        alpha = self.mean**2 / np.diagonal(self.cov, axis1=1, axis2=2)
        if (alpha < 1.0).any():
            alpha = alpha.clip(1.0001, None)
        setattr(self, "alpha", alpha)

    def update(self, coeff, mean, cov):
        coeff, mean, cov = self._check_args(coeff, mean, cov)
        setattr(self, "coeff", coeff)
        setattr(self, "mean", mean)
        setattr(self, "cov", cov)
        self._compute_alpha()

    @property
    def beta(self):
        return self.mean

    def grad_pos(self, A):
        assert A.shape[0] == self.beta.shape[0]
        return self.coeff * (self.alpha / self.beta)

    def grad_neg(self, A):
        assert A.shape[0] == self.beta.shape[0]
        return self.coeff * ((self.alpha - 1) / A)

    def loss(self, A):
        assert A.shape[0] == self.beta.shape[0]
        return (
            self.coeff
            * (-(self.alpha - 1) * np.log(A) + self.alpha * A / self.beta).sum()
        )


def _get_regularizer_classes():
    """Find the NMF_Regularizer classes in this module.

    Returns
    -------
    classes : dict
        Dictionary of NMF_Regularizer classes keyed by their names.
    """
    classes = {}
    for name, cls in inspect.getmembers(
        importlib.import_module("radai.nmf_regularizer"), inspect.isclass
    ):
        if issubclass(cls, NMF_Regularizer) and name != "NMF_Regularizer":
            classes[name] = cls
    return classes


def make_nmf_regularizer(arg, frozen):
    """Create regularizer from arg, if dict uses as kwargs to instantiate.

    Parameters
    ----------
    arg : Union[NMF_Regularizer, dict]
        A regularizer object (just returned) or the kwargs for the regularizer.
        If kwargs, they must contain a "classname" field giving a valid class
        from this module, and the remaining kwargs are passed to
        instantiate the regularizer.
    frozen : array_like
        boolean array describing frozen state of basis components

    Returns
    -------
    NMF_Regularizer
        The desired filter instance.
    """
    if isinstance(arg, dict):
        arg = dict(**arg)
        arg.update({"frozen": frozen})
        classname = arg.pop("classname")
        classes = _get_regularizer_classes()
        if classname not in classes:
            raise ValueError(
                f"NMF_Regularizer class {classname} not found in "
                "nmf_regularizer module\n"
                f"({list(classes.keys())})"
            )
        return classes[classname](**arg)
    elif isinstance(filter, NMF_Regularizer):
        arg.frozen = frozen
        return arg
    else:
        raise TypeError(f"Cannot make regularizer from {type(arg)}: {arg}")
